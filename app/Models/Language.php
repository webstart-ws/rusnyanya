<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    use HasFactory;
    public $timestamps = false;
    protected $appends = ['vacancyId'];

    protected $fillable = ['name'];
    public function vacancies()
    {
        return $this->belongsToMany(
            Vacancy::class,
            'vacancy_required_languages',
            'language_id',
            'vacancy_id'

        );
    }

    public function applicants()
    {
        return $this->belongsToMany(
            Applicant::class,
            'applicant_language',
            'language_id',
            'applicant_id'

        );
    }

    public function getVacancyIdsAttribute()
    {
        return count($this->vacancies) > 0 ? $this->vacancies()->pluck('id') : [];
    }
}
