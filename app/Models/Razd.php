<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Razd extends Model
{
    use HasFactory;

    protected $table = 'razd';

}
