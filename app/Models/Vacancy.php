<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

class Vacancy extends Model
{
    use HasFactory;

    protected $table = 'vacancies';

    protected $fillable = [
        'title',
        'user_id',
//        'position_id',
//        'document_id',
        'driver_license',
        'salary',
        'price_period_id',
        'recommendations',
        'comments',
        'country_id',
        'region_id',
        'city_id',
        'published',
        'is_paid',
        'is_top',
    ];

    public function scopeWithAll($query)
    {
        $query->with('author', 'pricePeriod', 'country', 'city', 'documents', 'positions', 'accommodations', 'languages', 'responds');
    }

    const DEFAULT_IMAGE = 'img/profile.jpg';

    public function getAvatarAttribute(): string
    {
        return $this->images->count() > 0
            ? $this->images->first()->path
            : self::DEFAULT_IMAGE;
    }


    public function getFillableFields()
    {
        return $this->fillable;
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getFromDateAttribute()
    {
       return Carbon::parse($this->updated_at)->format('d-m-Y');
    }

    public function getDateCommentAttribute()
    {
        return Carbon::parse($this->crated_at)->format('d-m-Y');
    }

    public function pricePeriod()
    {
        return $this->hasOne(PricePeriod::class, 'id', "price_period_id");
    }


    public function country()
    {
        return $this->hasOne(Country::class,'id', 'country_id');
    }

    public function getCountryNameAttribute()
    {
        return $this->country ? $this->country->name : 'Все страны';
    }

    public function city()
    {
        return $this->hasOne(City::class,'id', 'city_id');
    }

    public function getCityNameAttribute()
    {
        return $this->city ? $this->city->name : '';
    }

    public function region()
    {
        return $this->hasOne(Region::class,'id', 'region_id');
    }

    public function getRegionNameAttribute()
    {
        return $this->region ? $this->region->name : 'Района Нет';
    }

    /**
     * @return BelongsToMany
     */
    public function documents(): BelongsToMany
    {
        return $this->belongsToMany(
            Document::class,
            'vacancy_documents',
            'vacancy_id',
            'document_id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function positions(): BelongsToMany
    {
        return $this->belongsToMany(
            Position::class,
            'vacancy_positions',
            'vacancy_id',
            'position_id',
        );
    }

    /**
     * @return BelongsToMany
     */
    public function accommodations() : BelongsToMany
    {
        return $this->belongsToMany(
            Accommodation::class,
            'vacancy_accommodations',
            'vacancy_id',
            'accommodation_id'
        );
    }

    /**
     * @return string
     */
    public function getAccommodationNamesAttribute(): string
    {
        return count($this->accommodations) ? implode(',', $this->accommodations()->pluck('name')->toArray()) : '';
    }


    public function getDocumentNameAttribute()
    {
        return count($this->documents) ? implode(',', $this->documents()->pluck('name')->toArray()) : ' ';

    }

    public function getPositionNameAttribute()
    {
        return count($this->positions) ? implode(',', $this->positions()->pluck('name')->toArray()) : ' ';

    }
    /**
     * @return BelongsToMany
     */
    public function languages() : BelongsToMany
    {
        return $this->belongsToMany(
            Language::class,
            'vacancy_required_languages',
            'vacancy_id',
            'language_id'
        );
    }

    public function getLanguageNameAttribute()
    {
        return count($this->languages) ? implode(',', $this->languages()->pluck('name')->toArray()) : ' ';

    }

    /**
     * @return BelongsToMany
     */
    public function responds() : BelongsToMany
    {
        return $this->belongsToMany(
            User::class,
            'vacancy_responds',
            'vacancy_id',
            'user_id'
        )->withPivot('note');
    }


}
