<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';
    public $timestamps = false;

    use HasFactory;

    protected $fillable = ['name'];
    protected $appends = ['vacancyIds'];
    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'positiones',
            'pid',
            'uid'
        );
    }

    public function vacancies()
    {
        return $this->belongsToMany(
            Vacancy::class,
            'vacancy_positions',
            'position_id',
            'vacancy_id'
        );
    }


    public function jobType()
    {
        return $this->hasMany(JobType::class, 'id_positions', 'id');
    }

    public function getUserIdsAttribute() : array
    {
        return $this->users->pluck('id');
    }
    public function getVacancyIdsAttribute()
    {

        return count($this->vacancies) > 0 ? $this->vacancies()->pluck('id') : [];
    }
}
