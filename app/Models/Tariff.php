<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Tariff extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'cost',
        'currency_code',
        'currency_symbol',
    ];

    const LOW = 'Низкий';
    const MEDIUM = 'Средний';
    const HIGH = 'Высокий';

    const TARIFFS = [
        self::LOW,
        self::MEDIUM,
        self::HIGH,
    ];

    /**
     * @return BelongsToMany
     */
    public function countries(): BelongsToMany
    {
        return $this->belongsToMany(
            Country::class,
            'country_tariffs',
            'tariff_id',
            'country_id'
        );
    }
}
