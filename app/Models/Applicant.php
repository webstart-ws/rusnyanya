<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

class Applicant extends Model
{
    use HasFactory;

    protected $table = 'applicants';

    const DEFAULT_IMAGE = 'img/profile.jpg';

    protected $fillable = ['user_id', 'comments', 'about', 'salary', 'price_period_id', 'country_of_work_id', 'district_of_work_id', 'city_of_work_id', 'country_of_residence_id', 'district_of_residence_id', 'city_of_residence_id', 'schedule_mon', 'schedule_tue', 'schedule_wed', 'schedule_thu', 'schedule_fri', 'schedule_sat', 'schedule_sun', 'speciality', 'recommendations', 'driver_license', 'published', 'in_search', 'is_paid', 'is_top'];

    public function scopeWithAll($query)
    {
        $query->with('author',  'pricePeriod', 'country', 'city', 'documents', 'positions', 'accommodations', 'languages');
    }
    public function getAvatarAttribute(): string
    {
        return $this->images->count() > 0
            ? $this->images->first()->path
            : self::DEFAULT_IMAGE;
    }

    public function images(): HasMany
    {
        return $this->hasMany(UserImage::class, 'user_id','user_id');
    }

    public function setRecommendationsAttribute($value)
    {
        $this->attributes['recommendations'] = $value ?? '';

    }
    public function getFillableFields()
    {
        return $this->fillable;
    }



    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getFromDateAttribute(): string
    {
        return Carbon::parse($this->updated_at)->format('d-m-Y');
    }

    public function pricePeriod()
    {
        return $this->hasOne(PricePeriod::class, 'id', "price_period_id");
    }
    public function countryResidence()
    {
        return $this->hasOne(Country::class,'id', 'country_of_residence_id');
    }

    public function getCountryResidenceNameAttribute(): string
    {
        return $this->countryResidence ? $this->countryResidence->name : 'Все страны';
    }

    public function cityResidence()
    {
        return $this->hasOne(City::class,'id', 'city_of_residence_id');
    }

    public function getCityResidenceNameAttribute(): string
    {
        return $this->cityResidence ? $this->cityResidence->name : ' ';
    }

    public function districtResidence()
    {
        return $this->hasOne(Region::class,'id', 'district_of_residence_id');
    }

    public function getDistrictResidenceNameAttribute(): string
    {
        return $this->districtResidence ? $this->districtResidence->name : '';
    }


    public function country()
    {
        return $this->hasOne(Country::class,'id', 'country_of_work_id');
    }

    public function getCountryNameAttribute(): string
    {
        return $this->country ? $this->country->name : 'Все страны';
    }

    public function city()
    {
        return $this->hasOne(City::class,'id', 'city_of_work_id');
    }

    public function getCityNameAttribute(): string
    {
        return $this->city ? $this->city->name : '';
    }

    public function district()
    {
        return $this->hasOne(Region::class,'id', 'district_of_work_id');
    }

    public function getDistrictNameAttribute(): string
    {
        return $this->district ? $this->district->name : '';
    }

    public function education()
    {
        return $this->hasOne(Education::class, 'id', "education_id");
    }


    /**
     * @return BelongsToMany
     */
    public function positions(): BelongsToMany
    {
        return $this->belongsToMany(
            Position::class,
            'applicant_positions',
            'applicant_id',
            'position_id',
        );
    }


    /**
     * @return BelongsToMany
     */
    public function languages() : BelongsToMany
    {
        return $this->belongsToMany(
            Language::class,
            'applicant_language',
            'applicant_id',
            'language_id'
        )->withPivot('is_native');
    }

    /**
     * @return BelongsToMany
     */
    public function documents() : BelongsToMany
    {
        return $this->belongsToMany(
            Document::class,
            'applicant_documents',
            'applicant_id',
            'document_id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function accommodations() : BelongsToMany
    {
        return $this->belongsToMany(
            Accommodation::class,
            'applicant_accommodations',
            'applicant_id',
            'accommodation_id'
        );
    }

    public function getAgeAttribute(): int
    {
        return Carbon::parse($this->author->birthdate)->age;
    }

    public function getAccommodationNamesAttribute(): string
    {
        return count($this->accommodations) ? implode(',', $this->accommodations()->pluck('name')->toArray()) : '';
    }

    public function getLanguageNameAttribute(): string
    {
        return count($this->languages) ? implode(',', $this->languages()->pluck('name')->toArray()) : ' ';

    }

    public function getDocumentNameAttribute(): string
    {
        return count($this->documents) ? implode(',', $this->documents()->pluck('name')->toArray()) : ' ';

    }

    public function getPositionNameAttribute(): string
    {
        return count($this->positions) ? implode(',', $this->positions()->pluck('name')->toArray()) : ' ';

    }


}
