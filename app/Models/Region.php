<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Khsing\World\Models\Division;

class Region extends Division
{
    use HasFactory;
}
