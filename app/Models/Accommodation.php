<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accommodation extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $table = 'accommodations';
    protected $fillable = ['name'];
    protected $appends = ['vacancyIds'];

    public function vacancies()
    {
        return $this->belongsToMany(
            Vacancy::class,
            'vacancy_accommodations',
            'accommodation_id',
            'vacancy_id'
        );
    }

    public function applicants()
    {
        return $this->belongsToMany(
            Applicant::class,
            'applicant_accommodations',
            'accommodation_id',
            'applicant_id'
        );
    }



    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'accomodationes',
            'acid',
            'uid'
        )->withPivot('agroup');
    }
    public function getVacancyIdsAttribute()
    {
        return count($this->vacancies) > 0 ? $this->vacancies()->pluck('id') : [];
    }

}
