<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Khsing\World\Models\Country as WorldCountry;

class Country extends WorldCountry
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function tariffs(): BelongsToMany
    {
        return $this->belongsToMany(
            Tariff::class,
            'country_tariffs',
            'country_id',
            'tariff_id'
        );
    }

}
