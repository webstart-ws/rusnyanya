<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;

    protected $table = 'documents';

    protected $fillable = ['name'];

//    protected $appends = ['vacancyIds'];


    public function vacancies()
    {
        return $this->belongsToMany(
            Vacancy::class,
            'vacancy_documents',
            'document_id',
            'vacancy_id'
        );
    }
    public function getVacancyIdsAttribute(): array
{
    return count($this->vacancies) > 0 ? $this->vacancies()->pluck('id') : [];
}
    public static function getDocumentsByDivided(): array
    {
        $documents = Document::orderBy('id', 'asc')->get()->toArray();
        return count($documents) > 1 ? array_chunk($documents, ceil(count($documents) / 2)) : $documents;
    }




}
