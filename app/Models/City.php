<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Khsing\World\Models\City as WorldCity;

class City extends WorldCity
{
    use HasFactory;
}
