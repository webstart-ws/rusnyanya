<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'dt_reg',
        'password',
        'fname',
        'from_ip',
        'dt_last_login',
        'dt_last_edit',
        'phone',
        'viber',
        'whatsapp',
        'name',
        'fam',
        'dt_last_visit',
        'reg_type',
        'skype',
        'oplata',
        'oplata_period',
        'graph_1',
        'graph_2',
        'graph_3',
        'graph_4',
        'graph_5',
        'graph_6',
        'graph_7',
        'education_id',
        'spec',
        'lang_own',
        'recomend',
        'komm',
        'driving_licence',
        'in_search',
        'hide_contacts',
        'moder_check',
        'link',
        'soc_link',
        'email1',
        'birthdate',
        'grazd',
        'ncountry',
        'ntown',
        'lang_other',
        'wcountry',
        'wtown',
        'wdistrict',
        'flg_opl',
        'showing_user_count',
        'flg_important',
        'flg_active',
        'tarif',
        'tarif_dt',
        'tarif_info',
        'spam_sent',
        'superuser',
        'note',
        'num_views',
        'num_sales',
        'interviewed',
        'rate',
        'google_id',
        'provider',
        'provider_id',
        'access_token',
        'subscriptions_enabled'
    ];

    protected $appends = ['age', 'avatar'];

    const DEFAULT_IMAGE = 'img/profile.jpg';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const ROLE_VACANCY = 'vacancy';
    const ROLE_VACANCY_VIEW = 'vacancies';

    const ROLE_APPLICANT = 'questionnaire';
    const ROLE_APPLICANT_VIEW = 'questionnaires';

    const ROLES = [
        self::ROLE_VACANCY => [
            'id' => 1,
            'name' => 'Соискатель',
            'viewName' => self::ROLE_VACANCY_VIEW
        ],
        self::ROLE_APPLICANT => [
            'id' => 0,
            'name' => 'Работодатель',
            'viewName' => self::ROLE_APPLICANT_VIEW
        ],
    ];


    public static function boot()
    {
        parent::boot();

        static::creating(function($user) {
            $user->dt_last_edit = now();

            foreach (['fam', 'password', 'phone', 'viber', 'whatsapp', 'skype', 'soc_link', 'email1', 'subscriptions_enabled'] as $field) {
                $user->$field = request()->get($field) ?? '';
            }

            $user->dt_last_visit = now();
            $user->hide_contacts = request()->get('hide_contacts') == 1 ? request()->get('hide_contacts') : 0;
            $user->oplata = '';
            $user->spec = '';
            $user->lang_own = '';
            $user->komm = '';
            $user->link = '';
            $user->birthdate = now();
            $user->grazd = '';
            $user->lang_other = '';
            $user->wcountry = 281;
            $user->wtown = 312;
            $user->showing_user_count = 1;
            $user->flg_important = 1;
            $user->tarif_dt = now();
        });
    }

    protected $table = 'users';

    public $timestamps = false;

    public function education()
    {
        return $this->hasOne(Education::class, "id", "education_id");
    }

    public function pricePeriod()
    {
        return $this->hasOne(PricePeriod::class, 'id', "oplata_period");
    }

    public function comments()
    {
        return $this->hasMany(UsersComment::class, 'user_id', 'id');
    }

    public function user_admin_comments()
    {
        return $this->hasMany(UserAdminComments::class);
    }

//    public function country()
//    {
//        return $this->hasOne(Country::class,'id', 'ncountry');
//    }
//    public function city()
//    {
//        return $this->hasOne(City::class,'id', 'ntown');
//    }

    /**
     * @return BelongsToMany
     */
    public function positions() : BelongsToMany
    {
        return $this->belongsToMany(
            Position::class,
            'positiones',
            'uid',
            'pid',
        );
    }

    /**
     * @return BelongsToMany
     */
    public function accommodations() : BelongsToMany
    {
        return $this->belongsToMany(
            Accommodation::class,
            'accomodationes',
            'uid',
            'acid'
        )->withPivot('agroup');
    }

    public function documents() : BelongsToMany
    {
        return $this->belongsToMany(
            Document::class,
            'documentes',
            'uid',
            'dcid'
        )->withPivot('dgroup');
    }

    public function getDocumentNameAttribute()
    {
        return $this->documentes ? $this->documentes()->first()->name : null;
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value ?? Str::random(16));
    }

    public function getAgeAttribute()
    {
        return Carbon::parse($this->birthdate)->age;
    }

    public function getAvatarAttribute()
    {
        return $this->images->count() > 0
            ? $this->images->first()->path
            : self::DEFAULT_IMAGE;
    }

    public function images(): HasMany
    {
        return $this->hasMany(UserImage::class, 'user_id');
    }

    public function getLastDateAttribute()
    {
        return Carbon::parse($this->dt_last_visit)->format('d-m-Y');
    }

    public function vacancies()
    {
        return $this->hasMany(Vacancy::class, 'user_id');
    }

    public function applicants()
    {
        return $this->hasMany(Applicant::class, 'user_id');
    }

    public function applicant()
    {
        return $this->applicants()->first();
    }


    public function favorites()
    {
        return $this->belongsToMany(
            User::class,
            'favorites',
            'uid_v',
            'uid_a'
        )->withPivot('dt');
    }

    public function getFavoriteIdsAttribute(): array
    {
        return count($this->favorites) > 0 ? $this->favorites()->pluck('uid_a')->toArray() : [];
    }

}
