<?php

namespace App\View\Components;

use App\Models\Region;
use Illuminate\View\Component;

class RegionsSelect extends Component
{
    protected $countryId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($countryId = null)
    {
        $this->countryId = $countryId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $query = Region::query();

        if ($this->countryId) {
            $query->where(['country_id' => $this->countryId]);
        }

        $regions = $query->orderBy('name', 'asc')->get();

        return view('components.regions-select', compact('regions'));
    }
}
