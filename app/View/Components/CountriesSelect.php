<?php

namespace App\View\Components;

use App\Models\Country;
use Illuminate\View\Component;

class CountriesSelect extends Component
{
    public $continentId;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($continentId = null)
    {
        $this->continentId = $continentId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $query = Country::query();

        if ($this->continentId) {
            $query->where(['continent_id' => $this->continentId]);
        }

        $countries = $query->orderBy('name', 'asc')->get();
        $type = 'default';
        return view('components.countries-select', compact('countries', 'type'));
    }
}
