<?php

namespace App\View\Components;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\View\Component;

class CitiesSelect extends Component
{
    protected $regionId;
    protected $countryId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->countryId = $request->get('countryId') ?? null;
        $this->regionId = $request->get('divisionId') ?? null;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $query = City::query();

        if ($this->countryId) {
            $query->where(['country_id' => $this->countryId]);
        }

        if ($this->regionId) {
            $query->where(['division_id' => $this->regionId]);
        }

        $cities = $query->orderBy('name', 'asc')->get();

        return view('components.cities-select', compact('cities'));
    }
}
