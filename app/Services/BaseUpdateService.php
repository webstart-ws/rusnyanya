<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;

abstract class BaseUpdateService implements ServiceInterface
{
    protected $id;
    protected $model;
    protected $data;
    protected $savedData = [];

    public function __construct(int $id, FormRequest $updateRequest)
    {
        if ($id <= 0) {
            throw new \InvalidArgumentException("Invalid id: $id");
        }

        $this->id   = $id;
        $this->data = $updateRequest->all();
    }

    /**
     * @return Model
     */
    abstract protected function getBaseModel(): Model;

    protected function getModel()
    {
        if (empty($this->model)) {
            $this->model = $this->getBaseModel()::findOrFail($this->id);
        }

        return $this->model;
    }

    abstract public function update();

    public function run() : bool
    {
        return $this->update();
    }
}
