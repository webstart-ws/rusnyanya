<?php


namespace App\Services\Applicant;

use App\Models\Applicant;
use App\Models\User;
use App\Services\BaseCreateService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreateApplicant extends BaseCreateService
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = $request;
    }

    /**
     * @return User
     */
    public function getModel() : Model
    {
        return new Applicant();
    }

    /**
     * @return array
     */
    public function getCreatedData() : array
    {
        return $this->data->only([
            'user_id',
            'comments',
            'about',
            'salary',
            'price_period_id',
            'country_of_work_id',
            'district_of_work_id',
            'city_of_work_id',
            'country_of_residence_id',
            'district_of_residence_id',
            'city_of_residence_id',
            'schedule_mon',
            'schedule_tue',
            'schedule_wed',
            'schedule_thu',
            'schedule_fri',
            'schedule_sat',
            'schedule_sun',
            'speciality',
            'recommendations',
            'driver_license',
            'published',
            'in_search',
            'is_paid',
            'is_top',
            'documents',
            'positions',
            'accommodations',
            'languages',
            'native_language',
        ]);
    }

    public function create()
    {
        $newApplicant = $this->getModel()->fill(
            $this->getCreatedData()
        );

        if ($newApplicant->save()) {

            DB::transaction(
                function () use ($newApplicant) {
                    if ($this->data['documents']) {
                        $newApplicant->documents()->attach($this->data['documents']);
                    }

                    if ($this->data['positions']) {
                        $newApplicant->positions()->attach($this->data['positions']);
                    }

                    if ($this->data['accommodations']) {
                        $newApplicant->accommodations()->attach($this->data['accommodations']);
                    }

                    if ($this->data['languages']) {
                        $newApplicant->languages()->attach($this->data['languages']);
                    }

                    if ($this->data['native_language']) {
                        $newApplicant->languages()->attach($this->data['native_language']);
                    }

                    DB::commit();
                }
            );
            return true;
        }

        return false;
    }
}
