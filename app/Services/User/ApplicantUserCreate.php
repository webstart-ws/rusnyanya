<?php

namespace App\Services\User;

use App\Models\User;
use App\Models\UserImage;
use App\Services\Applicant\CreateApplicant;
use App\Services\BaseCreateService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\Applicant\CreateRequest as CreateApplicantRequest;


class ApplicantUserCreate extends BaseCreateService
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->data = $request;

    }

    /**
     * @return User
     */
    public function getModel() : Model
    {
        return new User();
    }

    /**
     * @return array
     */
    public function getCreatedData() : array
    {
        $this->data->merge(['reg_type' => 0]);
        return $this->data->only([
            'name',
            'fam',
            'email',
            'password',
            'password_repeat',
            'email1',
            'phone',
            'whatsapp',
            'skype',
            'viber',
            'soc_link',
            'grazd',
            'reg_type',
        ]);
    }

    public function create()
    {
        DB::beginTransaction();

        $newUser = $this->getModel()->fill(
            $this->getCreatedData()
        );

        /* Validate applicant request */
        app()->make(CreateApplicantRequest::class);

        if ($newUser->save()) {
            request()->merge(['user_id' => $newUser->id]);

            /* create new applicant */
            (new CreateApplicant(request()))->run();

            /* save user images */
            $this->saveImages($newUser);

            Mail::send('auth.register-email', ['user' => $newUser, 'password' => request()->get('password')], function($message) use ($newUser) {
                $message->to($newUser->email, $newUser->name)->subject("Registration Email");
                $message->from("rusnyaya@gmail.com", "Rusnyanya Administration");
            });

            Session::flash('success', __('auth.register.success'));

            Auth::loginUsingId($newUser->id);
            return true;
        }

        Session::flash('danger', __('auth.register.error'));

        return false;
    }


    public function saveImages($model)
    {
        $new_images = [];

        if (isset($this->data['images'])) {
            $images = $this->data['images'];
            foreach ($images as $key => $image) {
                $fileName = time() . "{$key}." . $image->getClientOriginalExtension();
                $filePath = $image->storeAs("uploads/users/applicant/{$model->id}", $fileName, 'public');
                $path = '/storage/' . $filePath;
                $new_images[] = new UserImage(['path' => $path, 'user_id' => $model->id]);

                if ($key == 0) {
                    $model->fname = $path;
                    $model->save();
                }
            }
        }

        $model->images()->saveMany($new_images);
    }
}
