<?php

namespace App\Services\User;

use App\Models\User;
use App\Models\UserImage;
use App\Services\BaseCreateService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;


class VacancyUserCreate extends BaseCreateService
{
    /**
     * @return User
     */
    public function getModel() : Model
    {
        return new User();
    }

    /**
     * @return array
     */
    public function getCreatedData() : array
    {
        // add or change data array if needed
        $this->data['reg_type'] = 1;
        return $this->data;
    }

    public function create()
    {
        $newVacancy = $this->getModel()->fill(
            $this->getCreatedData()
        );

        if ($newVacancy->save()) {
            $this->saveImages($newVacancy);

            Mail::send('auth.register-email', ['user' => $newVacancy, 'password' => request()->get('password')], function($message) use ($newVacancy) {
                $message->to($newVacancy->email, $newVacancy->name)->subject("Registration Email");
                $message->from("rusnyaya@gmail.com", "Rusnyanya Administration");
            });

            Session::flash('success', __('auth.register.success'));

            Auth::loginUsingId($newVacancy->id);
            return true;
        }

        Session::flash('danger', __('auth.register.error'));

        return false;
    }


    public function saveImages($model)
    {
        $new_images = [];

        if (isset($this->data['images'])) {
            $images = $this->data['images'];
            foreach ($images as $key => $image) {
                $fileName = time() . "{$key}." . $image->getClientOriginalExtension();
                $filePath = $image->storeAs("uploads/users/vacancy/{$model->id}", $fileName, 'public');
                $path = '/storage/' . $filePath;
                $new_images[] = new UserImage(['path' => $path, 'user_id' => $model->id]);

                if ($key == 0) {
                    $model->fname = $path;
                    $model->save();
                }
            }
        }

        $model->images()->saveMany($new_images);
    }
}
