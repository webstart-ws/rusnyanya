<?php

namespace App\Services\User;

use App\Models\User;
use App\Models\UserImage;
use App\Services\BaseUpdateService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;


class ApplicantUserUpdate extends BaseUpdateService
{
    /**
     * @return User
     */
    protected function getBaseModel(): Model
    {
        return new User();
    }

    /**
     * @return array
     */
    public function getUpdatedData() : array
    {
        // add or change data array if needed
        $this->data['reg_type'] = 0;
        return $this->data;
    }

    public function update()
    {
        $newVacancy = $this->getModel()->fill(
            $this->getUpdatedData()
        );

        if ($newVacancy->save()) {
            $this->updateImages($newVacancy);

            Session::flash('success', __('auth.update.success'));
            return true;
        }

        Session::flash('danger', __('auth.update.error'));

        return false;
    }


    public function updateImages($model)
    {
        $new_images = [];

        if (isset($this->data['images']) && $model->images) {
            $images = $this->data['images'];
            $modelImagesCount = $model->images->count();
            foreach ($images as $key => $image) {
                if ($modelImagesCount < 5) {
                    $modelImagesCount += 1;
                    $fileName = time() . "{$key}." . $image->getClientOriginalExtension();
                    $filePath = $image->storeAs("uploads/users/vacancy/{$model->id}", $fileName, 'public');
                    $path = '/storage/' . $filePath;
                    $new_images[] = new UserImage(['path' => $path, 'user_id' => $model->id]);

                    if ($key == 0) {
                        $model->fname = $path;
                        $model->save();
                    }
                }
            }
        }

        $model->images()->saveMany($new_images);
    }

}
