<?php

namespace App\Services\Social;

use App\Models\User;
use App\Services\ServiceInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Contracts\User as SocialiteUser;

class SocialAuth implements ServiceInterface
{
    protected $user;

    protected $driver;

    public function __construct(SocialiteUser $user, $driver)
    {
        $this->user = $user;
        $this->driver = $driver;
    }

    /**
     * check if the user exists then sign in, if not, then create and sign in
     *
     * @return bool
     */
    public function login(): bool
    {
        $user = User::where('email', $this->user->getEmail())->first();

        if ($this->user->getEmail() && $user) {
            $user->update([
                'provider' => $this->driver,
                'provider_id' => $this->user->id,
                'access_token' => $this->user->token
            ]);

            Auth::login($user, true);
            Session::flash("success", "Вы успешно вошли в систему");

            return true;
        }

        Session::flash(
            "danger",
            "Вы не можете войти в систему с помощью {$this->driver}, поскольку вы не зарегистрированы в нашей системе."
        );

        return false;
    }

    /**
     * @return bool
     * @throws \Throwable
     */
    public function run(): bool
    {
        return $this->login();
    }
}
