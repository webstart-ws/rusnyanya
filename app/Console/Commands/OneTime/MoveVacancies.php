<?php

namespace App\Console\Commands\OneTime;

use App\Models\User;
use App\Models\Vacancy;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MoveVacancies extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
        protected $signature = 'vacancy:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move vacancies from users table to newly created vacancies table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userTableVacancies = User::with('documents', 'positions', 'accommodations')
            ->where("reg_type", User::ROLES[User::ROLE_VACANCY]['id'])->get();

        DB::beginTransaction();

        try {
            foreach ($userTableVacancies as $oldVacancy) {
                $newVacancy = new Vacancy();
                $newVacancy->title = $oldVacancy->spec;
                $newVacancy->user_id = $oldVacancy->id;
                $newVacancy->driver_license = $oldVacancy->driving_licence;
                $newVacancy->salary = $oldVacancy->oplata;
                $newVacancy->price_period_id = $oldVacancy->oplata_period;
                $newVacancy->recommendations = $oldVacancy->recomend;
                $newVacancy->comments = $oldVacancy->komm;
                $newVacancy->country_id = $oldVacancy->wcountry;
                $newVacancy->region_id = $oldVacancy->wdistrict;
                $newVacancy->city_id = $oldVacancy->wtown;
                $newVacancy->published = $oldVacancy->flg_active;
                $newVacancy->is_paid = $oldVacancy->flg_opl;
                $newVacancy->is_top = $oldVacancy->flg_important;
                $newVacancy->number_views = $oldVacancy->num_views;
                $newVacancy->created_at = $oldVacancy->dt_reg;
                $newVacancy->save();

                if ($oldVacancy->documents) {
                    $vacancyDocumentIds = $oldVacancy->documents()->pluck('dcid');
                    $oldVacancy->documents()->detach();
                    $newVacancy->documents()->attach($vacancyDocumentIds);
                }

                if ($oldVacancy->positions) {
                    $vacancyPositionIds = $oldVacancy->positions()->pluck('pid');
                    $oldVacancy->positions()->detach();
                    $newVacancy->positions()->attach($vacancyPositionIds);
                }

                if ($oldVacancy->accommodations) {
                    $vacancyAccommodationIds = $oldVacancy->accommodations()->pluck('acid');
                    $oldVacancy->accommodations()->detach();
                    $newVacancy->accommodations()->attach($vacancyAccommodationIds);
                }
            }

            echo 'All vacancies moved to vacancies table';
        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Vacancies moving stopped. '. $exception->getMessage());
            echo $exception->getMessage() . "\n";
            return false;
        }

        DB::commit();
        echo 'All applicants moved to applicants table';
        return true;
    }
}
