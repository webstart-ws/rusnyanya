<?php

namespace App\Console\Commands\OneTime;

use App\Models\User;
use App\Models\Applicant;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MoveApplicants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
        protected $signature = 'applicant:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move applicants from users table to newly created applicants table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::beginTransaction();

        $userTableApplicants = User::with('documents', 'positions', 'accommodations')
            ->where("reg_type", User::ROLES[User::ROLE_APPLICANT]['id'])->get();

        try {
            foreach ($userTableApplicants as $oldApplicant) {
                $newApplicant = new Applicant();
                $newApplicant->user_id = $oldApplicant->id;
                $newApplicant->salary = $oldApplicant->oplata;
                $newApplicant->price_period_id = $oldApplicant->oplata_period;
                $newApplicant->comments = $oldApplicant->komm;
                $newApplicant->about = $oldApplicant->note;
                $newApplicant->country_of_work_id = $oldApplicant->wcountry;
                $newApplicant->district_of_work_id = $oldApplicant->wdistrict;
                $newApplicant->city_of_work_id = $oldApplicant->wtown;
                $newApplicant->country_of_residence_id = $oldApplicant->ncountry;
                $newApplicant->district_of_residence_id = $oldApplicant->ncountry;
                $newApplicant->city_of_residence_id = $oldApplicant->ntown;
                $newApplicant->schedule_mon = $oldApplicant->graph_1;
                $newApplicant->schedule_tue = $oldApplicant->graph_2;
                $newApplicant->schedule_wed = $oldApplicant->graph_3;
                $newApplicant->schedule_thu = $oldApplicant->graph_4;
                $newApplicant->schedule_fri = $oldApplicant->graph_5;
                $newApplicant->schedule_sat = $oldApplicant->graph_6;
                $newApplicant->schedule_sun = $oldApplicant->graph_7;
                $newApplicant->in_search = $oldApplicant->in_search;
                $newApplicant->speciality = $oldApplicant->spec;
                $newApplicant->recommendations = $oldApplicant->recomend;
                $newApplicant->driver_license = $oldApplicant->driving_licence;
                $newApplicant->published = $oldApplicant->flg_active;
                $newApplicant->is_paid = $oldApplicant->flg_opl;
                $newApplicant->is_top = $oldApplicant->flg_important;
                $newApplicant->save();

                if ($oldApplicant->documents) {
                    $applicantDocumentIds = $oldApplicant->documents()->pluck('dcid');
                    $oldApplicant->documents()->detach();
                    $newApplicant->documents()->attach($applicantDocumentIds);
                }

                if ($oldApplicant->positions) {
                    $applicantPositionIds = $oldApplicant->positions()->pluck('pid');
                    $oldApplicant->positions()->detach();
                    $newApplicant->positions()->attach($applicantPositionIds);
                }

                if ($oldApplicant->accommodations) {
                    $applicantAccommodationIds = $oldApplicant->accommodations()->pluck('acid');
                    $oldApplicant->accommodations()->detach();
                    $newApplicant->accommodations()->attach($applicantAccommodationIds);
                }
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Applicants moving stopped. '. $exception->getMessage());
            echo $exception->getMessage() . "\n";
            return false;
        }

        DB::commit();
        echo 'All applicants moved to applicants table';
        return true;
    }
}
