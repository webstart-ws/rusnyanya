<?php

namespace App\Http\Requests\Questionnaire;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'            => ['required', 'regex:/^[a-zA-ZА-Яа-яЁё]+$/u','min:2', 'alpha_dash'],
            'fam'             => ['required', 'regex:/^[a-zA-ZА-Яа-яЁё]+$/u', 'min:2','alpha_dash'],
            'email'           => ['required', 'email', 'unique:users,email'],
            'password'        => ['required', 'min:6'],
            'password_repeat' => ['required', 'same:password'],
            'images.*'        => ['nullable', 'mimes:jpg,jpeg,webp,png|max:2048'],
            'phone'           => ['required', 'regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|
                    2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|
                    4[987654310]|3[9643210]|3[70]|7|1)\d{1,14}$/'],
            'whatsapp'        => ['required', 'regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|
                    2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|
                    4[987654310]|3[9643210]|3[70]|7|1)\d{1,14}$/'],
//            'viber'           => ['required', 'regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|
//                    2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|
//                    4[987654310]|3[9643210]|3[70]|7|1)\d{1,14}$/'],
//            'skype'           => ['required'],
            'email1'          => ['required', 'email'],
            'grazd'           => ['required'],
            'education_id'=> ['required', 'integer', 'exists:education,id'],
            'agree_agreement' => ['required', 'in:1'],
            // 'g-recaptcha-response' => ['required', 'captcha'],
            'comments' => ['required', 'min:40'],
//            'about' => ['required'],
            'salary' => ['required', 'integer'],
            'birthdate' => ['required'],
            'positions' => ['required', 'array'],
            'positions.*' => ['required', 'integer', 'exists:positions,id'],

            'documents' => ['required', 'array'],
            'documents.*' => ['required', 'integer', 'exists:documents,id'],

            'accommodations' => ['required', 'array'],
            'accommodations.*' => ['required', 'integer', 'exists:accommodations,id'],
//            'languages' => ['required', 'array'],
//            'languages.*' => ['required', 'integer', /* 'exists:languages,id' */],

//            'price_period_id' => ['required', 'integer', 'exists:price_periods,id'],
            'country_of_work_id' => ['required', 'integer', 'exists:world_countries,id'],
//            'district_of_work_id' => ['required', 'integer', 'exists:world_divisions,id'],
            'city_of_work_id' => ['nullable', 'integer', 'exists:world_cities,id'],
            'country_of_residence_id' => ['required', 'integer', 'exists:world_countries,id'],
//            'district_of_residence_id' => ['required', 'integer', 'exists:world_divisions,id'],
            'city_of_residence_id' => ['nullable', 'integer', 'exists:world_cities,id'],
            'schedule_mon' => ['nullable'],
            'schedule_tue' => ['nullable'],
            'schedule_wed' => ['nullable'],
            'schedule_thu' => ['nullable'],
            'schedule_fri' => ['nullable'],
            'schedule_sat' => ['nullable'],
            'schedule_sun' => ['nullable'],
            'speciality'  => ['required'],
            'recommendations' => ['nullable', 'boolean'],
            'driver_license' => ['nullable', 'boolean'],
            'in_search' => ['nullable', 'boolean'],
            'native_language' => ['required'],

        ];
    }

    public function messages(): array
    {
        return [
            'images.*.mimes' => 'Разрешены только изображения в формате jpeg,png,jpg и webp',
            'images.*.max'   => 'Прости! Максимально допустимый размер изображения составляет 2 МБ',
//            'g-recaptcha-response.required' => 'Пожалуйста, заполните капчу',
            'name.required' => 'Пожалуйста, заполните имя',
            'name.regex' => 'Пожалуйста, заполните только буквы',
            'name.min' => 'Должно быть не менее 2 символов',
            'fam.min' => 'Должно быть не менее 2 символов',
            'fam.required' => 'Пожалуйста, заполните фамилию',
            'fam.regex' => 'Пожалуйста, заполните только буквы',
            'email.required'=> 'Пожалуйста, заполните электронную почту',
            'email.email'=> 'Введите действительный электронный адрес ',
            'email.unique'=> 'Это электронный адрес уже занято',
            'password.required'=> 'Пожалуйста, заполните пароль',
            'password.min'=> 'Должно быть не менее 6 символов',
            'password_repeat.required'=> 'Пожалуйста, повторите пароль ',
            'phone.required'=> 'Пожалуйста, заполните тел. номер',
            'phone.regex'=> 'Пожалуйста, заполните международным кодексом',
            'whatsapp.regex'=> 'Пожалуйста, заполните международным кодексом',
            'whatsapp.required'=> 'Пожалуйста, заполните WhatsApp',
//            'viber.required'=> 'Пожалуйста, заполните Viber',
//            'viber.regex'=> 'Пожалуйста, заполните международным кодексом',
            'email1.required'=> 'Пожалуйста, повторите электронную почту',
            'grazd.required'=> 'Пожалуйста, заполните Гражданство',
            'education_id.required'=> 'Пожалуйста, заполните образование',
//            'skype.required'  =>'Пожалуйста, заполните Skype',
            'agree_agreement.required'=> 'Это поле обязательно для заполнения ',

            'comments.required' =>  'Обязательно для заполнения',
            'comments.min' =>  'Минимум 40 символов',
            'salary.required' =>  'Заполните размер з/п',
            'birthdate.required' =>  'Пожалуйста, заполните дата рождения',
            'salary.integer' =>  'Только цифры',
            'positions.required' =>  'Пожалуйста, заполните должность',
//            'positions.*.required' =>  'Пожалуйста, заполните',
            'documents.required' =>  'Обязательно для заполнения',
//            'documents.*.required' =>  'Пожалуйста, заполните',
            'accommodations.required' =>  'Обязательно для заполнения',
//            'accommodations.*.required' =>  'Пожалуйста, заполните',
//            'languages.required' =>  'Выберите язык',
//            'languages.*.required' =>  'Пожалуйста, заполните',
//            'price_period_id.required' =>  'Условия оплаты',
            'country_of_work_id.required' =>  'Пожалуйста, заполните страну',
            'city_of_work_id.required' =>  'Пожалуйста, заполните город',
            'country_of_residence_id.required' =>  'Пожалуйста, заполните страну проживания',
            'city_of_residence_id.required' => 'Пожалуйста, заполните город проживания',
            'speciality.required'=> 'Пожалуйста, заполните вашу специальность',
            'native_language.required' =>  'Пожалуйста, заполните родной язык',
        ];
    }
}
