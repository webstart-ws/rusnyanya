<?php

namespace App\Http\Requests\User;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class VacancyUserCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'            => ['required', 'regex:/^[a-zA-ZА-Яа-яЁё]+$/u', 'alpha_dash'],
            'fam'             => ['required', 'regex:/^[a-zA-ZА-Яа-яЁё]+$/u', 'alpha_dash'],
            'email'           => ['required', 'email', 'unique:users,email'],
            'password'        => ['required', 'min:6'],
            'password_repeat' => ['required', 'same:password'],
            'images.*'        => ['nullable', 'mimes:jpg,jpeg,webp,jfif|max:2048'],
            'phone'           => ['required', 'regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|
                    2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|
                    4[987654310]|3[9643210]|3[70]|7|1)\d{1,14}$/'],

            'whatsapp'        => ['required', 'regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|
                    2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|
                    4[987654310]|3[9643210]|3[70]|7|1)\d{1,14}$/'],
//            'viber'           => ['required', 'regex:/\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|
//                    2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|
//                    4[987654310]|3[9643210]|3[70]|7|1)\d{1,14}$/'],

            'email1'          => ['nullable', 'email'],
            'agree_agreement' => ['required', 'in:1'],
//            'g-recaptcha-response' => ['required', 'captcha'],
        ];
    }

    public function messages(): array
    {
        return [
            'images.*.mimes' => 'Разрешены только изображения в формате jpeg,png,jpg и webp',
            'images.*.max'   => 'Прости! Максимально допустимый размер изображения составляет 2 МБ',
//            'g-recaptcha-response.required' => 'Пожалуйста, заполните капчу',
            'name.required' => 'Пожалуйста, заполните имя',
            'name.regex' => 'Пожалуйста, заполните только буквы',
            'fam.required' => 'Пожалуйста, заполните фамилию',
            'fam.regex' => 'Пожалуйста, заполните только буквы',
            'email.required'=> 'Пожалуйста, заполните электронную почту',
            'email.email'=> 'Введите действительный электронный адрес ',
            'email.unique'=> 'Это электронный адрес уже занято',
            'password.required'=> 'Пожалуйста, заполните пароль',
            'password.min'=> 'Должно быть не менее 6 символов',
            'password_repeat.required'=> 'Пожалуйста, повторите пароль ',
            'phone.required'=> 'Пожалуйста, заполните тел. номер',
            'phone.regex'=> 'Пожалуйста, заполните международным кодексом',
            'email1.required'=> 'Пожалуйста, повторите электронную почту',
            'agree_agreement.required'=> 'Это поле обязательно для заполнения ',
            'whatsapp.regex'=> 'Пожалуйста, заполните международным кодексом',
            'whatsapp.required'=> 'Пожалуйста, заполните WhatsApp',
//            'viber.required'=> 'Пожалуйста, заполните Viber',
//            'viber.regex'=> 'Пожалуйста, заполните международным кодексом',
//            'skype.required'  =>'Пожалуйста, заполните Skype',

        ];
    }
}
