<?php

namespace App\Http\Requests\Applicant;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'comments' => ['required', 'min:40'],
//            'about' => ['required'],
            'salary' => ['required', 'integer'],
            'birthdate' => ['required'],
            'positions' => ['required', 'array'],
            'positions.*' => ['required', 'integer', 'exists:positions,id'],

            'documents' => ['required', 'array'],
            'documents.*' => ['required', 'integer', 'exists:documents,id'],

            'languages' => ['required', 'array'],
            'languages.*' => ['required', 'integer', /* 'exists:languages,id' */],

            'price_period_id' => ['required', 'integer', 'exists:price_periods,id'],
            'country_of_work_id' => ['required', 'integer', 'exists:world_countries,id'],
//            'district_of_work_id' => ['required', 'integer', 'exists:world_divisions,id'],
            'city_of_work_id' => ['nullable', 'integer', 'exists:world_cities,id'],
            'country_of_residence_id' => ['required', 'integer', 'exists:world_countries,id'],
//            'district_of_residence_id' => ['required', 'integer', 'exists:world_divisions,id'],
            'city_of_residence_id' => ['nullable', 'integer', 'exists:world_cities,id'],
            'schedule_mon' => ['nullable'],
            'schedule_tue' => ['nullable'],
            'schedule_wed' => ['nullable'],
            'schedule_thu' => ['nullable'],
            'schedule_fri' => ['nullable'],
            'schedule_sat' => ['nullable'],
            'schedule_sun' => ['nullable'],
            'speciality'  => ['required'],
            'recommendations' => ['nullable', 'boolean'],
            'driver_license' => ['nullable', 'boolean'],
            'in_search' => ['nullable', 'boolean'],
            'native_language' => ['required'],
        ];
    }

    public function messages(): array
    {
        return [
            'comments.required' =>  'Обязательно для заполнения',
            'comments.min' =>  'Минимум 40 символов',
            'salary.required' =>  'Заполните размер з/п',
            'salary.integer' =>  'Только цифры',
            'positions.required' =>  'Пожалуйста, заполните должность',
            'positions.*.required' =>  'Пожалуйста, заполните',
            'documents.required' =>  'Обязательно для заполнения',
            'documents.*.required' =>  'Пожалуйста, заполните',
            'languages.required' =>  'Выберите язык',
            'languages.*.required' =>  'Пожалуйста, заполните',
            'price_period_id.required' =>  'Условия оплаты',
            'country_of_work_id.required' =>  'Пожалуйста, заполните страну',
            'city_of_work_id.required' =>  'Пожалуйста, заполните город',
            'country_of_residence_id.required' =>  'Пожалуйста, заполните страну проживания',
            'city_of_residence_id.required' => 'Пожалуйста, заполните город проживания',
            'speciality.required'=> 'Пожалуйста, заполните вашу специальность',
            'native_language.required' =>  'Пожалуйста, заполните родной язык',
        ];
    }
}
