<?php

namespace App\Http\Middleware;

use App\Models\User;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
            if ($request->get('register_type') == User::ROLE_VACANCY_VIEW) {
                return route('register', ['type' => User::ROLE_VACANCY_VIEW]);
            } elseif ($request->get('register_type') == User::ROLE_APPLICANT_VIEW) {
                return route('register', ['type' => User::ROLE_APPLICANT_VIEW]);
            }

            return route('login');
        }
    }
}
