<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UsersComment;
use App\Models\Vacancy;
use App\Models\Country;
use App\Models\Position;
use App\Models\Language;
use App\Models\Document;
use App\Models\PricePeriod;
use App\Models\Accommodation;

use App\Services\User\VacancyUserCreate;
use App\Services\User\VacancyUserUpdate;

use Illuminate\Contracts\Foundation\Application;

use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\User\VacancyUserCreateRequest;
use App\Http\Requests\User\VacancyUserUpdateRequest;


class VacancyController extends Controller
{
    const PER_PAGE = 5;

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $vacancies = Vacancy::where(['published' => 1])->where('title', '!=', ' ');

        if ($request->get('search')) {
            $vacancies->when($request->get('id'), function ($query) use ($request) {
                if (intval($request->get('id')) == 0){
                    $query->where('id', '%$request->get("id")%');
                }else{

                    $query->where('id', $request->get('id'));
                }
            })
            ->when($request->get('country_id'), function ($query) use ($request) {
                $query->where('country_id', $request->get('country_id'));
            })
            ->when($request->get('region_id'), function ($query) use ($request) {
                $query->where('region_id', $request->get('region_id'));
            })
            ->when($request->get('city_id'), function ($query) use ($request) {
                $query->where('city_id', $request->get('city_id'));
            })
            ->when($request->get('accommodation'), function ($query) use ($request) {
                $vacancyIds = Accommodation::with('vacancies')
                    ->find($request->get('accommodation'))
                    ->map(function ($model) {
                        return $model->vacancies()->pluck('vacancy_id');
                    })->first();

                $query->whereIn('id', $vacancyIds);
            })
                ->when($request->get('language'), function ($query) use ($request) {
                    $vacancyIds = Language::with('vacancies')
                        ->find($request->get('language'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                })
                ->when($request->get('document'), function ($query) use ($request) {
                    $vacancyIds = Document::with('vacancies')
                        ->find($request->get('document'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                })
            ->when($request->get('position'), function ($query) use ($request) {
                $vacancyIds = Position::with('vacancies')
                    ->find($request->get('position'))
                    ->map(function ($model) {
                        return $model->vacancies()->pluck('vacancy_id');
                    })->first();

                $query->whereIn('id', $vacancyIds);
            });


        } else {


//            if ($request->get('accommodation')) {
//                if ($accommodation = Accommodation::with(['vacancies'])->find($request->get('accommodation'))) {
//                    $vacancies->whereIn('vacancy_id', $accommodation->vacancyIds);
//                }
//            }
//            if ($request->get('position')) {
//                $position = Position::with('vacancies')->find($request->get('position'));
////                dd($request->get('position'));
//                $vacancies->whereIn('vacancy_id ', $position->vacancyIds);
//            }

            $vacancies ->when($request->get('position'), function ($query) use ($request) {
                $vacancyIds = Position::with('vacancies')
                    ->find($request->get('position'))
                    ->map(function ($model) {
                        return $model->vacancies()->pluck('vacancy_id');
                    })->first();

                $query->whereIn('id', $vacancyIds);
            })
                ->when($request->get('accommodation'), function ($query) use ($request) {
                    $vacancyIds = Accommodation::with('vacancies')
                        ->find($request->get('accommodation'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                });
        }

        return view('vacancies.index', [
            'count' => $vacancies->count(),
            'vacancies' => $vacancies->orderByDesc('created_at')->paginate(self::PER_PAGE),
            'positions' => Position::all(),
            'countries' => Country::all(),
            'accommodations' => Accommodation::all(),
            'documents' => Document::all(),
            'languages' => Language::all(),
            'price' => PricePeriod::all(),
        ]);
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {

        return view('vacancies.create', [
            'accommodations'=> Accommodation::all(),
            'positions' => Position::all(),
            'prices'    => PricePeriod::all(),
            'languages' => Language::all(),
            'documents' => Document::getDocumentsByDivided(),
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'comments' => 'required|min:40',
            'position_id' => 'required|exists:positions,id',
//            'document_id' => 'required|int|exists:documents,id',
            'salary' => 'nullable|int',
            'price_period_id' => 'nullable|int|exists:price_periods,id',
            'published' => 'nullable|boolean',
        ]);

        if ($validator->fails()) {
            return redirect()->route('vacancy.create')
                ->with('danger',  __('vacancy.save.validate'))
                ->withErrors($validator)
                ->withInput($request->all());
        }

        $request->merge(['user_id' => auth()->user()->getAuthIdentifier()]);
        $vacancyId = Vacancy::create($request->only((new Vacancy())->getFillableFields()))->id;

        return redirect()->route('vacancy.edit', $vacancyId)
            ->with('success', __('vacancy.save.success'));
    }


    /**
     * @param $id
     * @return View|RedirectResponse
     */
    public function show($id)
    {
        if ($vacancy = Vacancy::with('accommodations', 'pricePeriod', 'country', 'city')->find($id)) {
            $comments = UsersComment::where('user_id', '=', $vacancy['user_id'])->get();
//            dd($comments);
            return view('vacancies.show', compact('vacancy', 'comments'));
        }

        return redirect()->route('home');
    }

    /**
     * @param $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit($id)
    {
        $vacancy = Vacancy::find($id);

        if ($vacancy && $vacancy->user_id == auth()->user()->getAuthIdentifier()) {
            return view('vacancies.edit', [
                'vacancy'   => $vacancy,
                'positions' => Position::all(),
                'prices'    => PricePeriod::all(),
                'languages' => Language::all(),
                'documents' => Document::getDocumentsByDivided(),
            ]);
        }

        return redirect()->route('vacancies.list');
    }


    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $vacancy = Vacancy::find($id);

        if ($vacancy && $vacancy->user_id == auth()->user()->getAuthIdentifier()) {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'comments' => 'required|min:40',
                'position_id' => 'required|exists:positions,id',
                'document_id' => 'required|int|exists:documents,id',
                'salary' => 'nullable|int',
                'price_period_id' => 'nullable|int|exists:price_periods,id',
                'published' => 'nullable|boolean',
            ]);

            if ($validator->fails()) {
                return redirect()->route('vacancy.edit', $id)
                    ->with('danger', __('vacancy.update.validate'))
                    ->withErrors($validator)
                    ->withInput($request->all());
            }

            $vacancy->update($request->only($request->only((new Vacancy())->getFillableFields())));

            return redirect()->route('vacancy.my')->with('success', __('vacancy.update.success'));
        }

        return redirect()->route('vacancies.list');
    }

    /**
     * @param Vacancy $vacancy
     * @return JsonResponse
     */
    public function destroy(Vacancy $vacancy): JsonResponse
    {
        if ($vacancy && $vacancy->user_id == auth()->user()->getAuthIdentifier()) {
            try {
                $vacancy->delete();
                return response()->json(['success' => true, 'message' => __('vacancy.delete.success')]);
            } catch (\Exception $exception) {
                return response()->json(['success' => false, 'message' => __('vacancy.delete.error')], 400);
            }
        }

        return response()->json(['success' => false, 'message' => __('vacancy.not_found')], 404);
    }

    /**
     * @param VacancyUserCreateRequest $createRequest
     * @return RedirectResponse
     */
    public function register(VacancyUserCreateRequest $createRequest): RedirectResponse
    {
        (new VacancyUserCreate($createRequest))->run();

        return redirect()->route('profile');
    }

    /**
     * @param VacancyUserUpdateRequest $updateRequest
     * @return RedirectResponse
     */
    public function updateProfile(VacancyUserUpdateRequest $updateRequest): RedirectResponse
    {
        (new VacancyUserUpdate(auth()->user()->getAuthIdentifier(), $updateRequest))->run();

        return redirect()->route('profile');
    }

    /**
     * @return Application|Factory|View
     */
    public function myVacancies()
    {
        $vacancies = Vacancy::where('user_id', auth()->user()->getAuthIdentifier())->paginate(self::PER_PAGE);
        return view('vacancies.my', compact('vacancies'));
    }




}
