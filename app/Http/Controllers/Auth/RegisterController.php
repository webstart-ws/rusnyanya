<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Accommodation;
use App\Models\City;
use App\Models\Country;
use App\Models\Document;
use App\Models\Language;
use App\Models\Position;
use App\Models\Education;
use App\Models\PricePeriod;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    /**
     * Show the registration form.
     *
     * @param Request $request
     * @return View|RedirectResponse
     */
    public function showRegistrationForm(Request $request)
    {
        if ($request->get('type') == User::ROLE_APPLICANT_VIEW) {
            $viewName = User::ROLE_APPLICANT_VIEW;
            $viewData = [
                'positions'        =>  Position::all(),
                'accommodations'   =>  Accommodation::all(),
                'languages'        =>  Language::all(),
                'price_period'     =>  PricePeriod::all(),
                'countries'        =>  Country::orderBy('name', 'asc')->get(),
                'educations'       =>  Education::all(),
            ];

            $viewData['documents'] = Document::getDocumentsByDivided();
        } else if ($request->get('type') == User::ROLE_VACANCY_VIEW) {
            $viewName = User::ROLE_VACANCY_VIEW;
            $viewData = [
                'infoMessage' => $request->has('auth_required') ? __('auth.auth_required') : ''
            ];
        } else {
            return redirect()->route('home');
        }

        return view("$viewName.user.register", $viewData);
    }

}
