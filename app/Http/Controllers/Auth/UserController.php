<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Education;
use App\Models\Applicant;
use App\Models\User;
use App\Models\PricePeriod;
use App\Models\UserImage;
use App\Models\UserAdminComments;
use App\Models\UsersComment;
use App\Models\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Services\User\ApplicantUserUpdate;
use App\Http\Requests\Questionnaire\UpdateRequest;


class UserController extends Controller
{
    const PER_PAGE = 20;

    public function profilePage()
    {
        $user = User::where('id', auth()->user()->id)->with('images', 'comments')->first();
//dd($this->getViewFolderName());
        return view("{$this->getViewFolderName()}.user.profile", compact('user'));
    }


    /**
     * Delete user account
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete()
    {
        try {
            $user = User::find(Auth::user()->id);
            Auth::logout();

            if ($user->delete()) {
                return response()->json(['success' => true, 'message' => __("auth.delete.success")]);
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['success' => false, 'message' => 'auth.delete.error'], 400);
    }


    /**
     * Delete user specific image
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage(Request $request, $id)
    {
        DB::beginTransaction();

        try {
            if ($userImage = UserImage::find($id)) {
                $imagePath = $userImage->path;
                if ($userImage->delete()) {
                    Storage::disk('public')->delete(substr($imagePath, 8));
                    DB::commit();
                    return response()->json(['success' => true, 'message' => __("auth.image.delete.success")]);
                }
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
            DB::rollBack();
        }

        return response()->json(['success' => false, 'message' => 'auth.image.delete.error'], 400);
    }

    public function payments()
    {
        return view("{$this->getViewFolderName()}.user.payments");
    }

    /**
     * Unsubscribe user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function subscribe(Request $request)
    {
        try {
            if ($request->has('subscribeValue')) {
                User::where('id', auth()->user()->id)
                    ->update(['subscriptions_enabled' => $request->get('subscribeValue')]);

                $key = $request->get('subscribeValue') == '1' ? '' : 'un';
                return response()->json(['success' => true, 'message' => __("auth.{$key}subscribe.success")]);
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['success' => false, 'message' => 'auth.subscribe.error'], 400);
    }


    /**
     * get auth user view folder name
     *
     * @return string
     */
    public function getViewFolderName(): string
    {
//        dd(auth()->user()->reg_type);
        return auth()->user()->reg_type == User::ROLES[User::ROLE_VACANCY]['id']
            ? User::ROLE_VACANCY_VIEW
            : User::ROLE_APPLICANT_VIEW;
    }

    public function getAllUsers()
    {
        $users = User::paginate(15);
        return view('admin.users.all', compact('users'));
    }

    public function edit($id) {
        if ($user = Applicant::with('accommodations', 'pricePeriod', 'country', 'city','author', 'images')->where('user_id', $id)->first()) {
            $education = Education::all();
            $pricePeriod = PricePeriod::all();
            return view('admin.users.edit', compact(
                'user',
                'education',
                'pricePeriod'
            ));
        } else {
            return redirect()->back()->with('error', 'User not found!');
        }
    }

    public function update($id, Request $request) {

        $applicant = Applicant::find($id);
        $applicant_data = [
            'comments' => $request->comments,
            'about' => $request->about,
            'salary' => $request->salary,
            'price_period_id' => $request->price_period_id,
            'driver_license' => $request->driver_license,
            'published' => $request->published,
            'in_search' => $request->in_search,
            'is_paid' => $request->is_paid,
            'speciality' => $request->speciality
        ];

        $applicant->update($applicant_data);
        $user_data = [
            'name' => $request->name,
            'fam' => $request->fam,
            'email' => $request->email,
            'phone' => $request->phone,
            'viber' => $request->viber,
            'whatsapp' => $request->whatsapp,
            'skype' => $request->skype,
            'education_id' => $request->education

        ];
        $applicant->author()->update($user_data);
        return redirect()->back();
    }

    public function get_applicants()
    {
        $applicants = Applicant::withAll()->paginate(self::PER_PAGE);
        return view('admin.users.applicants_all', compact('applicants'));
    }

    public function get_vacancy()
    {
        if ($vacancys = Vacancy::with('accommodations', 'pricePeriod', 'country', 'city')->paginate(self::PER_PAGE)) {
            // $comments = UsersComment::where('user_id', '=', $vacancy['user_id'])->get();
            return view('admin.users.vacancy_all', compact('vacancys'));
        }

    }

    public function edit_vacancy($id) {
        if ($user = Vacancy::with('accommodations', 'pricePeriod', 'author', 'country', 'city')->where('user_id', $id)->first()) {
            $education = Education::all();
            $pricePeriod = PricePeriod::all();
            return view('admin.users.vacancy_edit', compact(
                'user',
                'education',
                'pricePeriod'
            ));
        } else {
            return redirect()->back()->with('error', 'User not found!');
        }
    }

    public function update_vacancy($id, Request $request) {
        $vacancy = Vacancy::find($id);
        $vacancy_data = [
            'comments' => $request->comments,
            'salary' => $request->salary,
            'price_period_id' => $request->price_period_id,
            'driver_license' => $request->driver_license,
            'published' => $request->published,
            'is_paid' => $request->is_paid,
            'speciality' => $request->speciality,
            'is_top' => $request->is_top,
            'title' => $request->title,
        ];

        $vacancy->update($vacancy_data);
        $user_data = [
            'name' => $request->name,
            'fam' => $request->fam,
            'email' => $request->email,
            'phone' => $request->phone,
            'viber' => $request->viber,
            'whatsapp' => $request->whatsapp,
            'skype' => $request->skype,
            'education_id' => $request->education

        ];
        $vacancy->author()->update($user_data);
        return redirect()->back();
    }

    public function delete_vacancy($id)
    {
        dd($id);
    }

}
