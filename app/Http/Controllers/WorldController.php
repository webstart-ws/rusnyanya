<?php

namespace App\Http\Controllers;

use App\View\Components\CitiesSelect;
use App\View\Components\CountriesSelect;
use App\View\Components\RegionsSelect;
use Illuminate\Http\Request;

class WorldController extends Controller
{
    public function getCountriesSelect(Request $request)
    {
        return (new CountriesSelect($request->get('continentId')))->render();
    }

    public function getRegionsSelect(Request $request)
    {
        return (new RegionsSelect($request->get('countryId')))->render();
    }

    public function getCitiesSelect(Request $request)
    {
        return (new CitiesSelect($request))->render();
    }


}
