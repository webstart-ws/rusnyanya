<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questionnaire\CreateRequest;
use App\Http\Requests\Questionnaire\UpdateRequest;
use App\Models\Accommodation;
use App\Models\Applicant;
use App\Models\Country;
use App\Models\Document;
use App\Models\Favorite;
use App\Models\Language;
use App\Models\Position;
use App\Models\PricePeriod;
use App\Models\User;
use App\Models\Education;
use App\Services\User\ApplicantUserCreate;
use App\Services\User\ApplicantUserUpdate;
use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class QuestionnairesController extends Controller
{
    const PER_PAGE = 5;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $applicants = Applicant::with('author')->where(['published' => 1, 'in_search' => 1]);

        if ($request->get('search')) {
            $applicants->when($request->get('id'), function ($query) use ($request) {
                if (intval($request->get('id')) == 0) {
                    $query->where('comments', 'LIKE', '%' . $request->get("id") . '%');
                } else {

                    $query->where('id', $request->get('id'));
                }
            })
                ->when($request->get('country_id'), function ($query) use ($request) {
                    $query->where('country_id', $request->get('country_id'));
                })
                ->when($request->get('region_id'), function ($query) use ($request) {
                    $query->where('region_id', $request->get('region_id'));
                })
                ->when($request->get('city_id'), function ($query) use ($request) {
                    $query->where('city_id', $request->get('city_id'));
                })
                ->when($request->get('driver-license'), function ($query) use ($request) {
                    $query->where('driver_license', 1);
                })
                ->when($request->get('recommendations'), function ($query) use ($request) {
                    $query->where('recommendations', 1);
                })

                ->when($request->get('from-age'), function ($query) use ($request) {

                    $from = date('Y') - $request->get('from-age');
                    $from = $from . '-01-01 00:00:00';

                    $to = date('Y') - $request->get('before-age');
                    $to = $to . '-12-31 00:00:00';

                    $query->whereHas('author', function( $q) use ($from,$to) {
//
                        $q->whereDate('birthdate', '>=', $to)
                            ->whereDate('birthdate', '<=', $from);

//                        EXTRACT('Y', 'birthdate'), [$from-$this_data,$to-$this_data]
                    });
                })

                ->when($request->get('accommodation'), function ($query) use ($request) {
                    $vacancyIds = Accommodation::with('vacancies')
                        ->find($request->get('accommodation'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                })
                ->when($request->get('language'), function ($query) use ($request) {
                    $vacancyIds = Language::with('vacancies')
                        ->find($request->get('language'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                })
                ->when($request->get('document'), function ($query) use ($request) {
                    $vacancyIds = Document::with('vacancies')
                        ->find($request->get('document'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                })
                ->when($request->get('position'), function ($query) use ($request) {
                    $vacancyIds = Position::with('vacancies')
                        ->find($request->get('position'))
                        ->map(function ($model) {
                            return $model->vacancies()->pluck('vacancy_id');
                        })->first();

                    $query->whereIn('id', $vacancyIds);
                });

        }else{
            $applicants->when($request->get('views'), function ($query) use ($request) {
                $query->whereHas('author', function( $q){
                    $q->orderByDesc('num_views');
                });
            })
                ->when($request->get('image'), function ($query) use ($request) {
                    $query->whereHas('author', function( $q){
                        $q->where('fname', '!=', ' ');
                    });
                })
                ->when($request->get('active'), function ($query) use ($request) {
                    $query->orderByDesc('updated_at')
                        ->whereHas('author', function( $q){
                        $q->orderByDesc('dt_last_visit');
                    });
                })
                ->when($request->get('city'), function ($query) use ($request) {


                    $query->where('city_of_work_id', $request->get('city'));
                })
                ->when($request->get('open'), function ($query) use ($request) {
                    $query->where('is_paid', 1);
                });
        }

        return view('questionnaires.index', [
            'count'          =>   $applicants->count(),
            'applicants'     =>   $applicants->paginate(self::PER_PAGE),
            'positions'      =>   Position::all(),
            'countries'      =>   Country::all(),
            'accommodations' =>   Accommodation::all(),
            'documents'      =>   Document::all(),
            'languages'      =>   Language::all(),
            'price'          =>   PricePeriod::all(),
            'favoriteIds'    =>   auth()->user() ? auth()->user()->favoriteIds : [],
        ]);
    }

    /**
     * @param $id
     * @return View|RedirectResponse
     */
    public function show($id)
    {
        if ($applicant = Applicant::with('accommodations', 'pricePeriod', 'country', 'city','author', 'images')->find($id)) {
            $education = Education::find($applicant->author->education_id);
            return view('questionnaires.show', compact('applicant', 'education'));
        }

        // if ($applicant = Applicant::with('education', 'accommodations', 'pricePeriod', 'country', 'city','author')->find($id)) {
        //     $view_data['applicant'] = $applicant;
        //     $view_data['favoriteIds'] = auth()->user() ? auth()->user()->favoriteIds : [];
        //     return view('questionnaires.show', $view_data);
        // }

        return redirect()->route('home');
    }

    /**
     * @param CreateRequest $createRequest
     * @return RedirectResponse
     */
    public function register(CreateRequest $createRequest): RedirectResponse
    {

        (new ApplicantUserCreate($createRequest))->run();

        return redirect()->route('profile');
    }

    public function edit($id)
    {
        $vacancy = Applicant::find($id);

        if ($vacancy && $vacancy->user_id == auth()->user()->getAuthIdentifier()) {
            return view('vacancies.edit', [
                'vacancy'   => $vacancy,
                'positions' => Position::all(),
                'prices'    => PricePeriod::all(),
                'languages' => Language::all(),
                'documents' => Document::getDocumentsByDivided(),
            ]);
        }

        return redirect()->route('vacancies.list');
    }

    public function store()
    {
        // TODO: create store functional for questionnaire
    }

    /**
     * @param UpdateRequest $updateRequest
     * @return RedirectResponse
     */
    public function updateProfile(UpdateRequest $updateRequest): RedirectResponse
    {
        (new ApplicantUserUpdate(auth()->user()->getAuthIdentifier(), $updateRequest))->run();

        return redirect()->route('profile');
    }

    public function addFavorite(Request $request)
    {
        try {
            if ($request->get('userId')) {
                $favorite = Favorite::where([
                    'uid_v' => auth()->user()->getAuthIdentifier(),
                    'uid_a' => $request->get('userId')
                ])->first();

                if ($favorite) {
                    $favorite->delete();
                    return response()->json(['success' => true, 'message' => 'Favorite deleted successfully']);
                } else {
                    Favorite::create([
                        'uid_v' => auth()->user()->getAuthIdentifier(),
                        'uid_a' => $request->get('userId'),
                        'dt' => Carbon::now()
                    ]);
                    return response()->json(['success' => true, 'message' => 'Favorite assigned successfully']);
                }
            }
        } catch (\Exception $exception) {
            return response()->json(['success' => false, 'message' => $exception->getMessage()], 400);
        }

        return response()->json(['success' => true, 'message' => 'Please send assigning favorite userId'], 422);
    }

    public function myQuestionnaires($id)
    {
        $applicant = Applicant::with(
            'accommodations',
            'pricePeriod',
            'country',
            'city',
            'author',
            'images',
            'countryResidence',
            'cityResidence',
            'districtResidence',
            'district',
            'positions',
            'languages',
            'documents',)
            ->where('user_id', $id)->first();
//        dd($applicant->author);
        return view('questionnaires.user.edit', [
            'applicant'   => $applicant,
            'positions'        =>  Position::all(),
            'accommodations'   =>  Accommodation::all(),
            'languages'        =>  Language::all(),
            'price_period'     =>  PricePeriod::all(),
            'countries'        =>  Country::orderBy('name', 'asc')->get(),
            'educations'       =>  Education::all(),
            'documents' => Document::getDocumentsByDivided(),
        ]);
    }

    public function  editProfile(Request  $request)
    {
        dd($request);
    }

}
