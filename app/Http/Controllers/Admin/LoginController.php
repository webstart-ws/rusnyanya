<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
        $this->middleware('auth:admin')->only('logout');
    }

    public function showLoginForm()
    {
        return view('admin.auth.login');
    }

    /**
     * Login the admin to the system
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        /* try to login with request parameters */
        if (Auth::guard('admin')->attempt($this->credentials($request), $request->get('remember'))) {
            return $request->ajax()
                ? response()->json(['success' => true, 'redirect_url' => route('dashboard')])
                : redirect()->intended('/admin');
        }

        $errors = [
            'message' => 'These credentials do not match our records'
        ];

        /* return failed login response */
        return $request->ajax()
            ? response()->json(['success' => false, 'errors' => $errors], 401)
            : back()->withInput($request->only('email', 'remember'))->withErrors($errors);
    }

    /**
     * Validate the admin login request.
     *
     * @param Request $request
     * @return JsonResponse|RedirectResponse
     */
    protected function validateLogin(Request $request)
    {
        $validator = Validator::make($request->only('email', 'password'), [
            'email'    => 'required|email',
            'password' => 'required|min:6'
        ]);

        /* return validation errors if something is wrong */
        if ($validator->fails()) {
            return $request->ajax()
                ? response()->json(['success' => false, 'errors' => $validator->errors()], 422)
                : redirect()->route('admin.login')->withInput($request->only(['email', 'remember']))->withErrors($validator->errors());
        }
    }

    /**
     * Log the admin out of the application.
     *
     * @param  Request  $request
     * @return RedirectResponse|JsonResponse
     */
    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        $message = 'You are successfully logged out';
        return $request->ajax()
            ? response()->json(['success' => 'success', 'message' => $message], 204)
            : redirect()->intended('/admin')->with('success', $message);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  Request $request
     * @return array
     */
    protected function credentials(Request $request): array
    {
        return $request->only('email', 'password');
    }
}
