<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Validator;

class NewsController extends Controller
{
    const PER_PAGE = 5;
    public function index()
    {
//        $news = News::latest()->paginate(self::PER_PAGE);
        $news = News::paginate(self::PER_PAGE);
        return view('news.index', [
            'news' => $news,
        ]);
    }

    public function all() {
        $news = News::paginate(15);
        return view('admin.news.all', [
            'news' => $news,
        ]);
    }

    public function edit($id) {
        $post = News::find($id);
        if (!$post)
            abort(404);
        return view('admin.news.edit', compact('post'));
    }

    public function update($id, Request $request) {
        $post = News::find($id);
        if (!$post)
            abort(404);

        $validator = Validator::make($request->all(), [
            'title' => 'string',
            'header_image' => 'required|image|mimes: png,jpeg,jpg,gif|max: 2048',
            'dt' => 'string',
            'name' => 'string',
            'komm' => 'string',
            'main_komm' => 'string'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

            $originalName = $request->file('header_image')->getClientOriginalName();
            $fileName = pathinfo($originalName, PATHINFO_FILENAME);
            $extension = $request->file('header_image')->getClientOriginalExtension();
            $fileName = $fileName . '_' . md5(microtime()) . $extension;

            $request->file('header_image')->move(public_path('news_images/'), $fileName);

            $request['fname1'] = $fileName;
            $request['pref'] = str_replace("-", " ", $request->title);

            $news = News::create($request->except('header_image'));
            return redirect()->back()->with('success', 'Post Updated!');
    }
    public function uploadImage(Request $request) {
        if ($request->hasFile('upload')) {
            $originalName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originalName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . md5(microtime()) . $extension;

            $request->file('upload')->move(public_path('news'), $fileName);

            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('/news/' . $fileName);
            $msg = 'Image Uploaded Successfully';
            $response = "<script> window.parent.CKEDITOR.tools.callFunction( $CKEditorFuncNum, '$url', '$msg')</script>";
            @header('Content-type: text/html; charset=utf-8');
            echo $response;
        }
    }
    public function delete($id) {
        $post = News::find($id);
        if ($post) {
            $news = News::destroy($id);
            return redirect()->back()-with('success', 'Post is deleted!');
        }
        return redirect()->back()->with('fail', 'Post not found!');
    }

    public function add() {
        return view('admin.news.create');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function show($id)
    {
        if ($news = News::find($id)) {
            return view('news.show', compact('news'));
        }

        return redirect()->route('home');
    }
}
