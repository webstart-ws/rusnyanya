<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Country;
use App\Models\City;
use App\Models\PricePeriod;
use App\Models\Position;
use App\Models\Accommodation;
use App\Models\Document;
use App\Models\Language;

class MainController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function english()
    {
        return view('english');
    }

    public function about()
    {
        return view('about');
    }

    public function contacts()
    {
        return view('contacts');
    }

    public function howItWork()
    {
        return view('how-it-works');
    }
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function check_in()
    {
        return view('check_in', [

            'position' => Position::all(),
            'countries' => Country::all(),
            'cities' => City::all(),
            'accom' => Accommodation::all(),
            'doc' => Document::all(),
            'language' => Language::all(),
            'price' => PricePeriod::all(),
        ]);
    }

    public function check_in_()
    {
        return view('check_in_');
    }

}
