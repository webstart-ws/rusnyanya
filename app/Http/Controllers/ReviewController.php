<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;
use Validator;

class ReviewController extends Controller
{
    public function add() {
        return view('admin.reviews.add');
    }

    public function create(Request $request) {
        $validate = Validator::make($request->all(), [
            'photo' => 'required|image|mimes:jpg,jpeg,png',
            'name' => 'required|string',
            'review' => 'required|string'
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate);
        }


        Review::create($request->all());
        return redirect()->back()->withErrors($validate);
    }

    public function all() {
        $reviews = Review::all();
        return view('admin.reviews.all', compact('reviews'));
    }

    public function uploadImage($image) {

    }

    public function edit($id) {
        $review = Review::find($id);
        if (!$review)
            abort(404);
        return view('admin.reviews.edit', compact('review'));    
    }

    public function delete($id) {
        Review::destroy($id);
        return redirect()->back();
    }
}
