<?php

namespace App\Http\Controllers;

use App\Models\Applicant;
use App\Models\ApplicantPlace;
use App\Models\Vacancy;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\JobType;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $questionnaires = Applicant::with('accommodations', 'pricePeriod', 'country', 'city','author', 'images')
            ->where('speciality', '!=', ' ')
            ->where('price_period_id', '!=', ' ')
            ->orderByDesc('updated_at')
            ->take(4)
            ->get();
//dd($questionnaires);
        $vacancies =  Vacancy::with('accommodations', 'pricePeriod', 'country', 'city')
            ->where('title', '!=' ,' ')
            ->where('price_period_id', '!=', ' ')
            ->orderByDesc('updated_at')
            ->orderByDesc('number_views')
            ->take(4)
            ->get();

        return view('home', compact('questionnaires', 'vacancies'));
    }

    public function getJobType()
    {
        $job_type = JobType::all();
        $applicant_place = ApplicantPlace::all()->toArray();

        if ($job_type != [])
        {
            $view = view('inc.footer-jobs-links', compact('job_type', 'applicant_place'));
            return response()->json(['success' => true, 'html' => $view->render()]);
        }
        else{
           return response()->json(['success' => false]);
        }




    }
}
