<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Tariff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TariffsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $low = Tariff::LOW;
        $medium = Tariff::MEDIUM;
        $high = Tariff::HIGH;

        DB::statement("SET foreign_key_checks=0");
        DB::table('tariffs')->truncate();
        DB::statement("SET foreign_key_checks=1");

        $first_group_countries = [118, 119, 120, 126, 133, 134, 135, 136, 137, 139, 142, 156, 157, 159, 161];
        $second_group_countries = [121, 123, 130, 131, 132, 146, 151, 153, 154, 155, 162, 248];
        $third_group_countries = [82, 122, 124, 125, 128, 129, 141, 144, 147, 148, 149, 150, 152, 158, 160];

        $low_tariff_first = Tariff::create(['id' => 1, 'type' => $low, 'cost' => 11]);
        $low_tariff_first->countries()->attach($first_group_countries);

        $low_tariff_second = Tariff::create(['id' => 2, 'type' => $low, 'cost' => 16]);
        $low_tariff_second->countries()->attach($first_group_countries);

        $low_tariff_third = Tariff::create(['id' => 3, 'type' => $low, 'cost' => 24]);
        $low_tariff_third->countries()->attach($first_group_countries);


        $medium_tariff_first = Tariff::create(['id' => 4, 'type' => $medium, 'cost' => 40]);
        $medium_tariff_first->countries()->attach($second_group_countries);

        $medium_tariff_second = Tariff::create(['id' => 5, 'type' => $medium, 'cost' => 50]);
        $medium_tariff_second->countries()->attach($second_group_countries);

        $medium_tariff_third = Tariff::create(['id' => 6, 'type' => $medium, 'cost' => 70]);
        $medium_tariff_third->countries()->attach($second_group_countries);


        $high_tariff_first = Tariff::create(['id' => 7, 'type' => $high, 'cost' => 210]);
        $high_tariff_first->countries()->attach($third_group_countries);

        $high_tariff_second = Tariff::create(['id' => 8, 'type' => $high, 'cost' => 265]);
        $high_tariff_second->countries()->attach($third_group_countries);

        $high_tariff_third = Tariff::create(['id' => 9, 'type' => $high, 'cost' => 390]);
        $high_tariff_third->countries()->attach($third_group_countries);
    }
}
