<?php

namespace Database\Seeders;

use App\Models\ApplicantPlace;
use Illuminate\Database\Seeder;

class ApplicantPlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApplicantPlace::create([
            'id' => 1,
            'name' => 'Няня в Амстердаме',
            'city_id' => 864,

        ]);
        ApplicantPlace::create([
            'id' => 2,
            'name' => 'Няня в Афинах',
            'city_id' => 2338

        ]);
        ApplicantPlace::create([
            'id' => 3,
            'name' => 'Няня в Берлине',
            'city_id' => 523

        ]);
        ApplicantPlace::create([
            'id' => 4,
            'name' => 'Няня в Братиславе',
            'city_id' => 1834,

        ]);
        ApplicantPlace::create([
            'id' => 5,
            'name' => 'Няня в Сараево',
            'city_id' => 418,

        ]);
        ApplicantPlace::create([
            'id' => 6,
            'name' => 'Няня в Будапешт',
            'city_id' => 2368,

        ]);
        ApplicantPlace::create([
            'id' =>7,
            'name' => 'Няня в Бухаресте',
            'city_id' => 1261,

        ]);
        ApplicantPlace::create([
            'id' => 8,
            'name' => 'Няня в Варшаве',
            'city_id' => 358,

        ]);
        ApplicantPlace::create([
            'id' => 9,
            'name' => 'Няня в Вене',
            'city_id' => 214,

        ]);
        ApplicantPlace::create([
            'id' => 10,
            'name' => 'Няня в Вильнюсе',
            'city_id' => 1231,

        ]);
        ApplicantPlace::create([
            'id' => 11,
            'name' => 'Няня в Дублине',
            'city_id' => 143,

        ]);
        ApplicantPlace::create([
            'id' => 12,
            'name' => 'Няня в Загребе',
            'city_id' => 1090,

        ]);
        ApplicantPlace::create([
            'id' => 13,
            'name' => 'Няня в Копенгагене',
            'city_id' => 510,

        ]);
        ApplicantPlace::create([
            'id' => 14,
            'name' => 'Няня в Киев',
            'city_id' => 2229,

        ]);
        ApplicantPlace::create([
            'id' => 15,
            'name' => 'Няня в Лиссабоне',
            'city_id' => 1619,

        ]);
        ApplicantPlace::create([
            'id' => 16,
            'name' => 'Няня в Лондон',
            'city_id' => 3732,

        ]);
        ApplicantPlace::create([
            'id' => 17,
            'name' => 'Няня в Мадрид',
            'city_id' => 2311,

        ]);
        ApplicantPlace::create([
            'id' => 18,
            'name' => 'Няня в Минск',
            'city_id' => 303,

        ]);
        ApplicantPlace::create([
            'id' => 19,
            'name' => 'Няня в Москва',
            'city_id' => 627,

        ]);
        ApplicantPlace::create([
            'id' => 20,
            'name' => 'Няня в Париж',
            'city_id' => 695,

        ]);
        ApplicantPlace::create([
            'id' => 21,
            'name' => 'Няня в Рим',
            'city_id' => 2492,

        ]);
        ApplicantPlace::create([
            'id' => 22,
            'name' => 'Няня в Рига',
            'city_id' => 1160,

        ]);
        ApplicantPlace::create([
            'id' => 23,
            'name' => 'Няня в Стокгольм',
            'city_id' => 1696,

        ]);
        ApplicantPlace::create([
            'id' => 24,
            'name' => 'Няня в Хельсинки',
            'city_id' => 723,

        ]);


    }
}
