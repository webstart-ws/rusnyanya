<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $email = 'super@admin.com';
        $admin = Admin::where('email', $email)->first();

        if (!$admin) {
            Admin::create([
                'name'     => 'super admin',
                'email'    => $email,
                'password' => 'test1234',
            ]);
        }
    }
}
