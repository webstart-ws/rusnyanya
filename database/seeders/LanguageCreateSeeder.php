<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageCreateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Language::create([
           'id' => 17077,
           'name' => 'Русский',


       ]);
        Language::create([
            'id' => 17078,
            'name' => 'Украинский',

        ]);
    }
}
