<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\JobType;

class JobTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        JobType::create([
            'id' => 1,
            'name' => 'Работа няней',
            'id_positions' => 38,

        ]);
        JobType::create([
            'id' => 2,
            'name' => 'Работа сиделкой',
            'id_positions' => 39,

        ]);
        JobType::create([
            'id' => 3,
            'name' => 'Работа репетитором',
            'id_positions' => 40,

        ]);
        JobType::create([
            'id' => 4,
            'name' => 'Работа домработницей',
            'id_positions' => 41,

        ]);


    }
}
