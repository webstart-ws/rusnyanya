<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;

class RemoveUselessCountries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $delete_ids = array(120, 143, 155, 151, 131, 132, 146, 123, 130, 153, 121, 154, 248, 129, 128, 150, 141, 125, 148, 149, 147, 152, 122, 144, 124, 158, 82, 162, 160, 156, 157, 161, 139, 138, 133, 126, 159, 137, 135, 134, 136, 118, 119, 142);
        Country::whereNotIn('id', $delete_ids)->delete();
    }
}
