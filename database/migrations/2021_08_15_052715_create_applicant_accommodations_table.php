<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_accommodations', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('applicant_id');
            $table->foreign('applicant_id')
                ->references('id')->on('applicants')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('accommodation_id');
            $table->foreign('accommodation_id')
                ->references('id')->on('accommodations')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicant_accommodations');
    }
}
