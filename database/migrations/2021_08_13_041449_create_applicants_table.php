<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('applicant');

//        Schema::table('applicant_language', function (Blueprint $table) {
//            $table->dropForeign('FK_10CC7EA897139001');
//        });

        Schema::dropIfExists('applicants');

        Schema::create('applicants', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->text('comments');
            $table->text('about');

            $table->string('salary');
            $table->integer('price_period_id');

            $table->unsignedBigInteger('country_of_work_id');
            $table->unsignedBigInteger('district_of_work_id');
            $table->unsignedBigInteger('city_of_work_id');

            $table->unsignedBigInteger('country_of_residence_id');
            $table->unsignedBigInteger('district_of_residence_id');
            $table->unsignedBigInteger('city_of_residence_id');

            $table->string('schedule_mon', 15)->comment('graph_1');
            $table->string('schedule_tue', 15)->comment('graph_2');
            $table->string('schedule_wed', 15)->comment('graph_3');
            $table->string('schedule_thu', 15)->comment('graph_4');
            $table->string('schedule_fri', 15)->comment('graph_5');
            $table->string('schedule_sat', 15)->comment('graph_6');
            $table->string('schedule_sun', 15)->comment('graph_7');

            $table->string('speciality');
            $table->integer('recommendations')->default(0);
            $table->boolean('driver_license')->default(0);
            $table->boolean('published')->default(1);
            $table->boolean('in_search')->default(1);
            $table->boolean('is_paid');
            $table->boolean('is_top');
            $table->timestamps();

//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onUpdate('cascade')->onDelete('cascade');
//
//            $table->foreign('price_period_id')
//                ->references('id')->on('price_periods')
//                ->onUpdate('cascade')->onDelete('set null');
//
//            $table->foreign('country_of_work_id')
//                ->references('id')->on('world_countries')
//                ->onUpdate('cascade')->onDelete('set null');
//            $table->foreign('district_of_work_id')
//                ->references('id')->on('world_divisions')
//                ->onUpdate('cascade')->onDelete('set null');
//            $table->foreign('city_of_work_id')
//                ->references('id')->on('world_cities')
//                ->onUpdate('cascade')->onDelete('set null');
//
//            $table->foreign('country_of_residence_id')
//                ->references('id')->on('world_countries')
//                ->onUpdate('cascade')->onDelete('set null');
//            $table->foreign('district_of_residence_id')
//                ->references('id')->on('world_divisions')
//                ->onUpdate('cascade')->onDelete('set null');
//            $table->foreign('city_of_residence_id')
//                ->references('id')->on('world_cities')
//                ->onUpdate('cascade')->onDelete('set null');
        });

//        Schema::table('applicant_language', function (Blueprint $table) {
//            $table->foreign('applicant_id', 'FK_10CC7EA897139001')
//            ->references('id')->on('applicants')
//            ->onUpdate('cascade')->onDelete('cascade');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
