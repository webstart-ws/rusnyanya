<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacancyAccommodationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancy_accommodations', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('vacancy_id');
            $table->foreign('vacancy_id')
                ->references('id')->on('vacancies')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('accommodation_id');
            $table->foreign('accommodation_id')
                ->references('id')->on('accommodations')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->tinyInteger('agroup')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancy_accommodations');
    }
}
