<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixesInVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            $table->dropColumn(['position_id', 'document_id']);
            $table->integer('number_views')->after('published')->default(0);
            $table->boolean('is_paid')->after('published')->default(0);
            $table->boolean('is_top')->after('published')->default(0);
            $table->boolean('recommendations')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vacancies', function (Blueprint $table) {
            $table->integer('position_id')->comment('Должность');
            $table->integer('document_id')->nullable();
            $table->dropColumn(['number_views', 'is_paid', 'is_top']);
        });
    }
}
