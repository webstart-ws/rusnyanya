<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountryTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('country_tariffs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')
                ->references('id')->on('world_countries')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('tariff_id');
            $table->foreign('tariff_id')
                ->references('id')->on('tariffs')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('country_tariffs', function (Blueprint $table) {
            $table->dropForeign(['country_id']);
            $table->dropForeign(['tariff_id']);
        });
        Schema::dropIfExists('country_tariffs');
    }
}
