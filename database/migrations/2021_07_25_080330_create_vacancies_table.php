<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->id();
            $table->string('title')->comment('spec');

            $table->integer('user_id');
//            $table->foreign('user_id')
//                ->references('id')->on('users')
//                ->onDelete("cascade")
//                ->onUpdate('cascade');

            $table->integer('position_id')->comment('Должность');
//            $table->foreign('position_id')
//                ->references('id')->on('positions')
//                ->onDelete("no action")
//                ->onUpdate('cascade');

            $table->integer('document_id')->nullable();
            $table->boolean('driver_license')->default(0);

            $table->string('salary')->nullable();
            $table->integer('price_period_id')->nullable();
//            $table->foreign('price_period_id')
//                ->references('id')->on('price_period')
//                ->onDelete("cascade")
//                ->onUpdate('cascade');

            $table->integer('recommendations')->nullable();
            $table->text('comments')->nullable();

            $table->integer('country_id')->nullable();
            $table->integer('region_id')->nullable();
            $table->integer('city_id')->nullable();

            $table->boolean('published');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
