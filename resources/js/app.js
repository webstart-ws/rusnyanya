require('./bootstrap');

'use strict'

$(document).ready(function () {

    /*общая функция события resize*/
    $(window).on('resize load', function () {
        let thsWidth = $(this).innerWidth();
        headerReseze(thsWidth);
        footerReseze(thsWidth);
    });

    /*popup*/
    function openPopup(id) {
        $(".js-popup[data-id-popup='" + id + "']").fadeIn(100);
    }

    function close_popup() {
        $('.js-popup').fadeOut(350);
    }

    $('.js-popup__close').click(close_popup);

    $('.js-btn-popup').click(function (e) {
        e.preventDefault();
        // $('.js-popup').fadeOut(300);
        let index_btn_popup = $(this).attr('href');
        openPopup(index_btn_popup);
    });

    $('.js-popup').click(function (e) {
        let popup = $('.js-popup__wrapp');
        if (!popup.is(e.target) && popup.has(e.target).length === 0)
            close_popup();
    });

    /*шапка сайта*/
    const $MENU = $('.js-menu');
    const $MOB_RATING = $('.js-mob-rating');
    const $MOB_LOGIN_REGISTRATION = $('.js-mob-login-registration');
    const $MOB_MENU = $('.js-mob-menu');
    const $BURGER = $('.js-burger-menu');
    const $HEADER_ROW = $('.js-header-row');
    const $BODY_OPAS = $('.js-boyd--opas');
    const $USER_HE = $('.js-header-user');

    function headerReseze(thsWidth) {
        if (thsWidth <= 992) {
            $MOB_MENU.prepend($MENU);
        } else {
            $HEADER_ROW.append($MENU);
        }

        if (thsWidth <= 768) {
            $MOB_MENU.prepend($MOB_RATING);
            $MOB_MENU.prepend($MOB_LOGIN_REGISTRATION);
            $MOB_MENU.prepend($USER_HE);
        } else {
            $HEADER_ROW.append($MOB_RATING);
            $HEADER_ROW.append($MOB_LOGIN_REGISTRATION);
            $HEADER_ROW.append($USER_HE);
        }
    };

    function removeMob() {
        $MOB_MENU.removeClass('mob-menu_open');
        $BODY_OPAS.fadeOut(300);
        $('body').removeClass('body-fix');
    }

    //показать закрыть меню
    $BURGER.click(function () {
        if (!$(this).hasClass('burger-filter')) {
            if (!$(this).hasClass('burger-menu_active')) {
                $(this).toggleClass('burger-menu_active');
                $MOB_MENU.addClass('mob-menu_open');
                $BODY_OPAS.fadeIn(300);
                $('body').addClass('body-fix');
            } else {
                removeMob();
                $(this).removeClass('burger-menu_active');
            }
        } else {
            removeFilter();
        }
    });

    $('.js-boyd--opas').click(function () {
        $BURGER.toggleClass('burger-menu_active');
        removeMob();
        removeFilter();
    });

    //mob filter
    const BTN_FILTER = $('.js-bnt-filter');
    const WR_FILTER = $('.js-wr-filters');

    function removeFilter() {
        WR_FILTER.removeClass('active');
        BTN_FILTER.removeClass('active');
        $BODY_OPAS.fadeOut(300);
        $('body').removeClass('body-fix');
        $BURGER.removeClass('burger-menu_active burger-filter');
    }

    BTN_FILTER.click(function (e) {
        e.preventDefault();

        let ths = $(this);

        if (!ths.hasClass('active')) {
            ths.addClass('active');
            WR_FILTER.addClass('active');
            $BODY_OPAS.fadeToggle(300);
            $('body').toggleClass('body-fix');
            $BURGER.addClass('burger-menu_active burger-filter');
        } else {
            removeFilter();
        }
    });

    /*подвал сайта*/
    const $FOOTER_TOP_ROW = $('.js-footer-top-mob');
    const $FOOTER_LOGO = $('.js-footer-logo');
    const $FOOTER_SOCIAL = $('.js-footer-social');
    const $FOOTER_BOTOM = $('.js-footer-bottom');
    const $FOOTER_WR_LOGO = $('.js-wr-logo');
    const $FOOTER_WR_SOCIAL = $('.js-wr-social');

    function footerReseze(thsWidth) {
        if (thsWidth <= 480) {
            $FOOTER_TOP_ROW.append($FOOTER_LOGO);
            $FOOTER_TOP_ROW.append($FOOTER_SOCIAL);
        } else {
            $FOOTER_WR_LOGO.prepend($FOOTER_LOGO);
            $FOOTER_WR_SOCIAL.append($FOOTER_SOCIAL);
        }
    }

    /*select*/
    const $S_MAIN = $('.js-select-main');
    const S_ACTIVE = 'el-select_active';
    const $SELECT = $('.js-select');
    const $S_CONST = $('.js-el-select-content');
    const $S_ITEM = $('.js-select-item');

    // function selectClose() {
    //     $SELECT.removeClass(S_ACTIVE);
    //     $S_CONST.slideUp(300);
    // }

    // $S_MAIN.click(function (e) {
    //     let ths = $(this);
    //     let clos = ths.closest('.js-select');

    //     if (!clos.hasClass(S_ACTIVE)) {
    //         selectClose();
    //         clos.addClass(S_ACTIVE);
    //         clos.find('.js-el-select-content').slideDown(300);
    //     } else {
    //         selectClose();
    //     }
    // });

    $S_ITEM.click(function () {
        let ths = $(this);
        let clos = ths.closest('.js-select');
        let val = ths.attr('data-value');

        if (val.includes('Не выбрано')) {
            clos.addClass('l-select__not_selected');
        } else {
            clos.removeClass('l-select__not_selected');
        }

        clos.find('.el-select__item').removeClass('el-select__item_active');
        ths.addClass('el-select__item_active');

        clos.find('.js-select-text').text(val);
        clos.find('.js-select-text').attr('data-value', val);

        // selectClose();

    });

    // $(document).mouseup(function (e) {
    //     if (!$S_MAIN.is(e.target) && $S_MAIN.has(e.target).length == 0) {
    //         selectClose();
    //     }
    // });

    /*show more*/
    let s_text = null;
    $('.js-fil-list-all').click(function (e) {
        e.preventDefault();
        console.log('resources/app.js');

        let ths = $(this);
        let clos = ths.closest('.js-filter-list');
        let text = ths.find('span');


        if (!ths.hasClass('active')) {
            ths.addClass('active');
            s_text = text.text();
            clos.find('.js-fil-list').slideDown(300);
            text.text(ths.attr('data-text'));
        } else {
            ths.removeClass('active');
            clos.find('.js-fil-list').slideUp(300);
            text.text(s_text);
        }
    });


    /*validation*/
    const $FORM_VALID = $('.js-form-validation');

    class Validation {
        // конструктор класса
        constructor(form, email, valIn, password, textarea, scheck) {
            this.form = form;
            this.re_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/; // регулярное выражеине email
            this.email = email; // input email
            this.valIn = valIn; // input pole
            this.password = password; // поля паролей
            this.textarea = textarea;
            this.scheck = scheck;
        }

        // проверка есть ли поле email
        set email(value) {
            if (value.length > 0) {
                this._email = value;
                this.em = true;
            }
        }

        set valIn(value) {
            if (value.length > 0) {
                this._valIn = value;
                this.vl = true;
            }
        }

        set password(value) {
            if (value.length > 0) {
                this._password = value;
                this.ps = value;
            }
        }

        set textarea(value) {
            if (value.length > 0) {
                this._textarea = value;
                this.te = true;
            }
        }

        set scheck(value) {
            if (value.length > 0) {
                this._scheck = value;
                this.sh = true;
            }
        }

        // метод вывода текста ошибки
        error(elem, text) {
            let divElem = elem.closest('.js-el-form');

            if (divElem) {
                divElem.innerHTML += '<div class="el-error">' + text + '</div>';
            }
        }

        // метод очистки ошибок
        closeError() {
            if (this.form.find('.el-error').length > 0) {
                this.form.find('.el-error').remove();
            }
        }

        // метод удаление класса in-error с полей input
        removeClass(el_input) {
            el_input.removeClass('in-error');
        }

        // метод проверки на валидаци
        valid() {
            let valid = 0;

            // проверка полей email
            if (this.em) {
                for (let i = 0; i < this._email.length; i++) {
                    if (this.re_email.test(this._email[i].value)) {
                        this._email[i].classList.remove('in-error');
                    } else {
                        valid++;
                        this._email[i].classList.add('in-error');
                        this.error(this._email[i], this._email[i].getAttribute('data-text-valid'));
                    }
                }
            }

            // проверка на пустое поле
            if (this.vl) {
                for (let i = 0; i < this._valIn.length; i++) {
                    if (this._valIn[i].value.length > 2) {
                        this._valIn[i].classList.remove('in-error');
                    } else {
                        valid++;
                        this._valIn[i].classList.add('in-error');
                    }
                }
            }

            // проверка полей пароля
            let password = '';

            if (this.ps) {
                for (let i = 0; i < this._password.length; i++) {
                    if (this._password[i].getAttribute('name') == 'password') {

                        if (this._password[i].value.length >= 4) {
                            this._password[i].classList.remove('in-error');
                            password = this._password[i].value;
                        } else {
                            valid++;
                            this._password[i].classList.add('in-error');
                            this.error(this._password[i], this._password[i].getAttribute('data-text-valid'));
                        }
                    }

                    if (this._password[i].getAttribute('name') == 'password_repeat' && password.length >= 4) {
                        if (this._password[i].value == password) {
                            this._password[i].classList.remove('in-error');
                        } else {
                            valid++;
                            this._password[i].classList.add('in-error');
                            this.error(this._password[i], this._password[i].getAttribute('data-text-valid'));
                        }
                    }
                }
            }

            if (this.te) {
                for (let i = 0; i < this._textarea.length; i++) {
                    if (this._textarea[i].value.length >= 40) {
                        this._textarea[i].classList.remove('in-error');
                    } else {
                        valid++;
                        this._textarea[i].classList.add('in-error');
                        this.error(this._textarea[i], this._textarea[i].getAttribute('data-text-valid'));
                    }
                }
            }

            if (this.sh) {
                for (let i = 0; i < this._scheck.length; i++) {
                    if (this._scheck[i].checked) {
                        this._scheck[i].closest('.js-el-form').classList.remove('error');
                    } else {
                        this._scheck[i].closest('.js-el-form').classList.add('error');
                        valid++;
                    }
                }
            }

            return valid;
        }

        // метод проверки формы на валидность
        validForm() {

            this.closeError();

            if (this.valid() > 0) {
                return true;
            }

            return false;
        }
    }

    $FORM_VALID.on('submit', function (e) {
        let ths = $(this);
        let el_email = ths.find('input[name="email"]');
        let val_in = ths.find('.js-valid-in');
        let in_password = ths.find('.js-password');
        let textarea = ths.find('.js-textarea');
        let scheck = ths.find('.js-check');

        // создание экземпляра класса Validation
        let validation = new Validation(ths, el_email, val_in, in_password, textarea, scheck);

        if (validation.validForm()) {
            e.preventDefault();
        }
    });


    /*переключение форм*/
    const ITEM_FORM = $('.page-forms__cont');


    $('.js-btn-form').click(function (e) {
        e.preventDefault();
        let ths = $(this);
        let att = ths.attr('data-btn-form');

        let clos = ths.closest('.page-forms__cont');

        let el_email = clos.find('input[name="email"]');
        let val_in = clos.find('.js-valid-in');
        let in_password = clos.find('.js-password');
        let textarea = clos.find('.js-textarea');
        let scheck = clos.find('.js-check');

        // создание экземпляра класса Validation
        let validation = new Validation(clos, el_email, val_in, in_password, textarea, scheck);

        if (!validation.validForm()) {
            ITEM_FORM.removeClass('page-forms__cont_active');
            $('.page-forms__cont[data-pag-form="' + att + '"]').addClass('page-forms__cont_active');
        }

    });

    $('.js-in-phone').mask('+7(495) 999 99 99', { placeholder: "+7(495) 000 00 00" });

    /*mob-slider*/
    if ($(window).width() <= 480) {
        $('.js-mob-slick').each(function (e, elem) {
            $(elem).not('.slick-initialized').slick({
                slidesToScroll: 1,
                arrows: false,
                appendDots: $(elem).closest('.js-row-js').find('.js-slider-dots'),
                dots: true,
                infinite: true,


            });
        });

        $('.js-slider-worker').each(function (e, elem) {
            $(elem).not('.slick-initialized').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: false,
                appendDots: $(elem).closest('.js-row-js').find('.js-slider-dots'),
                dots: true,
                infinite: true,

                responsive: [
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        });
    }

    $(window).resize(function() {
        if ($(window).width() <= 480) {
            $('.js-mob-slick').each(function (e, elem) {
                $(elem).not('.slick-initialized').slick({
                    slidesToScroll: 1,
                    arrows: false,
                    appendDots: $(elem).closest('.js-row-js').find('.js-slider-dots'),
                    dots: true,
                    infinite: true,


                });
            });

            $('.js-slider-worker').each(function (e, elem) {
                $(elem).not('.slick-initialized').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    appendDots: $(elem).closest('.js-row-js').find('.js-slider-dots'),
                    dots: true,
                    infinite: true,

                });
            });
        }
        else {
            $('.js-mob-slick').each(function (e, elem) {
                $(elem).filter('.slick-initialized').slick('unslick');
            });
            $('.js-slider-worker').each(function (e, elem) {
                $(elem).filter('.slick-initialized').slick('unslick');
            });
        }
    });




    $('.js-new-item').each(function (e, elem) {
        $(elem).slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            appendDots: $(elem).closest('.js-row-js').find('.js-slider-dots'),
            dots: true,
            infinite: true,
        });
    });

    /*календарь*/
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: 'Предыдущий',
        nextText: 'Следующий',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $("#datepicker").datepicker({
        showOn: "button",
        buttonImage: "../img/icons/calendar.png",
        buttonImageOnly: true,
    });

    $('.slider-btn__check').click(function(){
        $(this).parent().toggleClass('on');
    });

    $(".el-select__content").mouseenter(function(){
        $(this).parent().find(".el-select__main").addClass('active');
    });
    $(".el-select__content").mouseleave(function(){
        $(this).parent().find(".el-select__main").removeClass('active');
    });
    // $(".el-select__main").mouseenter(function(){
    //     $(this).parent().find(".el-select__main").css("background-color", " #fefefe")
    // });
    // $(".el-select__main").mouseleave(function(){
    //     $(this).parent().find(".el-select__main").css("background-color", " #F7F7F6")
    // });
    // window.onload = function () {
    //     $('.news__item' ).addClass('loaded_hiding');
    //     window.setTimeout(function () {
    //         $('.news__item' ).addClass('loaded');
    //         $('.news__item' ).removeClass('loaded_hiding');
    //     }, 500);
    // }

    window.onload = function () {
        $('.news__item' ).addClass('loaded');
      }

});

