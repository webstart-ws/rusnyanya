@extends('layouts.app')

@section('title') English @endsection

@section('main_content')
    <div class="section-con section-con__top-pad section-con_no-cont section-con_mob-pad section-english">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages">
                        <div class="pages__item pages__item_mob english__item">
                            <div class="vacancy">
                                <h1 class="title-style-tree">Russian-speaking domestic personnel</h1>
                            </div>
                            <!-- section-text_p -->
                            <div class="section-text section-text_p">
                                <p>Welcome to RUSnyanya! RUSnyanya has been helping in seaching russian-speaking
                                    domestic personal in Europe (since 2011) and America (since 2018)</p>
                                <p>You can find Russian-speaking domestic personnel through database. For non
                                    russian-speaking I offer my personal assistance in finding up to three
                                    candidates that match family's requirements best. </p>
                            </div>
                        </div>
                        <div class="pages__item english__item pages__item_mob response">
                            <h3 class="title-style-four">Please, fill in the form below</h3>
                            <div class="response__text">
                                <p>and we’ll contact with you</p>
                            </div>
                            <div class="response__form">
                                <form action="#" class="js-form-validation">
                                    <div class="response__top">
                                        <div class="el-form-row el-form-row_col">
                                            <div class="el-form js-el-form">
                                                <span class="text-form-item text-item-english">First name:</span>
                                                <input type="text" data-text-valid="This is a valid field" class="type-input">
                                            </div>
                                            <div class="el-form js-el-form">
                                                <span class="text-form-item text-item-english">Prefferd E-mail:</span>
                                                <input type="text" name="email" data-text-valid="This is a valid field" class="type-input">

                                            </div>
                                        </div>
                                        <div class="el-form-row">
                                            <div class="el-form js-el-form">
                                                <span class="text-form-item">Address:</span>
                                                <input type="text" data-text-valid="This is a valid field" class="type-input">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-form-row">
                                        <div class="el-form js-el-form">
                                            <span class="text-form-item">Additional information:</span>
                                            <textarea class="type-textarea"></textarea>
                                        </div>
                                    </div>
                                    <div class="form-row-btn form-row-btn_end btn-mob-type">
                                        <button type="submit" class="site-btn bnt-type-one bnt-type-one2 f_15 js-bnt-valid">Send</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-con__aside section-con__aside-zero">
                    <div class="aside-baner">
                        <div class="aside-baner__img">
                            <img src="img/img-baner.png" alt="img">
                        </div>
                        <div class="aside-baner__text aside-baner__text_mar">
                            <p>Бесплатно регистрируйтесь и разместите вашу анкету или вакансию. Получайте прямые
                                предложения по всей Европе</p>
                        </div>
                        <div class="aside-baner__btn">
                            <a href="{{ route('register', ['type' => 'vacancies', 'auth_required' => true]) }}" class="site-btn btn-site-two">Добавить вакансию</a>
                            <a href="{{ route('register', ['type' => 'questionnaires']) }}" class="site-btn btn-site-two">Добавить анкету</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
