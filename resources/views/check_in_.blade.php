@extends('layouts.app')


@section('title') Регистрация @endsection

@section('main_content')



    <div class="wr-body">
        <div class="container">
            <div class="el-email">
                <div class="el-email__title">Вы успешно зарегистрировались</div>
                <br>
                <p>Алексей Попов, ваша анкета зарегистрирована на <a href="/">rusnyanya.com</a></p>
                <br>
                <p>
                    После прохождения модерации вы сможете посмотреть свою анкету по ссылке: <a href="{{asset('/Questionnaire')}}">https://rusnyanya.com/profiles/6518/</a>
                </p>
                <br>
                <p><b>Ваш логин:</b> <span>info-alexeypopov@yandex.ru</span></p>
                <p><b>Пароль:</b> <span>123123</span></p>
            </div>
            <div class="el-social">
                <div class="el-social__text">Следите за новостями RUSnyanya.com в соцсетях:</div>
                <div class="social">
                    <ul class="social__list">
                        <li class="social__item">
                            <a href="#" class="social__link social__link_fb">
                                <svg class="svg-icon">
                                    <use xlink:href="img/svg/sprite.svg#fb"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="#" class="social__link social__link_tl">
                                <svg class="svg-icon">
                                    <use xlink:href="img/svg/sprite.svg#tl"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="#" class="social__link social__link_vk">
                                <svg class="svg-icon">
                                    <use xlink:href="img/svg/sprite.svg#vk"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="#" class="social__link social__link_yo">
                                <svg class="svg-icon">
                                    <use xlink:href="img/svg/sprite.svg#yo"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
