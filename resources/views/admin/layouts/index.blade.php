@extends('admin.app')

@section('body')
<body class="page-body page-fade" data-url="http://neon.dev">
    <div class="page-container">
        <div class="sidebar-menu">
            <div class="sidebar-menu-inner">
                @include('admin.layouts.sidebar')
            </div>
        </div>

        <div class="main-content">

            @include('admin.layouts.menu')

            @yield('main-content')

        </div>
    </div>

    <script src="{{ asset('js/admin/gsap/TweenMax.min.js') }}"></script>
    <script src="{{ asset('js/admin/jquery-ui/jquery-ui-1.10.3.minimal.min.js') }}"></script>
    <script src="{{ asset('js/admin/bootstrap.js') }}"></script>
    <script src="{{ asset('js/admin/joinable.js') }}"></script>
    <script src="{{ asset('js/admin/resizeable.js') }}"></script>
    <script src="{{ asset('js/admin/neon-api.js') }}"></script>
    <script src="{{ asset('js/admin/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>

    <!-- JavaScripts initializations and stuff -->
    <script src="{{ asset('js/admin/neon-custom.js') }}"></script>

    <!-- Demo Settings -->
    <script src="{{ asset('js/admin/neon-demo.js') }}"></script>

    <script src="{{ asset('js/admin/dashboard.js') }}"></script>
    @yield('js')

</body>
@endsection
