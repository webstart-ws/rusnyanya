@include('admin.layouts.header')

<ul id="main-menu" class="main-menu">
    <!-- add class "multiple-expanded" to allow multiple submenus to open -->
    <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
    <li class="active opened active has-sub">
        <a href="index.html">
            <i class="entypo-gauge"></i>
            <span class="title">Dashboard</span>
        </a>
        <ul class="visible">
            <li class="active">
                <a href="{{ route('dashboard') }}">
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{ route('home') }}" target="_blank">
                    <span class="title">Visit Website</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="has-sub">
        <a href="layout-api.html">
            <i class="entypo-user"></i>
            <span class="title">Users</span>
        </a>
        <ul>
{{--             <li>
                <a href="{{ route('admin.users') }}">
                    <span class="title">All Users</span>
                </a>
            </li>
 --}}
            <li>
                <a href="{{ route('admin.applicants') }}">
                    <span class="title">Applicants</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.vacancy') }}">
                    <span class="title">Vacancy</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="has-sub">
        <a href="ui-panels.html">
            <i class="entypo-newspaper"></i>
            <span class="title">News</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('admin.news.all') }}">
                    <span class="title">All</span>
                </a>
            </li>
            <li>
                <a href="{{ route('admin.news.add') }}">
                    <span class="title">New</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="has-sub">
        <a href=rus"forms-main.html">
            <i class="entypo-doc-text"></i>
            <span class="title">Reviews</span>
        </a>
        <ul>
            <li>
                <a href="{{ route('admin.review.all') }}">
                    <span class="title">All</span>
                </a>
            </li>
        </ul>
    </li>
</ul>
