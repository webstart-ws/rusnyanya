@extends('admin.layouts.index')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Post
                    </div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal form-groups-bordered" method="POST" action="{{ route('admin.news.update', ['id' => $post->id]) }}"  enctype="multipart/form-data">
                        @csrf
                        <div id="headerImage">
                            <label class="col-sm-3 control-label">Header Image</label>
                            <input type="file" id="headerImageUpload" name="header_image" />
                            <img id="preview" src="" />
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="title" placeholder="Title" value="{{ $post->title }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="dt" placeholder="Date" value="{{ $post->dt }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="email" placeholder="Name" value="{{ $post->name }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Komm</label>
                            <div class="col-sm-5">
                                <textarea id="komm" class="form-control" name="komm" placeholder="Komm">{!! $post->komm !!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Main Komm</label>
                            <div class="col-sm-5">
                                <textarea id="main_komm" class="form-control" name="main_komm" placeholder="Main Komm">{!! $post->main_komm !!}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5"><button type="submit" class="btn btn-default">Edit</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/izitoast.css') }}">
@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script src="{{ asset('js/admin/izitoast.js') }}"></script>
    <script>
        CKEDITOR.replace( 'komm',  {
            filebrowserUploadUrl: "{{ route('news.content.image.post', ['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        } );
        CKEDITOR.replace( 'main_komm', {
            filebrowserUploadUrl: "{{ route('news.content.image.post', ['_token' => csrf_token()])}}",
            filebrowserUploadMethod: 'form'
        });

           function readURL(input) {

               if (input.files && input.files[0]) {
                   var reader = new FileReader();

                   reader.onload = function (e) {
                       console.log(e.target.result);
                       $('#preview').attr('src', e.target.result);
                   }

                   reader.readAsDataURL(input.files[0]);
               }
           }

        $(document).on('change', '#headerImageUpload',  function () {
               readURL(this);
       });
    </script>
    @if($errors->any())
        <script>
            iziToast.warning({
                title: 'ERROR!',
                message: 'Fill required fields',
                position: 'topRight'
            });
        </script>
    @endif
@endsection

