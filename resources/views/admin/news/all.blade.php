@extends('admin.layouts.index')

@section('main-content')
    <table class="table responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Date</th>
            <th>Click Count</th>
            <th>Tools</th>
        </tr>
        </thead>
        <tbody>
        @foreach($news as $new)
            <tr>
                <td>{{ $new->id }}</td>
                <td>{{ $new->name }}</td>
                <td>{{ $new->dt }}</td>
                <td>{{ $new->click_count }}</td>
                <td>
                    <a href="{{ route('admin.news.edit', ['id' => $new->id]) }}" class="btn btn-info">Edit</a>
                    <a href="{{ route('admin.news.delete', ['id' => $new->id]) }}" class="btn btn-danger">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{ $news->links('pagination::bootstrap-4') }}
@endsection

