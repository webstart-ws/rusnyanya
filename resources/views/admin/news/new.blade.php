@extends('admin.layouts.index')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Post
                    </div>
                </div>
                <div class="panel-body">
                    <form role="form" class="form-horizontal form-groups-bordered">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="name" placeholder="Title" value="{{ $post->title }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="fam" placeholder="Date" value="{{ $post->dt }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="email" placeholder="Name" value="{{ $post->name }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Chapter</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ $post->razd_id }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Viber</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="viber" placeholder="Viber" value="{{ $post->viber }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">WhatsApp</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="whatsapp" placeholder="WhatsApp" value="{{ $post->whatsapp }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Skype</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="skype" placeholder="Skype" value="{{ $post->skype }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Payment</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="oplata" placeholder="Payment" value="{{ $post->skype }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Education</label>
                            <div class="col-sm-5">
                                <select name="education" id="">
                                    <option value="">Choose education</option>
                                    @foreach($education as $item)
                                        <option value="{{ $item->id }}" {{ $user->education !== null && ($user->education->id == $item->id) ? 'selected=selected' : '' }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Speciality</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="spec" placeholder="Speciality" value="{{ $user->spec }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mother tongue</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="lang_own" placeholder="Mother tongue" value="{{ $user->spec }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Education</label>
                            <div class="col-sm-5">
                                <select name="recomend" id="">
                                    <option value="">Choose education</option>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Education</label>
                            <div class="col-sm-5">
                                <select name="recomend" id="">
                                    <option value="">Choose education</option>
                                    <option value="0">No</option>
                                    <option value="1">Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5"><button type="submit" class="btn btn-default">Sign in</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

