@extends('admin.layouts.index')

@section('main-content')
    <table class="table responsive">
        <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>E-mail</th>
                <th>Reg. Date</th>
                <th>IP</th>
                <th>Last Login</th>
                <th>Last Edit</th>
                <th>Phone</th>
                <th>Viber</th>
                <th>WhatsApp</th>
                <th>Skype</th>
                <th>Last Visit</th>
                <th>Payment</th>
                <th>Payment Period</th>
                <th>Education</th>
                <th>Tools</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name . ' ' . $user->fam }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->dt_reg }}</td>
                    <td>{{ $user->from_ip }}</td>
                    <td>{{ $user->dt_last_login }}</td>
                    <td>{{ $user->dt_last_edit }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>{{ $user->viber }}</td>
                    <td>{{ $user->whatsapp }}</td>
                    <td>{{ $user->skype }}</td>
                    <td>{{ $user->dt_last_visit }}</td>
                    <td>{{ $user->oplata }}</td>
                    <td>{{ $user->oplata_period }}</td>
                    <td>{{ $user->education_id }}</td>
                    <td><a href="{{ route('user.edit', ['id' => $user->id]) }}" class="btn btn-info">Edit</a><a href="{{ route('user.delete', ['id' => $user->id]) }}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $users->links('pagination::bootstrap-4') }}
@endsection

