@extends('admin.layouts.index')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        Edit user
                    </div>
                </div>
                <div class="panel-body">
                    <form role="form" class="form-horizontal form-groups-bordered" method="POST" action="{{ route('applicant.update', ['id' => $user->id]) }}">
                        @csrf
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="name" placeholder="Name" value="{{ $user->author->name }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="fam" placeholder="Last Name" value="{{ $user->author->fam }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">E-mail</label>
                            <div class="col-sm-5"><input type="email" class="form-control" name="email" placeholder="E-mail" value="{{ $user->author->email }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="phone" placeholder="Phone" value="{{ $user->author->phone }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Viber</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="viber" placeholder="Viber" value="{{ $user->author->viber }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">WhatsApp</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="whatsapp" placeholder="WhatsApp" value="{{ $user->author->whatsapp }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Skype</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="skype" placeholder="Skype" value="{{ $user->author->skype }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Education</label>
                            <div class="col-sm-5">
                                <select name="education" id="">
                                    <option value="">Choose education</option>
                                    @foreach($education as $item)
                                        <option value="{{ $item->id }}" {{ $user->author->education_id !== null && ($user->author->education_id == $item->id) ? 'selected=selected' : '' }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Price Period</label>
                            <div class="col-sm-5">
                                <select name="price_period_id" id="">
                                    <option value="">Choose Price Period</option>
                                    @foreach($pricePeriod as $pp)
                                        <option value="{{ $pp->id }}" {{ $user->price_period_id !== null && ($user->price_period_id == $pp->id) ? 'selected=selected' : '' }}>{{ $pp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Driver License</label>
                            <div class="col-sm-5">
                                <select name="driver_license" id="">
                                    <option value="" @if($user->driver_license == null) selected="selected" @endif>Choose Driver License</option>
                                    <option value="1" @if($user->driver_license == 1) selected="selected" @endif>Yes</option>
                                    <option value="0" @if($user->driver_license == 0) selected="selected" @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Published</label>
                            <div class="col-sm-5">
                                <select name="published" id="">
                                    <option value="" @if($user->published == null) selected="selected" @endif>Choose Published</option>
                                    <option value="1" @if($user->published == 1) selected="selected" @endif>Yes</option>
                                    <option value="0" @if($user->published == 0) selected="selected" @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Show In Search</label>
                            <div class="col-sm-5">
                                <select name="in_search" id="">
                                    <option value="" @if($user->in_search == null) selected="selected" @endif>Choose Published</option>
                                    <option value="1" @if($user->in_search == 1) selected="selected" @endif>Yes</option>
                                    <option value="0" @if($user->in_search == 0) selected="selected" @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Is Paid</label>
                            <div class="col-sm-5">
                                <select name="is_paid" id="">
                                    <option value="" @if($user->is_paid == null) selected="selected" @endif>Choose Published</option>
                                    <option value="1" @if($user->is_paid == 1) selected="selected" @endif>Yes</option>
                                    <option value="0" @if($user->is_paid == 0) selected="selected" @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Is Top</label>
                            <div class="col-sm-5">
                                <select name="is_top" id="">
                                    <option value="" @if($user->is_top == null) selected="selected" @endif>Choose Published</option>
                                    <option value="1" @if($user->is_top == 1) selected="selected" @endif>Yes</option>
                                    <option value="0" @if($user->is_top == 0) selected="selected" @endif>No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">About</label>
                            <div class="col-sm-5"><textarea class="form-control" name="about" placeholder="About">{{ $user->about }}</textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Comment</label>
                            <div class="col-sm-5"><textarea class="form-control" name="comments" placeholder="Comment">{{ $user->comments }}</textarea></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Speciality</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="speciality" placeholder="Speciality" value="{{ $user->author->speciality }}" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Salry</label>
                            <div class="col-sm-5"><input type="number" class="form-control" name="salary" placeholder="Salary" value="{{ $user->salary }}" /></div>
                        </div>
                        {{-- <div class="form-group">
                            <label class="col-sm-3 control-label">Mother tongue</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="lang_own" placeholder="Mother tongue" value="{{ $user->spec }}" /></div>
                        </div> --}}
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5"><button type="submit" class="btn btn-default">Update</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

