@extends('admin.layouts.index')

@section('main-content')
    <table class="table responsive">
        <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>E-mail</th>
                <th>Reg. Date</th>
                <th>IP</th>
                <th>Last Login</th>
                <th>Last Edit</th>
                <th>Phone</th>
                <th>Viber</th>
                <th>WhatsApp</th>
                <th>Skype</th>
                <th>Driving License</th>
                <th>Last Visit</th>
                <th>Payment</th>
                <th>Payment Period</th>
                <th>Education</th>
                <th>Tools</th>
            </tr>
        </thead>
        <tbody>
            @foreach($vacancys as $vacancy)
                <tr>
                    <td>{{ $vacancy->id }}</td>
                    <td>{{ $vacancy->author->name . ' ' . $vacancy->author->fam }}</td>
                    <td>{{ $vacancy->author->email }}</td>
                    <td>{{ $vacancy->author->dt_reg }}</td>
                    <td>{{ $vacancy->author->from_ip }}</td>
                    <td>{{ $vacancy->author->dt_last_login }}</td>
                    <td>{{ $vacancy->author->dt_last_edit }}</td>
                    <td>{{ $vacancy->author->phone }}</td>
                    <td>{{ $vacancy->author->viber }}</td>
                    <td>{{ $vacancy->author->whatsapp }}</td>
                    <td>{{ $vacancy->author->skype }}</td>
                    <td>{{ $vacancy->driving_license == 1 ? 'Есть' : 'Нет прав' }}</td>
                    <td>{{ $vacancy->author->dt_last_visit }}</td>
                    <td>{{ $vacancy->author->oplata }}</td>
                    <td>{{ $vacancy->price_period_id == "0" ? 'в час' : ' в месяц' }}</td>
                    <td>{{ $vacancy->author->education_id }}</td>
                    <td><a href="{{ route('vacancy.edit', ['id' => $vacancy->author->id]) }}" class="btn btn-info">Edit</a><a href="{{ route('vacancy.delete', ['id' => $vacancy->id]) }}" data-url="{{ route('vacancy.delete', ['id' => $vacancy->id]) }}" class="btn btn-danger delete">Delete</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $vacancys->links('pagination::bootstrap-4') }}

    @section('js')
        <script>
            $(document).ready(function() {
                $(".delete").click(function(e){
                    e.preventDefault()
                    let url = $(this).data('url');
                    $.ajax({
                        url: url,
                        type:'post',
                        data:'',
                        success: function(src){
                            console.log("ok")
                        }
                    })
                });
            });
        </script>
    @endsection
@endsection

