@extends('admin.layouts.index')

@section('main-content')
    <table class="table responsive">
        <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>E-mail</th>
                <th>Reg. Date</th>
                <th>IP</th>
                <th>Last Login</th>
                <th>Last Edit</th>
                <th>Phone</th>
                <th>Viber</th>
                <th>WhatsApp</th>
                <th>Skype</th>
                <th>Driving License</th>
                <th>Last Visit</th>
                <th>Payment</th>
                <th>Payment Period</th>
                <th>Education</th>
                <th>Tools</th>
            </tr>
        </thead>
        <tbody>
            @foreach($applicants as $applicant)
                <tr>
                    <td>{{ $applicant->id }}</td>
                    <td>{{ $applicant->author->name . ' ' . $applicant->author->fam }}</td>
                    <td>{{ $applicant->author->email }}</td>
                    <td>{{ $applicant->author->dt_reg }}</td>
                    <td>{{ $applicant->author->from_ip }}</td>
                    <td>{{ $applicant->author->dt_last_login }}</td>
                    <td>{{ $applicant->author->dt_last_edit }}</td>
                    <td>{{ $applicant->author->phone }}</td>
                    <td>{{ $applicant->author->viber }}</td>
                    <td>{{ $applicant->author->whatsapp }}</td>
                    <td>{{ $applicant->author->skype }}</td>
                    <td>{{ $applicant->driving_license == 1 ? 'Есть' : 'Нет прав' }}</td>
                    <td>{{ $applicant->author->dt_last_visit }}</td>
                    <td>{{ $applicant->author->oplata }}</td>
                    <td>{{ $applicant->price_period_id == "0" ? 'в час' : ' в месяц' }}</td>
                    <td>{{ $applicant->author->education_id }}</td>
                    <td><a href="{{ route('applicant.edit', ['id' => $applicant->author->id]) }}" class="btn btn-info">Edit</a><a href="{{ route('applicant.delete', ['id' => $applicant->id]) }}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $applicants->links('pagination::bootstrap-4') }}
@endsection

