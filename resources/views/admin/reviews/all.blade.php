@extends('admin.layouts.index')
@section('css')
    <style>
        .header-page {
            display: flex;
            justify-content: space-between;
        }
    </style>
@endsection
@section('main-content')
    @if ($reviews->count() == 0)
        <div class="header-page">
            <span>Nothing not found</span>
            <a href="{{ route('admin.review.add') }}">New Review</a>
        </div>
    @else
        <table class="table responsive">
            <thead>
            <tr>
                <th>#</th>
                <th>Full Name</th>
                <th>Review</th>
                <th>Tools</th>
            </tr>
            </thead>
            <tbody>
            @foreach($reviews as $review)
                <tr>
                    <td>{{ $review->id }}</td>
                    <td>{{ $review->name }}</td>
                    <td>{{ $review->review }}</td>
                    <td><a href="{{ route('admin.review.edit', ['id' => $review->id]) }}" class="btn btn-info">Edit</a><a href="{{ route('admin.review.delete', ['id' => $review->id]) }}" class="btn btn-danger review" data-id="{{$review->id}}">Delete</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
@endsection

@section('js')
    <script>
        $(document).ready(function(){


            $('.review').on('click', function(e) {
                let elemId = $(this).attr('data-id');
                e.preventDefault(2)
                if (confirm('Are you sure ?')) {
                    window.location.href = '/admin/reviews/delete/' + elemId;
                }
            })
        })


    </script>
@endsection
