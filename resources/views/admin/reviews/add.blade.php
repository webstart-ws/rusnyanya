@extends('admin.layouts.index')

@section('main-content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        Add Post
                    </div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal form-groups-bordered" method="POST" action="{{ route('admin.review.create') }}"  enctype="multipart/form-data">
                        @csrf
                        <div id="headerImage">
                            <label class="col-sm-3 control-label">Persone picture</label>
                            <input type="file" id="headerImageUpload" name="photo" />
                            <img id="preview" src="" />
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Name</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="name" placeholder="Name" /></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Review</label>
                            <div class="col-sm-5"><input type="text" class="form-control" name="review" placeholder="Review" /></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5"><button type="submit" class="btn btn-default">Add</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/admin/izitoast.js') }}"></script>

    @if($errors->any())
        <script>
            iziToast.warning({
                title: 'ERROR!',
                message: 'Fill required fields',
                position: 'topRight'
            });
        </script>
    @endif
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('css/admin/izitoast.css') }}">
@endsection
@section('js')
    <script>
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    console.log(e.target.result);
                    $('#preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#headerImageUpload',  function () {
            readURL(this);
        });
    </script>
@endsection

