@extends('layouts.app')

@section('title')
    Новости
@endsection

@section('main_content')

    <div class="section-con con-news">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="el-filetr">
                        <div class="el-filetr__item">
                            <h2 class="el-filetr__link el-filetr__link_active">Новости</h2>
                        </div>
                    </div>
                    <div class="news">
                        @foreach($news as $newse)

                            <div class="news__item new-item">
{{--                                <div class="preloader">--}}
{{--                                    <svg class="preloader__image" role="img" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="44" height="44" viewBox="0 0 44 44"><defs><clipPath id="rfiga"><path d="M22 0c12.15 0 22 9.85 22 22s-9.85 22-22 22S0 34.15 0 22 9.85 0 22 0zM5.281 22c0 9.233 7.485 16.719 16.719 16.719 9.233 0 16.719-7.486 16.719-16.72 0-9.233-7.486-16.718-16.719-16.718-9.234 0-16.719 7.485-16.719 16.719z"/></clipPath></defs><g><g><path fill="#f4504c" d="M22 0c12.15 0 22 9.85 22 22s-9.85 22-22 22S0 34.15 0 22 9.85 0 22 0zM5.281 22c0 9.233 7.485 16.719 16.719 16.719 9.233 0 16.719-7.486 16.719-16.72 0-9.233-7.486-16.718-16.719-16.718-9.234 0-16.719 7.485-16.719 16.719z"/></g><g clip-path="url(#rfiga)"><image width="27" height="58" transform="translate(-5 -6)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAA6CAYAAABBJCLGAAAIYUlEQVRYR71Yv28kSRl9X1WPf+Bbr3bZFVoJpAVZF6xFgC4gQjISpARIDvgDiEgJkWYmICQkQSIj8hASEZ0vIVgmOckmsYyls7RrvNx6ZmxP93RXfeirqq9d05673duAkcbdnu6uV+9973tVM1RV1RSAB+CY2RtjvBzlfwCcrslR3vKi7G0AGCIy3nsrRwAFAJvecq+cy8tSVVWTFWACJIDyViAFkwdlUBkogMmbmRWslwD0HgEL9wrYWxk0sQrsMqBVYPKgArYDEpEVwMSsnUQGDAH7UgYnIgHKGXWZJYwlKWVQARApBUwk1M/C55mcJGBvEpDWKWeW16wL1p19kLEDqPcEKWmxWFxkhugC5vWS81zCvGZqiAAmLPN6Kjth9io5TyXMXZjXTJ2ox9wkCibsbHKmTEZlbQ3yhboxO6p8XbCu9bsWD/9ngCpjaBdhdpaYaa2+yvYqo8q3ZP2st1oJsxq2fXbynmBqEG3qVfZu5cxrlpo+MPtX1lddViqjAGlTC4jKmRtBz7VObVsk4NBnn3dqpYDBicmpS7YnolzKvJ9yq+vnem+Iq3GeGHljG2OUTR5V5L0PTkwWbwfr5KHG1FJc/SPFVbC8BHEWwkJN/o+6RUZL2ZiA5VqeHl0Jo/XLsvzMGNPWqpONea1WJkhK+pD6agr9rBNXRhLk7wlApFq1rCxJmDFbFca6pLTgCbht6r913NjNQ40slXDVEqMgLViHcXhGDPLXbJFccmJmd7W61C9fEPOEyM9lTcubXsodwP6SpMmNIZZXhnkzq1HuclGG5LjMZKuymEUn1Qa4gP05M4UMFho5ufKeQdR92WodBs1rkwELkAGkVQyLG/+YBu6GrzLKDdIN4gCkfaduNAAxGSuZIAy1d8WNf8jcqGbwkWDb1HnNVMI2I7NE0c2NsCHvYQxM4+ELY1CKjL/PNjWahWqEgJikVUA95kGcO1QkDc714kDZagR+fiZgvzOAxER3Vb6TUaPXtyu1Oq3ba1ofSnUSQG/gHTNdSs1+u2K7lu8T8+TIndmGsWakuMCr5Q3IeCyEHTPXbEwA+40ErveeUvDmQJqLFEsYtIm1MpCa2FjbZHvvGcYQYA2Mc/ESFmy4MmwmYpBfZzWJQNLaFuwc2NpWOh1UayZm0B7S+iE5U0DZAFXcwJr/Osf/kZr9She37GE1SKhzxLYEuLxWIEsGdbx4t/NyJu24ZeNbOgs2DV8ZZ94I2C8TWN5P+bZt1R6f4gTuRReLzI7hrI2hzsy3lnnqiF5LzX6Rqs4owGgkaZo2OYoC1ABcNOGovtS1LTKSv05YhzYLlWoAb5kX3tuJtf7KOTejui5/Dhfr4mA5SZViJkwjbOADE6mjjCj3y5xQWKCJKoT5hcm4OGks2NraVjzxhb9en8/f0Lyuf1LEKQW5GjThwQKgsoiMwrUiDKjLjPStbHvDqptNSDb8NQr4uq4bZltZ62brvrjG5s1U3PjjYK/43So+WTNTL20B6gTQk2ONugZ64d5CFDdFEyUDGslA4Uc9+Vbkizn3uHTOXW/W9S2m07mA/SjbqORmSP3WE5B2ImgaaoQlhUHj5LgI7q1rVL0ePFfcsOcFe66borlZLBbV41ePS6quqx/SGhksQFhLkqVBFgDWUBPQy3fDrZSqRCqpWE+Y1d77UlLDe99szee3uH5S4zkqcePHaWeU213HyQfugtwlzVpgo1avBLRpmrIsy+rpzU2Ds7MGe3uO5vP591O9dHmIhkhfj0KCr0noBJoxquQ8flvVNdAJCwHx3tdN09RikKeTSY1qh7GLmoiYbm9vv2eM6aUHQ75sRLMoYN4G+XIT6iRrYVocnbBxzjkB8t7zbPadZmeHFjKUzJn45uYZiMx8EwbzrArxNEiXmOfg0Ri37LAJCXFhVivQZDKpz8/P3d7hoR8CGAwGodFpOp0+McYU2NqSkcXvAZQZTCEb5upQBjaT++La5/11qJNzTnRrrq6uFgFkb88Ph0P0+4PwLFHsYZpM+LExsmG5ubN3CjX6iAzdxn5TmSNIqI+AiHTy9lVV+ZcvX9ajEXBwsC8/CHT3LkHzh9Jns9nMAA9AdG3wEYDrWDN5JSDd3gFTsNtyzr/xvn5QS438xcVFM5vNWFhFJqvBHkyn6GEb2J6BZgAeMHgmbSu//Wxr406ZeZud+1LqI6z8bDZrqqriy8tLf7h36PvoJ9nuAwUZX/PrrR561krMikZXzIHNQ+Yr+SD9ecgPA8AT5/x5XXvgC0wm22738tLTT/dcYMJMWCFf27TM/K1sqxwbV36GobeER8xy+hi1u5BoQO1LlL5CxbujXTfaH+EY+zwYDsGDviDdq1Pub2L+98YFNq25lL3D8utp03g8cxyXkRt/dHQM4AV2y5JxeuoR3RBtHXwU96T3Gih9QPxP7uHbZxbr6wbuWbjxXJaaBlwU52TtBTm34CBZWfKfMMar2ceMw0Pf7/dFch37a1mFCTGzhLc5wxk9x3Ocn4PwXaA8gd+RO3bgx+MxxmPg0aPTwOL4+JhDo97VSGl9FamYEHzAFvugkxNY7Jygwg6vn4CqHXA5Bp9ugPZ34UajEfb396Nkof3bXw++FmCpZgfM9gfjsQE+wcbGEa2vr8smiIFjlOVbFkqnj37mEcwwYgwHIRk0Fd4bKcp4YIF9AJ8SsIejoyNTliVLgx7iEC8uX/D+8XHsn5RxkqsfxExaXbQfjUYkoAH26ad0mAygssU4+ebSLckoQIPhkProA5CM7mMICdGUBtHX73Ta+8hJwMAAfR4MRCUhCR4SqA9w1nirNq3vM/7SPWk8GXepX5ZW684v3x/MctXm5YMHexfVPKL0/P8CFo1391Pfuyb6ja//D+OVSn3yOQt/AAAAAElFTkSuQmCC"/></g></g></svg>--}}
{{--                                </div>--}}
                                <div class="new-item__img">
                                    <a href="{{ route('news.single', $newse->id ) }}">
                                        <img src="{{ asset('img/'. $newse->fname1 ) }}" alt="img">
                                    </a>
                                </div>
                                <div class="new-item__content">
                                    <a href="{{ route('news.single', $newse->id ) }}" class="new-item__title">{{$newse->name}}</a>
                                    <div class="new-item__date">
                                        <i class="svg-icon">
                                            <svg class="svg-icon_date">
                                                <use xlink:href="img/svg/sprite.svg#icon-calendar"></use>
                                            </svg>
                                        </i>
                                        <span>{{$news['dt']}}</span>
                                    </div>
                                    <div class="new-item__text">
                                        <p>{{$newse->main_komm}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                        {{ $news->links('pagination.custom') }}

                </div>
                @guest
                <div class="section-con__aside section-con__aside-zero">
                    <div class="aside-baner">
                        <div class="aside-baner__img">
                            <img src="{{asset("img/img-baner.png")}}" alt="img">
                        </div>
                        <div class="aside-baner__text aside-baner__text_mar">
                            <p>Бесплатно регистрируйтесь и разместите вашу анкету или вакансию. Получайте прямые
                                предложения по всей Европе</p>
                        </div>
                        <div class="aside-baner__btn">
                            <a href="{{ route('register', ['type' => 'vacancies', 'auth_required' => true]) }}" class="site-btn btn-site-two">Добавить вакансию</a>
                            <a href="{{ route('register', ['type' => 'questionnaires']) }}" class="site-btn btn-site-two">Добавить анкету</a>
                        </div>
                    </div>
                </div>
                    @endguest
            </div>
        </div>
    </div>
    <div class="scroll-up js-scroll-up">
        <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
            <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
        </svg>
    </div>

    <div class="body__opas js-boyd--opas"></div>
@endsection

