@extends('layouts.app')
@section('title') Contacts @endsection


@section('main_content')



    <div class="section-con section-con__top-pad section-con_no-cont section-con_mob-pad section-english">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages">
                        <div class="pages__item pages__item_mob english__item">
                            <div class="vacancy">
                                <h1 class="title-style-tree">Contacts | Контакты</h1>
                            </div>
                            <!-- section-text_p -->
                            <div class="section-text section-text_p">
                                <p><span>RUSnyanya</span></p>
                                <p><span>Amsterdam, The Netherlands</span></p>
                                <p><span>tel.: +31 (0) 6 52403153</span></p>
                                <p><span>KVK-nr: 56217870</span></p>
                                <p><span>IBAN: NL431INGB0006585352</span></p>

                            </div>
                        </div>

                    </div>
                </div>
                @guest
                <div class="section-con__aside section-con__aside-zero">
                    <div class="aside-baner">
                        <div class="aside-baner__img">
                            <img src="img/img-baner.png" alt="img">
                        </div>
                        <div class="aside-baner__text aside-baner__text_mar">
                            <p>Бесплатно регистрируйтесь и разместите вашу анкету или вакансию. Получайте прямые
                                предложения по всей Европе</p>
                        </div>
                        <div class="aside-baner__btn">
                            <a href="{{ route('register', ['type' => 'vacancies', 'auth_required' => true]) }}" class="site-btn btn-site-two">Добавить вакансию</a>
                            <a href="{{ route('register', ['type' => 'questionnaires']) }}" class="site-btn btn-site-two">Добавить анкету</a>
                        </div>
                    </div>
                </div>
                @endguest
            </div>
        </div>
    </div>


    <div class="scroll-up js-scroll-up">
        <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
            <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
        </svg>
    </div>

    <div class="body__opas js-boyd--opas"></div>
@endsection
