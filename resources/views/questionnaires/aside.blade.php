<div class="section-con__aside">
    <div class="aside-menu">
        <ul class="aside-menu__list">
            <li class="aside-menu__item">
                <a href="{{ route('profile') }}" class="aside-menu__link {{ url()->current() == route('profile') ? 'aside-menu__link_active' : '' }}"
                >Моя анкета</a>
            </li>
            <li class="aside-menu__item">
                <a href="{{ url('questionnaires/edit/'.auth()->user()->id) }}" class="aside-menu__link {{ url()->current() ==  url('questionnaires/edit/'.auth()->user()->id) ? 'aside-menu__link_active' : '' }}"
                >Личные данные</a>
            </li>
            <li class="aside-menu__item">
                <a href="{{ route('profile.payments') }}" class="aside-menu__link {{ url()->current() == route('profile.payments') ? 'aside-menu__link_active' : '' }}"
                >Оплата аккаунта</a>
            </li>
            <li class="aside-menu__item">
                @if(auth()->user()->subscriptions_enabled)
                    <a href="javascript:void(0);" class="aside-menu__link" id="subscribe-btn" data-value="0">Отписаться от рассылки</a>
                @else
                    <a href="javascript:void(0);" class="aside-menu__link" id="subscribe-btn" data-value="1">Подписаться на рассылку</a>
                @endif
            </li>
            <li class="aside-menu__item">
                <form action="{{ route('profile.delete') }}" method="post">
                    @method('DELETE')
                    @csrf
                    <a href="javascript:void(0);" class="aside-menu__link" id="profile-delete-btn">Удалить профиль</a>
                </form>
            </li>
        </ul>
    </div>
{{--    <div class="aside-content">--}}
{{--        <div class="title-style-six">Для новичков</div>--}}
{{--        <p>Вакансия заполняется по желанию. Размещается бесплатно до 10 дней. Заявки от пользователей вы будете получать на почту. </p>--}}
{{--        <p>Вы можете на время скрыть вашу вакансию, указав Мой статус 'Скрыть из поиска'. </p>--}}
{{--        <p>После оплаты тарифа вам будут доступны контатные данные соискателей.  </p>--}}
{{--    </div>--}}
</div>
