@extends('layouts.app')
@section('title')Анкета@endsection

@section('main_content')
    <div class="section-con section-con__top-pad section-con_no-cont section-con_mob-pad section-english">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages pages_pad_top">
                        <div class="pages__item">
                            <div class="questionnaire">
                                <div class="questionnaire__top">
                                    <div class="questionnaire__left">
                                        <div class="questionnaire__img js-row-js">
                                            <div class="questionnaire__img-slider js-new-item">
{{--                                                @foreach()--}}
                                                    <div>
                                                        <a href="{{asset('img/questionnaire.png')}}" data-fancybox="gallery">
                                                            <img src="{{asset('img/questionnaire.png')}}" alt="img">
                                                        </a>
                                                    </div>
{{--                                                @endforeach--}}
                                            </div>
                                            <div class="slider-dots slider-dots_img js-slider-dots"></div>
                                            <div class="questionnaire__znak">
                                                @auth
                                                <div href="#"
                                                     class="questionnaire__item-znak questionnaire__item-znak_star">
                                                    <i class="svg-icon">
                                                        <svg>
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-str')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                @endauth
                                                <a href="#"
                                                   class="questionnaire__item-znak questionnaire__item-znak_phone">
                                                    <i class="svg-icon">
                                                        <svg>
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-phone')}}">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="article-info">
                                            <div class="article-info__item">
                                                <i class="svg-icon">
                                                    <svg class="svg-icon_id">
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-id')}}"></use>
                                                    </svg>
                                                </i>
                                                <span>{{$applicant->user_id}}</span>
                                            </div>
                                            <div class="article-info__item">
                                                <i class="svg-icon">
                                                    <svg class="svg-icon_eye">
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-eye')}}"></use>
                                                    </svg>
                                                </i>
                                                <span>{{$applicant->author->num_views}}</span>
                                            </div>
                                            <div class="article-info__item">
                                                <i class="svg-icon">
                                                    <svg class="svg-icon_pen">
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-pen')}}"></use>
                                                    </svg>
                                                </i>
                                                <span>{{$applicant->author->last_date}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="questionnaire__contact">
                                        <h2 class="title-style-five questionnaire__title">{{ $applicant->author->name  }}, {{ $applicant->age }}</h2>

                                        <div class="questionnaire__info">
                                            @foreach($applicant->positions as $positions)
                                                {{ $positions['name']}},
                                            @endforeach
                                            </div>
                                        <div class="el-info el-info_mod vacancy_el_info">
                                            <div class="el-info__item">
                                                <i class="svg-icon">
                                                    <svg>
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-money')}}"></use>
                                                    </svg>
                                                </i>
                                                <!-- t_strong t_red-->
                                                <span class="el-info__text t_strong t_red">{{$applicant->salary}} {{$applicant->price_period_id == "0" ? 'в час' : ' в месяц'}}</span>
                                            </div>
                                            <div class="el-info__item">
                                                <i class="svg-icon">
                                                    <svg>
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#label-two')}}"></use>
                                                    </svg>
                                                </i>
                                                <span class="el-info__text">{{$applicant->country_name}} {{$applicant->city_name}}</span>
                                            </div>
                                            <div class="el-info__item">
                                                <i class="svg-icon">
                                                    <svg>
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-home')}}"></use>
                                                    </svg>
                                                </i>
                                                <span class="el-info__text">
                                                   {{ $applicant->accommodation_names }}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="questionnaire__row-list">
                                            <ul class="vacancy__list-item vacancy-list">
                                                <li>
                                                    <span class="vacancy-list__left">Нахожусь в:</span>
                                                    <span class="vacancy-list__right">{{$applicant->country_residence_name}}, {{$applicant->city_residece_name}}, {{$applicant->district_residence_name}}</span>
                                                </li>
                                                <li>
                                                    <span class="vacancy-list__left">Гражданство:</span>
                                                    <span class="vacancy-list__right">{{ $applicant->author->grazd }}</span>
                                                </li>
                                                <li>
                                                    <span class="vacancy-list__left">Документы:</span>
                                                    <span class="vacancy-list__right">
                                                        {{$applicant->document_name}}
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                        @auth
                                        <a href="javascript:void(0);" class="questionnaire__heart" data-user="{{ $applicant->user_id }}">
                                            <i class="svg-icon">
                                                <svg>
                                                    <use xlink:href="{{asset('img/svg/sprite.svg#icon-heart-star')}}"></use>
                                                </svg>
                                            </i>
                                            <i class="svg-icon {{ in_array($applicant->user_id, $favoriteIds) ? 'active' : '' }}">
                                                <svg>
                                                    <use xlink:href="{{asset('img/svg/sprite.svg#icon-heart')}}"></use>
                                                </svg>
                                            </i>
                                        </a>
                                        @endauth
                                    </div>

                                </div>
                                <div class="section-text section-text_mob section-text_mar">
                                    <h3 class="grey-line mt0">О себе</h3>
                                    <h4>{{$applicant->speciality}}</h4>
                                    <p>{{$applicant->comments}}</p>

                                    <h4>Образование</h4>
                                    <p>
                                        Киевский институт подготовки кадров государственной службы занятости Украины
                                        Получила высшее образование по менеджменту и администрирования.
                                        Бакалавр, 2012 г. (срок обучения 4 г. 5 мес.)</p>
                                    <h3 class="grey-line">Контакты</h3>
                                    <p>Связаться с кандидатом вы сможете после
                                        <a href="{{ route('register', ['type' => \App\Models\User::ROLE_VACANCY_VIEW]) }}"> регистрации </a> и
                                        <a href="{{ route('tariffs', ['register_type' => \App\Models\User::ROLE_VACANCY_VIEW]) }}">оплаты тарифа</a><br>
                                        Обнаружили неточности в контактах? <a href="#">Свяжитесь с администраторм.</a></p>
                                    <h3 class="grey-line">Дополнительная информация</h3>
                                    <ul class="vacancy__list-item vacancy-list">
                                        <li style="display: none">
                                            <span class="vacancy-list__left">Ищет работу в:</span>
                                            <span class="vacancy-list__right">Все страны</span>
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Образование:</span>
                                            <span class="vacancy-list__right">{{ $applicant->author->education_id ? $education->name: 'Не указано'}} </span>
                                            {{-- <span class="vacancy-list__right">{{ $applicant->author->education_id ? $applicant->education : '' }} </span> --}}
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Родной язык:</span>
                                            <span class="vacancy-list__right">{{$applicant->language_name}}</span>
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Знание языков: </span>
                                            <span class="vacancy-list__right">Английский</span>
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Вождение:</span>
                                            <span class="vacancy-list__right">{{$applicant['driving_license'] == 1 ? 'Есть' : 'Нет прав'}}</span>
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Рекомендации:</span>
                                            <span class="vacancy-list__right">{{$applicant['recommendations'] == 1 ? 'Есть' : 'Нет'}}</span>
                                        </li>
                                    </ul>
                                    <div class="schedule">
                                        <h3 class="schedule__title grey-line">Рабочий график на ближайшее время</h3>
                                        <div class="schedule__table table-schedule">
                                            <div class="table-schedule__row table-schedule__row_bol">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        Время <span>/ Дни недели</span>
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">Пн</div>
                                                <div class="table-schedule__col">Вт</div>
                                                <div class="table-schedule__col">Ср</div>
                                                <div class="table-schedule__col">Чт</div>
                                                <div class="table-schedule__col">Пт</div>
                                                <div class="table-schedule__col">Сб</div>
                                                <div class="table-schedule__col">Вс</div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Раннее утро</span> (06—09)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Утро</span> (09—12)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Начало дня</span> (12—15)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Конец дня</span> (15—18)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Вечер</span> (18—21)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Поздний вечер</span> (21—24)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                            <div class="table-schedule__row">
                                                <div class="table-schedule__col">
                                                    <div>
                                                        <span>Ночь</span> (24—06)
                                                    </div>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__ok">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-ok')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                                <div class="table-schedule__col">
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon__x">
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-x')}}"></use>
                                                        </svg>
                                                    </i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pages__item pages__item_mob response">
                            <h3 class="title-style-four">Комментарии Rusnyanya</h3>
                            <div class="hail hail_mod">
                                <div class="hail__item item-hail mt20">
                                    <div class="item-hail__content">
                                        <div class="item-hail__top">
                                            <div class="item-hail__date">15.09.2020</div>
                                        </div>
                                        <div class="item-hail__text">
                                            <p>{{$applicant->about}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="hail__item item-hail">
                                    <div class="item-hail__content">
                                        <div class="item-hail__top">
                                            <div class="item-hail__date">14.09.2020</div>
                                        </div>
                                        <div class="item-hail__text">
                                            <p>Уважаемые работодатели, по вопросам найма данного кандидата,
                                                просьба обращаться
                                                в watsapp +7495081654 или на почту <a
                                                    href="mailto:info@rusnyanya.com">info@rusnyanya.com</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-con__aside">
                    <div class="aside-baner">
                        <h3 class="aside-baner__title">Увеличьте ваши шансы найти работника!</h3>
                        <div class="aside-baner__text aside-baner__text_mar">
                            <p>Связывайтесь с кандидатами напрямую или получите профессиональную помощь
                                в поиске кандидата</p>
                        </div>
                        <div class="aside-baner__btn">
                            <a href="{{ route('tariffs') }}" class="site-btn btn-site-two">Посмотреть тарифы</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- попап -->
    <div class="popup js-popup" data-id-popup="popup-0">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_registr  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="autorization">
                        <div class="title-style-four popup__title">Авторизуйтесь</div>
                        <div class="popup__text">
                            <p>Если уже зарегистрированы</p>
                        </div>
                        <div class="popup__form">
                            <form action="#" class="js-form-validation">
                                <div class="el-form-row">
                                    <div class="el-form js-el-form">
                                        <span class="text-form-item">Логин или email:</span>
                                        <input type="text" name="email" data-text-valid="This is a valid field"
                                               class="type-input">
                                    </div>
                                </div>
                                <div class="el-form-row">
                                    <div class="el-form js-el-form">
                                        <span class="text-form-item">Пароль:</span>
                                        <input type="password" class="type-input">
                                    </div>
                                </div>
                                <div class="form-row-btn form-row-btn_f">
                                    <label class="el-check">
                                        <input type="checkbox" class="form-checkbox" checked>
                                        <i class="shecked-icon"></i>
                                        <span class="check-text">Запомнить меня</span>
                                    </label>
                                    <button type="submit" class="site-btn bnt-type-one bnt-type-one2 f_15 js-bnt-valid">Войти</button>
                                </div>
                                <div class="form-row-btn form-row-btn_end">
                                    <a href="popup-3" class="type-link-dot js-btn-popup">Забыли пароль?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="applicants">
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Нет аккаунта?</div>

                            <div class="popup__btn-row">
                                <a href="popup-0-sois" class="btn-site js-btn-popup btn-site-six mind-auto">Зарегистрироваться</a>
                            </div>
                        </div>

                        <div class="applicants__item">
                            <div class="title-style-four popup__title">Соискателям</div>
                            <div class="popup__text">
                                <p>Если вы хотите найти работу
                                    и создать анкету на нашем сайте</p>
                            </div>
                            <div class="popup__btn-row">
                                <a href="#" class="btn-site btn-site-six">Создать анкету</a>
                            </div>
                        </div>
                        <div class="applicants__item">
                            <div class="title-style-four popup__title">Работодателям</div>
                            <div class="popup__text">
                                <p>Вы можете совершенно бесплатно
                                    добавить вакансию о поиске работника.</p>
                            </div>
                            <div class="popup__btn-row">
                                <a href="#" class="btn-site btn-site-six">Добавить вакансию</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- не знаю пр какой кнопке будет вызываться http://joxi.ru/brR1oq4COMN7LA -->
    <div class="popup js-popup" data-id-popup="popup-0-sois">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_registr  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="autorization">
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Соискателям</div>
                            <div class="popup__text">
                                <p>Если вы хотите найти работу
                                    и создать анкету на нашем сайте</p>
                            </div>
                            <div class="popup__btn-row">
                                <a href="#" class="btn-site btn-site-six">Создать анкету</a>
                            </div>
                        </div>
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Работодателям</div>
                            <div class="popup__text">
                                <p>Вы можете совершенно бесплатно
                                    добавить вакансию о поиске работника.</p>
                            </div>
                            <div class="popup__btn-row">
                                <a href="#" class="btn-site btn-site-six">Добавить вакансию</a>
                            </div>
                        </div>

                    </div>
                    <div class="applicants">
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Уже зарегистрированы?</div>
                            <div class="popup__btn-row">
                                <a href="popup-0" class="btn-site js-btn-popup bnt-type-one">Войти в кабинет</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="popup js-popup" data-id-popup="popup-1">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_stand  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="popup__content">
                        <div class="title-style-four popup__title text-center">Показ контактных данных</div>
                        <div class="popup__text text-center">
                            <p>У вас есть возможность посмотреть еще 6 контактных данных.
                                Вы хотите воспользоваться этим?</p>
                        </div>
                        <div class="popup__btn_two">
                            <a href="#" class="site-btn bnt-type-one f_15 btn-site-7">Отмена</a>
                            <a href="#" class="site-btn bnt-type-one f_15">Показать контакты</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- попап -->
    <div class="popup popup-payment js-popup" data-id-popup="popup-2">
        <div class="popup__cont no-center">
            <div class="popup__wrapp popup__wrapp_payment  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="popup-payment__left">
                        <div class="payment__prise">
                            <div class="payment__prise-text">К оплате</div>
                            <div class="payment__prise-price"><span>EUR</span> 25.00</div>
                        </div>
                        <div class="payment__left-bot">
                            <a href="#" class="btn-type-9">
                                <svg>
                                    <use xlink:href="../img/svg/sprite.svg#icon-ar-left"></use>
                                </svg>
                                <span>Отменить оплату</span>
                            </a>
                        </div>
                    </div>

                    <div class="popup-payment__right">
                        <div class="title-style-four popup__title">Оплата тарифа </div>
                        <div class="popup-payment__row">
                            <div class="popup__text">
                                <p>Оплатите с помощью карты</p>
                            </div>
                            <div class="popup-payment__cart">
                                <img src="img/visa.png" alt="img">
                                <img src="img/mastercart.png" alt="img">
                            </div>
                        </div>
                        <div class="popup__form">
                            <form action="#">
                                <div class="el-form-row">
                                    <div class="el-form">
                                        <span class="text-form-item">Номер кредитной карты:</span>
                                        <input type="text" name="card" data-text-valid="This is a valid field"
                                               class="type-input" placeholder="4584 - ">
                                    </div>
                                </div>
                                <span class="text-form-item">Срок действия:</span>
                                <div class="el-form-row ">
                                    <div class="cols-row">
                                        <div class="el-form">
                                            <div class="el-select el-select_filter mtpc0 js-select">

                                                <div class="el-select__main js-select-main">
                                                    <span class="el-select__text js-select-text">Все страны</span>
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon_ar-bot">
                                                            <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                </div>

                                                <div class="el-select__content wrap-select-tarifs js-el-select-content">
                                                    <ul class="el-select__list el-select__tarifs">
                                                        <li class="el-select__item js-select-item el-select__item_active"
                                                            data-value="Все страны">Все страны</li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="el-form">
                                            <div class="el-select el-select_filter mtpc0 js-select">

                                                <div class="el-select__main js-select-main">
                                                    <span class="el-select__text js-select-text">Все страны</span>
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon_ar-bot">
                                                            <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                </div>

                                                <div class="el-select__content wrap-select-tarifs js-el-select-content">
                                                    <ul class="el-select__list el-select__tarifs">
                                                        <li class="el-select__item js-select-item el-select__item_active"
                                                            data-value="Все страны">Все страны</li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-form-row">
                                    <div class="cols-row">
                                        <div class="el-form">
                                        <span class="text-form-item">CVC/CW
                                        </span>
                                            <input type="text" name="card" data-text-valid="This is a valid field"
                                                   class="type-input" placeholder="341">
                                        </div>
                                        <div class="el-form">
                                            <span class="info-text-dop">Код из 3 или 4 цифр</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-form-row">
                                    <label class="el-check el-check-top">
                                        <input type="checkbox" class="form-checkbox" checked>
                                        <i class="shecked-icon"></i>
                                        <span class="check-text">Соглашаюсь с условиями оказания услуг</span>
                                    </label>
                                </div>

                                <div class="form-row-btn form-row-btn_mar">
                                    <button type="submit" class="site-btn bnt-type-one bnt-type-one2 f_15 ">Перейти к оплате <span>EUR
                                        25.00</span></button>
                                </div>

                                <div class="popup__dop-b">
                                    <div class="popup__text text-center">
                                        <p>Или выберите другой способ оплаты</p>
                                    </div>
                                    <div class="el-form-row">
                                        <a href="#" class="btn-pay">
                                            <span>Оплатить через</span>
                                            <img src="img/img-pay2.png" alt="img">
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="popup js-popup" data-id-popup="popup-3">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_stand  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="autorization popup__forget">
                        <div class="title-style-four popup__title">Восстановление пароля</div>
                        <div class="popup__form">
                            <form action="#" class="js-form-validation">
                                <div class="el-form-row">
                                    <div class="el-form js-el-form">
                                        <span class="text-form-item">Email:</span>
                                        <input type="text" name="email" data-text-valid="This is a valid field"
                                               class="type-input">
                                    </div>
                                </div>
                                <div class="form-row-btn form-row-btn__center">
                                    <button type="submit" class="site-btn bnt-type-one f_15 js-bnt-valid">Восстановить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><!-- кнопка прокрутки вверх -->
    <div class="scroll-up js-scroll-up">
        <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
            <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
        </svg>
    </div>

    <div class="body__opas js-boyd--opas"></div>
@endsection



@section('js')
    <script>
        @auth
        $('.questionnaire__heart').on('click', function (){
            let userId = $(this).attr('data-user');

            $.ajax({
                url: "{{ route('favorite.add') }}",
                method: "POST",
                data: {userId},
                success: response => {
                    alert(response.message)
                    $('.questionnaire__heart .svg-icon:nth-child(2)').toggleClass('active');
                },
                error: error => {
                    console.log(error);
                }
            });
        });
        @endauth
    </script>
@endsection
