@extends('layouts.app')

@section('title') Анкеты @endsection

@section('main_content')
    <div class="section-con-ankets">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="el-filetr">
                        <div class="el-filetr__item">
                            <h2 class="el-filetr__link el-filetr__link_active ankets-mt">Найдено {{ $count }} анкеты</h2>
                        </div>
                        <div class="el-filetr__item el-filetr__item_mob">

                            <div class="el-select el-select_servise js-select">
                                <div class="el-select__main el-select__main-ankets js-select-main">
                                    <i class="svg-icon">
                                        <svg class="svg-icon_arrows">
                                            <use xlink:href="img/svg/sprite.svg#arrowTopBot"></use>
                                        </svg>
                                    </i>
                                    <span class="el-select__text js-select-text">Сортировка анкет</span>
                                </div>
                                <div class="el-select__content js-el-select-content">
                                    <ul class="el-select__list">
                                        <li class="el-select__item js-select-item {{ request()->get(' ') ? 'el-select__item_active' : '' }}"
                                            data-value="Все специальности">
                                            <a href="{{ route('questionnaires.list') }}">Все анкеты</a>
                                        </li>
                                        <li class="el-select__item js-select-item {{ request()->get('views') == 1 ? 'el-select__item_active' : '' }}" >
                                            <a href="{{ route('questionnaires.list', ['views'=> 1]) }}">По рейтингу</a>
                                        </li>
                                        <li class="el-select__item js-select-item {{ request()->get('image') == 1 ? 'el-select__item_active' : '' }}" >
                                            <a href="{{ route('questionnaires.list', ['image' => 1 ]) }}">С фотографией</a>
                                        </li>
                                        <li class="el-select__item js-select-item {{ request()->get('active') == 1 ? 'el-select__item_active' : '' }}">
                                            <a href="{{ route('questionnaires.list', ['active' => 1 ]) }}"> По активности</a>
                                        </li>
                                        <li class="el-select__item js-select-item {{ request()->get('open') == 1 ? 'el-select__item_active' : '' }}">
                                            <a href="{{ route('questionnaires.list', ['open' => 1 ]) }}"> С открытыми контактами</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="el-filetr__item el-filetr__item_fil">
                            <a href="{{ route('questionnaires.list') }}" class="btn-filter js-bnt-filter">
                                <i class="svg-icon">
                                    <svg>
                                        <use xlink:href="{{ asset('img/svg/sprite.svg#icon-filter') }}"></use>
                                    </svg>
                                </i>
                                <span class="btn-filter__col">4</span>
                            </a>
                        </div>
                    </div>

                    <div class="news">
                        @foreach($applicants as $applicant)
                            <div class="news__item new-item new-item_mod new-item-height">
                                <div class="new-item__img js-row-js">
                                    <div class="new-item__img-slider js-new-item">
                                        <div class="new-item__img-img">
{{--                                              @foreach("{$applicant->avatar}" as $avatar)--}}
                                                <img src="{{ asset("$applicant->avatar") }}" alt="img">
                                                <a href="{{ route('questionnaires.show', $applicant->id) }}" target="_blank" class="new-item__img-link"></a>
{{--                                              @endforeach--}}
                                        </div>
                                    </div>
                                    <div class="slider-dots slider-dots_img js-slider-dots"></div>
                                </div>
                                <div class="new-item__content active">
                                    <!-- добавить класс активе для отображения -->
                                    <div class="new-item__top-item new-mob ">
                                        <div class="new-item__top-col new-item__id"><span>{{$applicant->author->num_views}}</span></div>
                                        <div class="new-item__top-col new-item__infos new-item__infos_email">
                                            <a href="#">
                                                <i class="svg-icon">
                                                    <svg>
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-emal')}}"></use>
                                                    </svg>
                                                </i>
                                            </a>
                                        </div>
                                        @auth
                                            <div class=" new-item__top-col new-item__infos new-item__infos_heart">
                                                <!-- добавить класс active -->
                                                <a href="javascript:void(0);" class="icon-heart" data-user="{{ $applicant->user_id }}">
                                                    <i class="svg-icon">
                                                        <svg>
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-heart-star')}}"></use>
                                                        </svg>
                                                    </i>
                                                    <i class="svg-icon {{ in_array($applicant->user_id, $favoriteIds) ? 'active' : '' }}">
                                                        <svg>
                                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-heart')}}"></use>
                                                        </svg>
                                                    </i>
                                                </a>
                                            </div>
                                        @endauth
                                    </div>
                                    <a href="{{ route('questionnaires.show', $applicant->id) }}" target="_blank" class="new-item__title">{{ $applicant->author->name }}, {{$applicant->age}}</a>
                                    <div class="worker-item__info">{{ $applicant->speciality  }}</div>
                                    <div class="el-info new-item__el-info">
                                        <div class="el-info__item">
                                            <i class="svg-icon">
                                                <svg>
                                                    <use xlink:href="img/svg/sprite.svg#label-two"></use>
                                                </svg>
                                            </i>
                                            <span class="el-info__text">{{ $applicant->country_name }} {{ $applicant->city_name }} {{ $applicant->district_name }}</span>
                                        </div>
                                        <div class="el-info__item">
                                            <i class="svg-icon">
                                                <svg>
                                                    <use xlink:href="img/svg/sprite.svg#icon-money"></use>
                                                </svg>
                                            </i>
                                            <span class="el-info__text">{{ $applicant->salary }} {{ $applicant->price_period_id == "0" ? 'в час' : ' в месяц'}}</span>
                                        </div>
                                    </div>
                                    <div class="new-item__text">
                                        <p>
                                            {{\Illuminate\Support\Str::limit($applicant->comments, 160, $end=' ...')}}
                                            </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    {{ $applicants->links('pagination.custom') }}
                </div>

                <div class="section-con__aside section-con__aside-ankents">
                    <div class="wr-filters js-wr-filters">
                        @guest
                        <div class="filter">
                            <div class="filter__title" id="drop_a">Фильтры</div>

                            <ul class="filter__list filter-list drop-fil">
                                <li class="filter-list__item drop-form filter-list__item_none">
                                    <form action="{{ route('questionnaires.list') }}" method="get">
                                        <input type="hidden" name="search" value="1" >
                                        <div class="filter__title">Кого вы ищите ?</div>

                                        <div class="filter__row">
                                                    <div class="filter__row-item">
                                                        <ul class="filter__list filter-list">
                                                            @foreach ($positions as $position)
                                                                <li class="filter-list__item">
                                                                    <label class="el-check">
                                                                        <input type="checkbox" class="form-checkbox" name="position[]" value="{{ $position->id }}">
                                                                        <i class="shecked-icon"></i>
                                                                        <span class="check-text">{{ $position->name }}</span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                        <div class="filter__row">
                                            <div class="filter__title">Место работы</div>
                                            <div class="filter__row-item">
                                                <div class="el-select el-select_filter   js-select">

                                                    <div class="el-select__main js-select-main">
                                                        <span class="el-select__text js-select-text">Все страны</span>
                                                        <i class="svg-icon">
                                                            <svg class="svg-icon_ar-bot">
                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                </use>
                                                            </svg>
                                                        </i>
                                                    </div>

                                                    <div class="el-select__content js-el-select-content">
                                                        <x-countries-select></x-countries-select>
                                                    </div>
                                                    <input type="hidden" class="country" style="display: none" name="country_id" value="">

                                                </div>
                                            </div>

                                            <div class="filter__row-item">
                                                <!-- l-select__not_selected -->
                                                <div class="el-select el-select_filter l-select__not_selected js-select">
                                                    <div class="el-select__main js-select-main">
                                                        <span class="el-select__text js-select-text">Не выбрано</span>
                                                        <i class="svg-icon">
                                                            <svg class="svg-icon_ar-bot">
                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                </use>
                                                            </svg>
                                                        </i>
                                                    </div>

                                                    <div class="el-select__content js-el-select-content" id="regions-select-area">

                                                    </div>
                                                    <input type="hidden" class="region" style="display: none" name="region_id" value="">

                                                </div>
                                            </div>

                                            <div class="filter__row-item">
                                                <!-- l-select__not_selected -->
                                                <div class="el-select el-select_filter l-select__not_selected js-select">
                                                    <div class="el-select__main js-select-main">
                                                        <span class="el-select__text js-select-text">Не выбрано</span>
                                                        <i class="svg-icon">
                                                            <svg class="svg-icon_ar-bot">
                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                </use>
                                                            </svg>
                                                        </i>
                                                    </div>

                                                    <div class="el-select__content js-el-select-content" id="cities-select-area">

                                                    </div>
                                                    <input type="hidden" class="city" style="display: none" name="city_id" value="">

                                                </div>
                                            </div>

                                        </div>

                                        <div class="filter__row">
                                            <div class="filter__title">Возраст</div>
                                            <div class="filter__row-item">
                                                <div class="filter-le-row">
                                                    <div class="filter-le__item">
                                                        <span class="filter-le__text">от</span>
                                                        <input type="number" class="filter-le__input filter-le_left js-in-number" value="18" name="from-age" placeholder="18">
                                                    </div>
                                                    <div class="filter-le__item">
                                                        <span class="filter-le__text">до</span>
                                                        <input type="number" class="filter-le__input filter-le_right js-in-number" value="60" name="before-age" placeholder="60">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="filter__row">
                                            <div class="filter__title">Родной язык</div>
                                            <div class="filter__row-item">
                                                <ul class="filter__list filter-list js-filter-list">
                                                    <li class="filter-list__item js-fil-list filter-list__item_none">
                                                        <ul>
                                                            @foreach ($languages as $language)
                                                                <li class="filter-list__item">
                                                                    <label class="el-check">
                                                                        <input type="checkbox" class="form-checkbox" name="own-language[]" value="{{ $language->id }}">
                                                                        <i class="shecked-icon"></i>
                                                                        <span class="check-text">{{$language->name}}</span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    <li class="filter-list__item">
                                                        <a href="" class="filter-list__all js-fil-list-all" data-text="Свернуть">
                                                            <span>Открыть все</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="filter__row">
                                            <div class="filter__title">Знание языков</div>
                                            <div class="filter__row-item">
                                                <ul class="filter__list filter-list js-filter-list">
                                                    <li class="filter-list__item js-fil-list filter-list__item_none">
                                                        <ul>
                                                            @foreach ($languages as $language)
                                                                <li class="filter-list__item">
                                                                    <label class="el-check">
                                                                        <input type="checkbox" class="form-checkbox" name="other-language[]" value="{{ $language->id }}">
                                                                        <i class="shecked-icon"></i>
                                                                        <span class="check-text">{{$language->name}}</span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    <li class="filter-list__item">
                                                        <a href="" class="filter-list__all js-fil-list-all"
                                                           data-text="Свернуть">
                                                            <span>Открыть все</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="filter__row">
                                            <div class="filter__title">Документы</div>
                                            <div class="filter__row-item">
                                                <ul class="filter__list filter-list drop-fil1">
                                                    <li class="filter-list__item drop-form1 filter-list__item_none">
                                                        <ul class="filter__list filter-list">
                                                            @foreach ($documents as $document)
                                                                <li class="filter-list__item">
                                                                    <label class="el-check">
                                                                        <input type="checkbox" class="form-checkbox" name="document[]" value="{{ $document->id }}">
                                                                        <i class="shecked-icon"></i>
                                                                        <span class="check-text">{{ $document->name }}</span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    <li class="filter-list__item">
                                                        <a href="" class="filter-list__all drop" id="dropdown_ctm1" data-text="Свернуть">
                                                            <span>Открыть</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="filter__row">
                                            <div class="filter__title">Проживание</div>
                                            <div class="filter__row-item">
                                                <ul class="filter__list filter-list drop-fil2">
                                                    <li class="filter-list__item drop-form2 filter-list__item_none">
                                                        <ul class="filter__list filter-list">
                                                            @foreach ($accommodations as $accommodation)
                                                                <li class="filter-list__item">
                                                                    <label class="el-check">
                                                                        <input type="checkbox" class="form-checkbox" name="accommodation" value="{{ $accommodation->id }}">
                                                                        <i class="shecked-icon"></i>
                                                                        <span class="check-text">{{ $accommodation->name }}</span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    <li class="filter-list__item">
                                                        <a href="" class="filter-list__all drop" id="dropdown_ctm2" data-text="Свернуть">
                                                            <span>Открыть</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="filter__row">
                                            <div class="filter__title">Дополнительно</div>
                                            <div class="filter__row-item">
                                                <ul class="filter__list filter-list">
                                                    <li class="filter-list__item">
                                                        <label class="el-check">
                                                            <input type="checkbox" class="form-checkbox" name="driver-license" value="1">
                                                            <i class="shecked-icon"></i>
                                                            <span class="check-text">Наличие прав</span>
                                                        </label>
                                                    </li>
                                                    <li class="filter-list__item">
                                                        <label class="el-check">
                                                            <input type="checkbox" class="form-checkbox" name="recommendations" value="1">
                                                            <i class="shecked-icon"></i>
                                                            <span class="check-text">Рекомендации</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="filter__row">
                                            <div class="filter__title">Поиск по слову, id</div>
                                            <div class="filter__row-item">
                                                <label class="row-lab">
                                                    <input type="text" class="type-input type-input_search"  name="id" value="">
                                                </label>
                                            </div>
                                        </div>
                                        <div class="filter__row filter__row_btn">
                                            <div class="filter__row-item filter__row-btn">
                                                <button class="site-btn bnt-type-one btn-fon-15 btn-100">Искать анкеты</button>
                                            </div>
                                        </div>
                                    </form>
                                </li>
                                <li class="filter-list__item">
                                    <a href="" class="filter-list__all drop" id="dropdown_ctm" data-text="Свернуть">
                                        <span>Открыть</span>
                                        <i class="svg-icon">
                                            <svg class="svg-icon_ar-bot">
                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                </use>
                                            </svg>
                                        </i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        @else
                            <div class="filter">
                                 <form action="{{ route('questionnaires.list') }}" method="get">
                                            <input type="hidden" name="search" value="1" >
                                            <div class="filter__title">Кого вы ищите ?</div>

                                            <div class="filter__row">
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list">
                                                        @foreach ($positions as $position)
                                                            <li class="filter-list__item">
                                                                <label class="el-check">
                                                                    <input type="checkbox" class="form-checkbox" name="position[]" value="{{ $position->id }}">
                                                                    <i class="shecked-icon"></i>
                                                                    <span class="check-text">{{ $position->name }}</span>
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="filter__row">
                                                <div class="filter__title">Место работы</div>
                                                <div class="filter__row-item">
                                                    <div class="el-select el-select_filter   js-select">

                                                        <div class="el-select__main js-select-main">
                                                            <span class="el-select__text js-select-text">Все страны</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </div>

                                                        <div class="el-select__content js-el-select-content">
                                                            <x-countries-select></x-countries-select>
                                                        </div>
                                                        <input type="hidden" class="country" style="display: none" name="country_id" value="">

                                                    </div>
                                                </div>

                                                <div class="filter__row-item">
                                                    <!-- l-select__not_selected -->
                                                    <div class="el-select el-select_filter l-select__not_selected js-select">
                                                        <div class="el-select__main js-select-main">
                                                            <span class="el-select__text js-select-text">Не выбрано</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </div>

                                                        <div class="el-select__content js-el-select-content" id="regions-select-area">

                                                        </div>
                                                        <input type="hidden" class="region" style="display: none" name="region_id" value="">

                                                    </div>
                                                </div>

                                                <div class="filter__row-item">
                                                    <!-- l-select__not_selected -->
                                                    <div class="el-select el-select_filter l-select__not_selected js-select">
                                                        <div class="el-select__main js-select-main">
                                                            <span class="el-select__text js-select-text">Не выбрано</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                    </use>
                                                                </svg>
                                                            </i>
                                                        </div>

                                                        <div class="el-select__content js-el-select-content" id="cities-select-area">

                                                        </div>
                                                        <input type="hidden" class="city" style="display: none" name="city_id" value="">

                                                    </div>
                                                </div>

                                            </div>

                                            <div class="filter__row">
                                                <div class="filter__title">Возраст</div>
                                                <div class="filter__row-item">
                                                    <div class="filter-le-row">
                                                        <div class="filter-le__item">
                                                            <span class="filter-le__text">от</span>
                                                            <input type="number" class="filter-le__input filter-le_left js-in-number" value="18" name="from-age" placeholder="18">
                                                        </div>
                                                        <div class="filter-le__item">
                                                            <span class="filter-le__text">до</span>
                                                            <input type="number" class="filter-le__input filter-le_right js-in-number" value="60" name="before-age" placeholder="60">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="filter__row">
                                                <div class="filter__title">Родной язык</div>
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list js-filter-list">
                                                        <li class="filter-list__item js-fil-list filter-list__item_none">
                                                            <ul>
                                                                @foreach ($languages as $language)
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox" class="form-checkbox" name="own-language[]" value="{{ $language->id }}">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">{{$language->name}}</span>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        <li class="filter-list__item">
                                                            <a href="" class="filter-list__all js-fil-list-all" data-text="Свернуть">
                                                                <span>Открыть все</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                        </use>
                                                                    </svg>
                                                                </i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="filter__row">
                                                <div class="filter__title">Знание языков</div>
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list js-filter-list">
                                                        <li class="filter-list__item js-fil-list filter-list__item_none">
                                                            <ul>
                                                                @foreach ($languages as $language)
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox" class="form-checkbox" name="other-language[]" value="{{ $language->id }}">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">{{$language->name}}</span>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        <li class="filter-list__item">
                                                            <a href="" class="filter-list__all js-fil-list-all"
                                                               data-text="Свернуть">
                                                                <span>Открыть все</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                        </use>
                                                                    </svg>
                                                                </i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="filter__row">
                                                <div class="filter__title">Документы</div>
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list drop-fil1">
                                                        <li class="filter-list__item drop-form1 filter-list__item_none">
                                                            <ul class="filter__list filter-list">
                                                                @foreach ($documents as $document)
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox" class="form-checkbox" name="document[]" value="{{ $document->id }}">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">{{ $document->name }}</span>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        <li class="filter-list__item">
                                                            <a href="" class="filter-list__all drop" id="dropdown_ctm1" data-text="Свернуть">
                                                                <span>Открыть</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                        </use>
                                                                    </svg>
                                                                </i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="filter__row">
                                                <div class="filter__title">Проживание</div>
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list drop-fil2">
                                                        <li class="filter-list__item drop-form2 filter-list__item_none">
                                                            <ul class="filter__list filter-list">
                                                                @foreach ($accommodations as $accommodation)
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox" class="form-checkbox" name="accommodation" value="{{ $accommodation->id }}">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">{{ $accommodation->name }}</span>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                        <li class="filter-list__item">
                                                            <a href="" class="filter-list__all drop" id="dropdown_ctm2" data-text="Свернуть">
                                                                <span>Открыть</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                        </use>
                                                                    </svg>
                                                                </i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="filter__row">
                                                <div class="filter__title">Дополнительно</div>
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list">
                                                        <li class="filter-list__item">
                                                            <label class="el-check">
                                                                <input type="checkbox" class="form-checkbox" name="driver-license" value="1">
                                                                <i class="shecked-icon"></i>
                                                                <span class="check-text">Наличие прав</span>
                                                            </label>
                                                        </li>
                                                        <li class="filter-list__item">
                                                            <label class="el-check">
                                                                <input type="checkbox" class="form-checkbox" name="recommendations" value="1">
                                                                <i class="shecked-icon"></i>
                                                                <span class="check-text">Рекомендации</span>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="filter__row">
                                                <div class="filter__title">Поиск по слову, id</div>
                                                <div class="filter__row-item">
                                                    <label class="row-lab">
                                                        <input type="text" class="type-input type-input_search"  name="id" value="">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="filter__row filter__row_btn">
                                                <div class="filter__row-item filter__row-btn">
                                                    <button class="site-btn bnt-type-one btn-fon-15 btn-100">Искать анкеты</button>
                                                </div>
                                            </div>
                                        </form>
                            </div>

                        @endauth
                    </div>
                    @guest
                        <div class="section-con__aside">
                            <div class="aside-baner">
                                <div class="aside-baner__img">
                                    <img src="{{asset('img/img-baner.png')}}" alt="img">
                                </div>
                                <div class="aside-baner__text aside-baner__text_mar">
                                    <p>Бесплатно регистрируйтесь и разместите вашу анкету или вакансию. Получайте прямые
                                        предложения по всей Европе</p>
                                </div>
                                <div class="aside-baner__btn">
                                    <a href="{{ route('register', ['type' => 'vacancies', 'auth_required' => true]) }}" class="site-btn btn-site-two">Добавить вакансию</a>
                                    <a href="{{ route('register', ['type' => 'questionnaires']) }}" class="site-btn btn-site-two">Добавить анкету</a>
                                </div>
                            </div>
                        </div>
                    @endauth
                </div>

            </div>

        </div>
    </div>


    <!-- не знаю пр какой кнопке будет вызываться http://joxi.ru/brR1oq4COMN7LA -->
    <div class="popup js-popup" data-id-popup="popup-0-sois">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_registr  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="autorization">
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Соискателям</div>
                            <div class="popup__text">
                                <p>Если вы хотите найти работу
                                    и создать анкету на нашем сайте</p>
                            </div>
                            <div class="popup__btn-row">
                                <a href="#" class="btn-site btn-site-six">Создать анкету</a>
                            </div>
                        </div>
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Работодателям</div>
                            <div class="popup__text">
                                <p>Вы можете совершенно бесплатно
                                    добавить вакансию о поиске работника.</p>
                            </div>
                            <div class="popup__btn-row">
                                <a href="#" class="btn-site btn-site-six">Добавить вакансию</a>
                            </div>
                        </div>

                    </div>
                    <div class="applicants">
                        <div class="applicants__item applicants__item_mobile">
                            <div class="title-style-four popup__title">Уже зарегистрированы?</div>
                            <div class="popup__btn-row">
                                <a href="popup-0" class="btn-site js-btn-popup bnt-type-one">Войти в кабинет</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="popup js-popup" data-id-popup="popup-1">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_stand  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="popup__content">
                        <div class="title-style-four popup__title text-center">Показ контактных данных</div>
                        <div class="popup__text text-center">
                            <p>У вас есть возможность посмотреть еще 6 контактных данных.
                                Вы хотите воспользоваться этим?</p>
                        </div>
                        <div class="popup__btn_two">
                            <a href="#" class="site-btn bnt-type-one f_15 btn-site-7">Отмена</a>
                            <a href="#" class="site-btn bnt-type-one f_15">Показать контакты</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- кнопка прокрутки вверх -->
    <div class="scroll-up js-scroll-up">
        <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
            <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
        </svg>
    </div>

    <div class="body__opas js-boyd--opas"></div>
@endsection


@section('js')
    <script>
        @auth
            $('.icon-heart').on('click', function (){
                let userId = $(this).attr('data-user');

                $.ajax({
                    url: "{{ route('favorite.add') }}",
                    method: "POST",
                    data: {userId},
                    success: response => {
                        alert(response.message)
                        $(`.icon-heart[data-user=${userId}] .svg-icon:nth-child(2)`).toggleClass('active');
                    },
                    error: error => {
                        console.log(error);
                    }
                });
            });
        @endauth
    </script>
@endsection
