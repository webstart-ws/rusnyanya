@extends('layouts.app')

@section('title') Редактирование профиля @endsection

{{--@section('css')--}}
<link rel="stylesheet" href="{{ asset('css/register.css') }}">
{{--@endsection--}}

@section('main_content')
    <div class="section-con section-con__top-pad section-con_mob-pad section-con_no-cont section-reg">
        <div class="container" id="create-questionnaire-page">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages pages__cont">
                        <form action="{{ route('questionnaire.profile.edit') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="pages__item pages-anket">
                                <div class="vacancy">
                                    <h1 class="title-style-tree">Редактирование профиля</h1>
                                    <div class="vacancy__info">Все поля обязательны к заполнению.</div>
                                    <input type="hidden" name="id" value="{{ $applicant->user_id }}">
                                </div>
                            </div>
                            <div class="pages__item response">
                                <div class="page-forms">
                                    <div class="page-forms__cont page-forms__cont_active js-page-forms__one page-forms__one mt-0" data-pag-form="0">
                                        <div class="page-forms__max-w">
                                            <div class="title-form-h mt-0">Основные данные</div>

                                            <div class="form-row-wr">
                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Ваше имя:

                                                        </span>
                                                        @if($errors->has('name'))
                                                            <div class="validation-error-text">{{ $errors->first('name') }}</div>
                                                        @endif
                                                        <input type="text" data-text-valid="This is a valid field" value="{{ old('name') ?? $applicant->author->name }}" name="name" class="type-input js-valid-in">

                                                    </div>
                                                    <div class="el-form">
                                                        <span class="text-form-item ">Фамилия:

                                                        </span>
                                                        @if($errors->has('fam'))
                                                            <div class="validation-error-text">{{ $errors->first('fam') }}</div>
                                                        @endif
                                                        <input type="text" data-text-valid="This is a valid field" value="{{ old('fam') ?? $applicant->author->fam }}" name="fam" class="type-input js-valid-in">

                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form js-el-form">
                                                        <span class="text-form-item">Электронное письмо:

                                                        </span>
                                                        @if($errors->has('email'))
                                                            <div class="validation-error-text">{{ $errors->first('email') }}</div>
                                                        @endif
                                                        <input type="text" data-text-valid="This is a valid field" id="email" value="{{ old('email') ?? $applicant->author->email }}" name="email" class="type-input">

                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form js-el-form">
                                                        <span class="text-form-item inactiv_focuse_title">Пароль:

                                                        </span>
                                                        @if($errors->has('password'))
                                                            <div class="validation-error-text">{{ $errors->first('password') }}</div>
                                                        @endif
                                                        <input type="password" data-text-valid="min 6 characters" id="pwd" name="password" class="type-input js-password" value="{{ old('password') }}">
                                                        <span class="show_pass">
                                                            <svg class="svg_one_pass" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0z"/>
                                                                <path fill="rgba(111,127,137,0.3)" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.822 9.822 0 0 1-17.64 0A9.77 9.77 0 0 1 12 6m0-2a11.827 11.827 0 0 0-11 7.5 11.817 11.817 0 0 0 22 0A11.827 11.827 0 0 0 12 4zm0 5a2.5 2.5 0 1 1-2.5 2.5A2.5 2.5 0 0 1 12 9m0-2a4.5 4.5 0 1 0 4.5 4.5A4.507 4.507 0 0 0 12 7z"/>
                                                            </svg>
                                                            <svg class="svg_one_pass_act" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z"/>
                                                                <path fill="#2d3133" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.647 9.647 0 0 1-2.41 3.12l1.41 1.41A11.8 11.8 0 0 0 23 11.5 11.834 11.834 0 0 0 8.36 4.57l1.65 1.65A10.108 10.108 0 0 1 12 6zm-1.07 1.14L13 9.21a2.5 2.5 0 0 1 1.28 1.28l2.07 2.07a4.679 4.679 0 0 0 .14-1.07A4.483 4.483 0 0 0 12 7a4.244 4.244 0 0 0-1.07.14zM2.01 3.87l2.68 2.68A11.738 11.738 0 0 0 1 11.5 11.827 11.827 0 0 0 12 19a11.73 11.73 0 0 0 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45zm7.5 7.5l2.61 2.61A.5.5 0 0 1 12 14a2.5 2.5 0 0 1-2.5-2.5c0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75a4.6 4.6 0 0 0-.36 1.78 4.505 4.505 0 0 0 6.27 4.14l.98.98A10.432 10.432 0 0 1 12 17a9.77 9.77 0 0 1-8.82-5.5 9.9 9.9 0 0 1 2.93-3.53z"/>
                                                            </svg>
                                                        </span>


                                                    </div>

                                                    <div class="el-form js-el-form">
                                                        <span class="text-form-item inactiv_focuse_title">Повторите пароль:

                                                        </span>
                                                        @if($errors->has('password_repeat'))
                                                            <div class="validation-error-text">{{ $errors->first('password_repeat') }}</div>
                                                        @endif
                                                        <input type="password" data-text-valid="Не совпадает" name="password_repeat" id="pwd_two" class="type-input js-password" value="{{ old('password_repeat') }}">
                                                        <span class="show_pass_two">
                                                            <svg class="svg_one_pass1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0z"/>
                                                                <path fill="rgba(111,127,137,0.3)" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.822 9.822 0 0 1-17.64 0A9.77 9.77 0 0 1 12 6m0-2a11.827 11.827 0 0 0-11 7.5 11.817 11.817 0 0 0 22 0A11.827 11.827 0 0 0 12 4zm0 5a2.5 2.5 0 1 1-2.5 2.5A2.5 2.5 0 0 1 12 9m0-2a4.5 4.5 0 1 0 4.5 4.5A4.507 4.507 0 0 0 12 7z"/>
                                                            </svg>
                                                            <svg class="svg_one_pass_act1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z"/>
                                                                <path fill="#2d3133" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.647 9.647 0 0 1-2.41 3.12l1.41 1.41A11.8 11.8 0 0 0 23 11.5 11.834 11.834 0 0 0 8.36 4.57l1.65 1.65A10.108 10.108 0 0 1 12 6zm-1.07 1.14L13 9.21a2.5 2.5 0 0 1 1.28 1.28l2.07 2.07a4.679 4.679 0 0 0 .14-1.07A4.483 4.483 0 0 0 12 7a4.244 4.244 0 0 0-1.07.14zM2.01 3.87l2.68 2.68A11.738 11.738 0 0 0 1 11.5 11.827 11.827 0 0 0 12 19a11.73 11.73 0 0 0 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45zm7.5 7.5l2.61 2.61A.5.5 0 0 1 12 14a2.5 2.5 0 0 1-2.5-2.5c0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75a4.6 4.6 0 0 0-.36 1.78 4.505 4.505 0 0 0 6.27 4.14l.98.98A10.432 10.432 0 0 1 12 17a9.77 9.77 0 0 1-8.82-5.5 9.9 9.9 0 0 1 2.93-3.53z"/>
                                                            </svg>
                                                        </span>

                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Ищу работу в стране:
                                                        </span>
                                                        @if($errors->has('country_of_work_id'))
                                                            <div class="validation-error-text">{{ $errors->first('country_of_work_id') }}</div>
                                                        @endif
                                                        <div class="el-select el-select_filter js-select">
                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text" id="selected-work-country">Все страны</span>
                                                                <i class="svg-icon"><svg class="svg-icon_ar-bot"><use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use></svg></i>
                                                            </div>

                                                            <div class="el-select__content js-el-select-content">
                                                                <x-countries-select></x-countries-select>
                                                                <input type="hidden" name="country_of_work_id" value="{{ old('country_of_work_id') ?? $applicant->country->name}}" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="el-form">
                                                        <span class="text-form-item">Ищу работу в городе:
                                                        </span>
                                                        @if($errors->has('city_of_work_id'))
                                                            <div class="validation-error-text">{{ $errors->first('city_of_work_id') }}</div>
                                                        @endif
                                                        <div class="el-select el-select_filter js-select" id="work_cities_select_area">
                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text selected-work-city">Любой город</span>
                                                                <i class="svg-icon"><svg class="svg-icon_ar-bot"><use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use></svg></i>
                                                            </div>

                                                            <div class="el-select__content js-el-select-content" id="cities-select-area"></div>
                                                            <input type="hidden" name="city_of_work_id" value="{{ old('city_of_work_id')}}" />
{{--                                                            <input type="hidden" name="city_of_work_id" value="{{ old('city_of_work_id') ?? $applicant->city->Name}}" />--}}
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__two page-forms__two" data-pag-form="1">
                                        <div class="title-form-h">Ваши фотографии</div>

                                        <div class="el-form-row form-margin">
                                            <label class="img-form">
                                                <input type="file" name="images[]" id="register-images-input" accept="image/*" multiple="multiple" />
                                                <span class="img-form__text">Перетащите файлы сюда или нажмите кнопку ниже,
                                                    чтобы выбрать их на компьютере. Разрешается добавлять до 5 фотографий</span>
                                                <span class="img-form__btn">
                                                    <button class="site-btn bnt-type-one bnt-type-one3 f_15" id="file-choose-btn">Выберите файл</button>
                                                </span>
                                            </label>

                                            @error('images.*')
                                            <div class="validation-error-text">{{ $message }}</div>
                                            @enderror

                                            <div id="register-images">
                                                @foreach($applicant->images as $image)
                                                    <div>
                                                        <img class="thumbnail" src="{{ asset($image->path) }}" title="" alt="" />
                                                        <span class="close delete-btn" data-id="{{ $image->id }}">&times;</span>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="0">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="2">Далее</a>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="0">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="2">Далее</a>
                                        </div>

                                    </div>

                                    <div class="page-forms__cont js-page-forms__three page-forms__three" data-pag-form="2">
                                        <div class="title-form-h">Контактные данные</div>
                                        <div class="el-form-row">
                                            <p class="page-forms__text page-bottom">Укажите не менее двух контактов на Ваш выбор, по которым с вами могут связаться потенциальные работодатели.
                                            </p>
                                        </div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form js-el-form">
                                                <span class="text-form-item">E-mail:</span>
                                                <input type="text" data-text-valid="This is a valid field" id="mail" value="{{ old('email1') ?? $applicant->author->email1}}" name="email1" class="type-input">
                                                @if($errors->has('email1'))
                                                    <div class="validation-error-text">{{ $errors->first('email1') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form" id="tele">
                                                <span class="text-form-item">Телефон:</span>
                                                <input type="tel" id="phone" data-text-valid="This is a valid field" value="{{ old('phone') ?? $applicant->author->phone}}"
                                                       name="phone" class="type-input js-in-phone js-valid-in" />
                                                @if($errors->has('phone'))
                                                    <div class="validation-error-text">{{ $errors->first('phone') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form" id="tele">
                                                <span class="text-form-item">Whatsapp:</span>
                                                <input type="tel" id="phone1" data-text-valid="This is a valid field" value="{{ old('whatsapp') ?? $applicant->author->whatsapp}}"
                                                       name="whatsapp" class="type-input" />
                                                @if($errors->has('whatsapp'))
                                                    <div class="validation-error-text">{{ $errors->first('whatsapp') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form">
                                                <span class="text-form-item">Skype:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('skype') ?? $applicant->author->skype}}" name="skype" class="type-input">
                                                @if($errors->has('skype'))
                                                    <div class="validation-error-text">{{ $errors->first('skype') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Viber:</span>
                                                <input type="tel" id="phone2" data-text-valid="This is a valid field" value="{{ old('viber') ?? $applicant->author->viber}}"
                                                       name="viber" class="type-input js-in-phone" />
                                                @if($errors->has('viber'))
                                                    <div class="validation-error-text">{{ $errors->first('viber') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Ссылка на социальную сеть:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('linkSocial') ?? $applicant->author->soc_link}}" name="soc_link" class="type-input">
                                                @if($errors->has('linkSocial'))
                                                    <div class="validation-error-text">{{ $errors->first('linkSocial') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="1">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="3">Далее</a>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__four page-forms__four" data-pag-form="3">
                                        <div class="title-form-h">Общие данные</div>
                                        <div class="el-form-row">
                                            <p style="display: none" class="page-forms__text page-bottom">Укажите не менее двух контактов на выбор
                                                соискателя.</p>
                                        </div>


                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form">
                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Дата рождения:</span>
                                                        <div class="pos">
                                                            <input type="date" class="type-input type-input_date js-valid-in"
                                                                   name="birthdate" value="{{ old('birthdate') ?? $applicant->author->birthdate}}">
                                                        </div>
                                                        @if($errors->has('birthdate'))
                                                            <div class="validation-error-text">{{ $errors->first('birthdate') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Образование</span>
                                                        <div class="el-select el-select_filter   js-select">

                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text selected-education">Все образование</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>

                                                            <div class="el-select__content js-el-select-content">
                                                                <ul class="el-select__list">
                                                                    @foreach ($educations as $education)
                                                                        <li class="el-select__item js-select-item education-option {{ old('education_id') || $applicant->author->education_id == $education->id ? 'el-select__item_active' : '' }}"
                                                                            data-value="{{ $education->name }}"
                                                                            data-id="{{ $education->id }}"
                                                                        >{{ $education->name }}</li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                            <input type="hidden" name="education_id" value="{{ old('education_id') ?? $applicant->author->education_id}}" />
                                                            @if($errors->has('education_id'))
                                                                <div class="validation-error-text">{{ $errors->first('education_id') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Специальность:</span>
                                                        <input type="text" data-text-valid="This is a valid field" name="speciality"
                                                               class="type-input " value="{{ old('speciality') || $applicant->speciality}}">
                                                        @if($errors->has('speciality'))
                                                            <div class="validation-error-text">{{ $errors->first('speciality') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Гражданство:</span>
                                                        <input type="text" data-text-valid="This is a valid field" name="grazd"
                                                               class="type-input " value="{{ old('grazd') || $applicant->author->grazd}}">
                                                        @if($errors->has('grazd'))
                                                            <div class="validation-error-text">{{ $errors->first('grazd') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Выберите страну</span>
                                                        @if($errors->has('country_of_residence_id'))
                                                            <div class="validation-error-text">{{ $errors->first('country_of_residence_id') }}</div>
                                                        @endif
                                                        <div class="el-select el-select_filter   js-select">

                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text">Все страны</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>

                                                            <div class="el-select__content js-el-select-content">
                                                                <ul class="el-select__list" id="residence-country-select">
                                                                    <li class="el-select__item js-select-item" data-value="Все страны">Все страны</li>
                                                                    @foreach ($countries as $country)
                                                                        <li class="el-select__item js-select-item {{ old('country_of_residence_id') ?? $applicant->country_of_residence_id == $country->id ? 'el-select__item_active' : '' }}"
                                                                            data-value="{{ $country->name }}" data-id="{{ $country->id }}"
                                                                        >{{ $country->name }}</li>
                                                                    @endforeach
                                                                </ul>


                                                                <input type="hidden" name="country_of_residence_id" value="{{ old('country_of_residence_id') ?? $applicant->country_of_residence_id}}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Мой город:</span>
                                                        @if($errors->has('city_of_residence_id'))
                                                            <div class="validation-error-text">{{ $errors->first('city_of_residence_id') }}</div>
                                                        @endif
                                                        <div class="el-select el-select_filter js-select">

                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text selected-residence-city">Выберите город</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>

                                                            <div class="el-select__content js-el-select-content" id="city_of_residence_area"></div>
                                                            <input type="hidden" name="city_of_residence_id" value="{{ old('city_of_residence_id') ?? $applicant->city_of_residence_id}}" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="el-form el-form_w_flex">
                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Ваш родной язык:</span>
                                                        <div class="filter__row-item">
                                                            <ul class="filter__list filter-list js-filter-list">
                                                                <li class="filter-list__item js-fil-list filter-list__item_none">
                                                                    <ul>
                                                                        @foreach($languages as $language)
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="radio" name="native_language" class="form-checkbox" value="{{ $language->id }}"
                                                                                        {{ old('native_language') || $applicant->languageName == $language->id ? 'checked' : '' }} />
{{--                                                                                    <input type="radio" name="native_language" class="form-checkbox" value="{{ $language->id }}"--}}
{{--                                                                                        {{ old('native_language') ?? $applicant->languages['id'] == $language->id ? 'checked' : '' }} />--}}
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">{{ $language->name }}</span>
                                                                                </label>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                                @if($errors->has('native_language'))
                                                                    <li><div class="validation-error-text">{{ $errors->first('native_language') }}</div></li>
                                                                @endif
                                                                <li class="filter-list__item">
                                                                    <a href="" class="filter-list__all js-fil-list-all" data-text="Свернуть">
                                                                        <span>Открыть все</span>
                                                                        <i class="svg-icon">
                                                                            <svg class="svg-icon_ar-bot">
                                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                            </svg>
                                                                        </i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Какие еще языки вы знаете?</span>
                                                        <div class="filter__row-item">
                                                            <ul class="filter__list filter-list js-filter-list">
                                                                <li class="filter-list__item js-fil-list filter-list__item_none">
                                                                    <ul>
                                                                        @foreach($languages as $language)
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox" name="languages[]" class="form-checkbox" value="{{ $language->id }}"
                                                                                        {{ old('languages') && in_array($language->id, old('languages')) && $applicant->languageName ? 'checked' : '' }} />
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">{{ $language->name }}</span>
                                                                                </label>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                                @if($errors->has('languages'))
                                                                    <li><div class="validation-error-text">{{ $errors->first('languages') }}</div></li>
                                                                @endif
                                                                <li class="filter-list__item">
                                                                    <a href="" class="filter-list__all js-fil-list-all" data-text="Свернуть">
                                                                        <span>Открыть все</span>
                                                                        <i class="svg-icon">
                                                                            <svg class="svg-icon_ar-bot">
                                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                            </svg>
                                                                        </i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="wr-tops mt-3">
                                            <div class="title-form-h">Наличие документов
                                                @if($errors->has('documents'))
                                                    <div class="validation-error-text">{{ $errors->first('documents') }}</div>
                                                @endif
                                            </div>
                                            @if(count($documents) > 0)
                                                <div class="el-form-row el-form-row_three mt-0 pt-0">
                                                    @foreach($documents as $i => $document)
                                                        <div class="el-form el-form_w_flex">
                                                            <div class="el-form-row">
                                                                <div class="el-form">
                                                                    <div class="filter__row-item">
                                                                        <ul class="filter__list filter-list js-filter-list">
                                                                            @if(isset($document[$i]))
                                                                                @foreach($documents[$i] as $doc)
                                                                                    <li class="filter-list__item">
                                                                                        <label class="el-check">
                                                                                            <input type="radio" class="form-checkbox" name="documents[]" value="{{ $doc['id'] }}"
                                                                                                {{ old('documents')  && in_array($doc['id'], old('documents')) || $applicant->documentName ? 'checked' : '' }}>
{{--                                                                                            <input type="radio" class="form-checkbox" name="documents[]" value="{{ $doc['id'] }}"--}}
{{--                                                                                                {{ old('documents') ?? $applicant->documents->id && in_array($doc['id'], old('documents') ?? $applicant->documents->id) ? 'checked' : '' }}>--}}
                                                                                            <i class="shecked-icon"></i>
                                                                                            <span class="check-text">{{ $doc['name'] }}</span>
                                                                                        </label>
                                                                                    </li>
                                                                                @endforeach
                                                                            @endif
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="2">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="4">Далее</a>
                                        </div>
                                    </div>

                                    {{--                                <div class="page-forms__cont js-page-forms__six page-forms__six page-top-form" data-pag-form="5">--}}
                                    {{--                                    <div class="el-form-row">--}}
                                    {{--                                        <div class="page-forms-bot">--}}
                                    {{--                                            <div class="page-forms-bot__right">--}}
                                    {{--                                                <button type="submit" class="site-btn bnt-type-one bnt-type-one1 f_15 js-bnt-valid">Далее</button>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <div class="page-forms__btn">--}}
                                    {{--                                        <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="4">Назад</a>--}}
                                    {{--                                    </div>--}}
                                    {{--                                </div>--}}
                                </div>
                            </div>

                            <div class="pages__item pages-anket">
                                <div class="vacancy">
                                    <h1 class="title-style-tree">Моя анкета</h1>
                                    <div class="vacancy__info">Все поля обязательны к заполнению.
                                    </div>
                                </div>
                            </div>
                            <div class="pages__item response">
                                <div class="page-forms">
                                    <div class="page-forms__cont js-page-forms__five page-forms__five" data-pag-form="4">
                                        <div class="el-form-row"></div>
                                        <div class="el-form-row el-form-row_three pt-0 mt-0">
                                            <div class="el-form">
                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Ищу работу на должность:</span>
                                                        @if($errors->has('positions'))
                                                            <div class="validation-error-text">{{ $errors->first('positions') }}</div>
                                                        @endif
                                                        <div class="filter__row-item">
                                                            <ul class="filter__list filter-list js-filter-list">
                                                                @foreach ($positions as $position)
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
{{--                                                                            <input type="checkbox" name="positions[]" class="form-checkbox" value="{{ $position->id }}"--}}
{{--                                                                                {{ old('positions') ?? $applicant->positions->id && in_array($position->id, old('positions') ?? $applicant->positions->id) ? 'checked' : '' }} />--}}
                                                                            <input type="checkbox" name="positions[]" class="form-checkbox" value="{{ $position->id }}"
                                                                                {{ old('positions') && in_array($position->id, old('positions')) && $applicant->positions->id ? 'checked' : '' }} />
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">{{ $position->name }}</span>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="el-form el-form_w_flex">
                                                <div class="el-form-row el-form-row_two">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Желаемая зарплата:</span>
                                                        <div class="salary">
                                                            <div class="salary__price">
                                                                <input type="number" class="salary-in" name="salary" placeholder="Сумма" value="{{ old('salary') ?? $applicant->salary}}">
                                                                <span class="salary__currency">€</span>
                                                            </div>
                                                            <div class="salary__select">
                                                                <div class="el-select el-select_filter js-select">
                                                                    <div class="el-select__main js-select-main">
                                                                        <span class="el-select__text js-select-text selected-price-period">В час</span>
                                                                        <i class="svg-icon">
                                                                            <svg class="svg-icon_ar-bot">
                                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                            </svg>
                                                                        </i>
                                                                    </div>

                                                                    <div class="el-select__content js-el-select-content">
                                                                        <ul class="el-select__list" id="price-period-select">
                                                                            @foreach ($price_period as $price)
                                                                                <li class="el-select__item js-select-item {{ old('price_period_id') || $applicant->pricePeriod->id == $price->id ? 'el-select__item_active' : '' }}"
                                                                                    data-value="{{ $price->name }}"
                                                                                    data-id="{{ $price->id }}"
                                                                                >{{ $price->name }}
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                        <input type="hidden" name="price_period_id" value="{{ old('price_period_id') || ($price_period->first() ? $price_period->first()->id : '')}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($errors->has('salary'))
                                                            <div class="validation-error-text">{{ $errors->first('salary') }}</div>
                                                        @endif
                                                        @if($errors->has('price_period_id'))
                                                            <div class="validation-error-text">{{ $errors->first('price_period_id') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_two">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Рекомендации:</span>
                                                        <div class="el-select el-select_filter   js-select">
                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text selected-recommendations">Нет</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>

                                                            <div
                                                                class="el-select__content js-el-select-content">
                                                                <ul class="el-select__list" id="recommendations-select">
                                                                    <li class="el-select__item js-select-item {{ old('recommendations') || $applicant->recommendation == 0 ? 'el-select__item_active' : '' }}"
                                                                        data-id="0" data-value="Нет">
                                                                        Нет
                                                                    </li>
                                                                    <li class="el-select__item js-select-item {{ old('recommendations') || $applicant->recommendation == 1 ? 'el-select__item_active' : '' }}"
                                                                        data-id="1" data-value="Есть">
                                                                        Есть
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <input type="hidden" name="recommendations" value="{{ old('recommendations') || $applicant->recommendations}}">
                                                        </div>
                                                        @if($errors->has('recommendations'))
                                                            <div class="validation-error-text">{{ $errors->first('recommendations') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_two">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Водительские права:</span>
                                                        <div class="el-select el-select_filter js-select">
                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text selected-driver-license">Нет</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>
                                                            <div class="el-select__content js-el-select-content">
                                                                <ul class="el-select__list" id="driver-license-select">
                                                                    <li class="el-select__item js-select-item {{ old('driver_license') || $applicant->driver_license == 1 ? 'el-select__item_active' : '' }}"
                                                                        data-value="Есть права" data-id="1">Есть права</li>
                                                                    <li class="el-select__item js-select-item {{ old('driver_license') || $applicant->driver_license == 0 ? 'el-select__item_active' : '' }}"
                                                                        data-value="Нет" data-id="0">Нет</li>
                                                                </ul>
                                                            </div>
                                                            <input type="hidden" name="driver_license" value="{{old('driver_license') || $applicant->driver_license}}">
                                                        </div>
                                                        @if($errors->has('driver_license'))
                                                            <div class="validation-error-text">{{ $errors->first('driver_license') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="ancet-search">
                                                        <span class="text-form-item">Участие в поиске.</span>
                                                        <p class="ancet-search__text">Вы можете скрыть вашу анкету из поиска, например, если нашли работу</p>
                                                        <div class="slider-row-btn">
                                                            <span class="slider-row-btn__left pointer-event in_search_label">Скрыть анкету</span>
                                                            <label class="slider-btn">
                                                                <input type="checkbox" class="slider-btn__check" value="{{old('in_search') || $applicant->in_search}}" name="in_search" {{ old('in_search')?? $applicant->in_search ? 'checked' : '' }}>
                                                                <i class="slider-btn__icon"></i>
                                                            </label>
                                                            <span class="slider-row-btn__right pointer-event in_search_label">Опубликовать анкету</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{--                                        <div class="page-forms__btn">--}}
                                        {{--                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="3">Назад</a>--}}
                                        {{--                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="5">Далее</a>--}}
                                        {{--                                        </div>--}}
                                    </div>

                                    <div class="page-forms__cont js-page-forms__six page-forms__six page-top-form" data-pag-form="5">
                                        <div class="el-form-row">
                                            <div class="el-form page-down-form">
                                                <span class="text-form-item">Хочу работать:</span>
                                                @if($errors->has('accommodations'))
                                                    <div class="validation-error-text">{{ $errors->first('accommodations') }}</div>
                                                @endif
                                                <div class="filter__row-item">
                                                    <ul class="filter__list filter-list js-filter-list">
                                                        @foreach ($accommodations as $ac)
                                                            <li class="filter-list__item">
                                                                <label class="el-check">
                                                                    <input type="radio" name="accommodations[]" value="{{ $ac->id }}" class="form-checkbox"
                                                                        {{ old('accommodations') && in_array($ac->id, old('accommodations')) || $applicant->accommodationNames ? 'checked' : '' }} />
                                                                    <i class="shecked-icon"></i>
                                                                    <span class="check-text">{{ $ac->name }}</span>
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="el-form-row">
                                            <div class="el-form">
                                                <span class="text-form-item">Расскажите о себе <span>(минимум 40 символов):
                                                   @if($errors->has('comments'))
                                                            <div class="validation-error-text">{{ $errors->first('comments') }}</div>
                                                        @endif
                                                    </span></span>
                                                <textarea class="type-textarea js-textarea" name="comments">{{ old('comments') ?? $applicant->comments}} </textarea>

                                            </div>
                                        </div>
                                        <div class="el-form-row">
                                            <div class="page-forms-bot mb-2">
                                                <div class="page-forms-bot__left">
                                                    @if($errors->has('agree_agreement'))
                                                        <div class="validation-error-text">{{ $errors->first('agree_agreement') }}</div>
                                                    @endif
                                                    <label class="el-check js-el-form">
                                                        <input type="checkbox" class="form-checkbox js-check" name="agree_agreement"  value="{{ old('agree_agreement') ?? $applicant->agree_agreement }}"
                                                            />
                                                        <i class="shecked-icon"></i>
                                                        <span class="check-text">Я согласен с условиями оказания
                                                        услуг и с политикой конфиденциальности в отношении обработки персональных данных </span>
                                                    </label>
                                                </div>
                                            </div>
                                            {{--                                                <div class="page-forms-bot mb-2">--}}
                                            {{--                                                    <div class="form-group">--}}
                                            {{--                                                        {!! NoCaptcha::renderJs() !!}--}}
                                            {{--                                                        {!! NoCaptcha::display() !!}--}}
                                            {{--                                                        @if($errors->has('g-recaptcha-response'))--}}
                                            {{--                                                            <div class="validation-error-text"> {{ $errors->first('g-recaptcha-response') }} </div>--}}
                                            {{--                                                        @enderror--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                </div>--}}
                                            <div class="page-forms-bot__right">
                                                <button type="submit" class="site-btn bnt-type-one bnt-type-one1 f_15 js-bnt-valid">Редактировать</button>
                                            </div>
                                        </div>
                                        {{--                                        <div class="page-forms__btn">--}}
                                        {{--                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="4">Назад</a>--}}
                                        {{--                                        </div>--}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="section-con__aside">
                    <div class="aside-content">
                        @include('questionnaires.aside')
{{--                        <div class="title-style-six">Для новичков</div>--}}
{{--                        <p>Услуги портала бесплатные. Анкета размещается бесплатно.</p>--}}
{{--                        <p>Уделите время анкете: разместите фотографию, расскажите о себе подробно и достоверно заполните профайл.   </p>--}}
{{--                        <p>Если вы найдете работу, то просто поменяйте 'Мой статус' на 'убрать из поиска' и ваша анкета не будет публиковаться на сайте.  </p>--}}
{{--                        <p>В разделе 'Ищу работу в', укажите наиболее предпочтительное место для работы. Вас найдут по всему региону. </p>--}}
{{--                        <p>В личном кабинете вы сможете ознакомится с платными услугами. </p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit"></script>
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        $('.in_search_label').on('click', function (){
            $('input[name=in_search]').trigger('click');
        });

        function recaptchaCallback() {
            $('button[type=submit]').removeAttr('disabled');
        }

        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            utilsScript: "js/utils.js",
        });

        var input1 = document.querySelector("#phone1");
        window.intlTelInput(input1, {
            utilsScript: "js/utils.js",
        });

        var input2 = document.querySelector("#phone2");
        window.intlTelInput(input2, {
            utilsScript: "js/utils.js",
        });


        /* working country select value change event */
        $('#country-select li').on('click', function (){
            $('input[name=country_of_work_id]').val($(this).data('id'));
            $('input[name=city_of_work_id]').val('');
            $('.selected-work-city').text('Любой город').attr('data-value', '');
        });

        let workCountryID = $('input[name=country_of_work_id]').val();
        if (workCountryID) {
            let workCountryName = $(`.education-option[data-id=${workCountryID}]`).data('value');
            $('#selected-work-country').attr('data-value', workCountryName).text(workCountryName)
            $(`.countries[data-id=${workCountryID}]`).click();
        }

        $('#cities-select-area').on('click', function (){
            $('input[name=city_of_work_id]').val($(this).data('id'));
        });


        /* education value change event */
        $('.education-option').on('click', function (){
            $('input[name=education_id]').val($(this).data('id'));
        });

        let educationID = $('input[name=education_id]').val();
        if (educationID) {
            let educationName = $(`.education-option[data-id=${educationID}]`).data('value');
            $('.selected-education').attr('data-value', educationName).text(educationName)
            $(`.education-option[data-id=${educationID}]`).click();
        }

        $('#residence-country-select li').on('click', function (){
            let countryId = $(this).data('id');
            $.ajax({
                url: `/world-cities?countryId=${countryId}`,
                method: 'GET',
                success: response => {
                    $('#city_of_residence_area').html(response);
                }
            });
            $('input[name=country_of_residence_id]').val(countryId);
            $('input[name=city_of_residence_id]').val('');
            $('.selected-residence-city').text('Выберите город').attr('data-value', '');
        });

        $('#residence-city-select-area').on('click', function (){
            $('input[name=city_of_residence_id]').val($(this).data('id'));
        });

        /* Price period valu chnage event */
        $('#price-period-select li').on('click', function (){
            $('input[name=price_period_id]').val($(this).data('id'));
        });

        let pricePeriodID = $('input[name=price_period_id]').val();
        let pricePeriodName = $(`#price-period-select li[data-id=${pricePeriodID}]`).data('value');
        $('.selected-price-period').attr('data-value', pricePeriodName).text(pricePeriodName)
        $(`#price-period-select li[data-id=${pricePeriodID}]`).click();

        /* Driver license value change event */
        $('#driver-license-select li').on('click', function (){
            $('input[name=driver_license]').val($(this).data('id'));
        });

        let driverLicenseID = $('input[name=driver_license]').val();
        let driverLicenseName = $(`#driver-license-select li[data-id=${driverLicenseID}]`).data('value');
        $('.selected-driver-license').attr('data-value', driverLicenseName).text(driverLicenseName)
        $(`#driver-license-select li[data-id=${driverLicenseID}]`).click();


        /* recommendations value change event */
        $('#recommendations-select li').on('click', function (){
            $('input[name=recommendations]').val($(this).data('id'));

        });

        let recommendationsID = $('input[name=recommendations]').val();

        let recommendationsName = $(`#recommendations-select li[data-id=${recommendationsID}]`).data('value');
        $('.selected-recommendations').attr('data-value', recommendationsName).text(recommendationsName);
        $(`#recommendations-select li[data-id=${recommendationsID}]`).click();

    </script>
@endsection
