@extends('layouts.app')

@section('title') Моя анкета @endsection

@section('main_content')
    <div class="section-con section-con__top-pad section-con_mob-pad  section-con_no-cont">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages vacancy-mob-pad">
                        <div class="pages__item pages__item_mob bgwhite">
                            <div class="vacancy">
                                <h1 class="title-style-five">{{ $user->spec }}</h1>
                                <div class="vacancy__info">{{ $user->position ?  $user->position->name : ''}}</div>
                                <div class="el-info vacancy_el_info">
                                    <div class="el-info__item">
                                        <i class="svg-icon">
                                            <svg>
                                                <use xlink:href="img/svg/sprite.svg#icon-money"></use>
                                            </svg>
                                        </i>
                                        <!-- t_strong t_red-->
                                        <span class="el-info__text t_strong t_red">{{ $user->oplata }} {{$user->pricePeriod ? $user->pricePeriod->name : ''}}</span>
                                    </div>
                                    <div class="el-info__item">
                                        <i class="svg-icon">
                                            <svg>
                                                <use xlink:href="img/svg/sprite.svg#label-two"></use>
                                            </svg>
                                        </i>
                                        <span class="el-info__text">{{$user->country ? $user->country->name : ''}}, {{$user->city ? $user->city->name : ''}}</span>
                                    </div>
                                    <div class="el-info__item">
                                        <i class="svg-icon">
                                            <svg>
                                                <use xlink:href="img/svg/sprite.svg#icon-home"></use>
                                            </svg>
                                        </i>
                                        <span class="el-info__text no-life">{{ $user->accommodation ?  $user->accommodation->name : ''}}</span>
                                    </div>
                                </div>
                                <div class="el-hr el-mob"></div>
                                <div class="vacancy__item">
                                    <ul class="vacancy__list-item vacancy-list">
                                        <li>
                                            <span class="vacancy-list__left">Документы:</span>
                                            <span class="vacancy-list__right">{{ $user->document ?  $user->document->name : ''}}</span>
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Рекомендации:</span>
                                            <span class="vacancy-list__right">Обязательны рекомендации</span>
                                        </li>
                                        <li>
                                            <span class="vacancy-list__left">Район города: </span>
                                            <span class="vacancy-list__right">Amsterdam Oud Zuid</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="el-hr"></div>
                            <div class="section-text section-text_mar">
                                <h3>Подробнее о профиле</h3>
                                <p>Ищем ответственного человека, умеющего ухаживать за ребенком 6 месяцев.
                                    Временная работа на полный рабочий день 4 раза в неделю, начиная с 12 октября и до конца января. Территориально: октябрь и до середины ноября - Амстердам юг, после середины ноября - Пурмеренд центр(транспорт оплачивается).
                                    Требования: документы(residence permit/паспорт), рекомендации с предыдущих мест работ/опыт общения с детьми, внимательность и ответственность, выполнение любых требований родителей, касающихся воспитания и ухода за ребенком, отсутствие вредных привычек, умение оказать первую мед.помощь, добродушность и порядочность, будет плюсом - грамотная русская речь и наличие любого мед.образования.</p>
                                <h3>Контакты работодателя</h3>
                                <p>Контакты работодателя Ксения скрыты, для просмотра контактов <a href="#">оплатите тариф</a> чтобы связаться
                                    с работодателем на прямую или бесплатно оставьте свой отклик в форме ниже.</p>
                            </div>
                            <div class="article-info article-info_mar">
                                <div class="article-info__item">
                                    <i class="svg-icon">
                                        <svg class="svg-icon_id">
                                            <use xlink:href="img/svg/sprite.svg#icon-id"></use>
                                        </svg>
                                    </i>
                                    <span>{{$user->id}}</span>
                                </div>
                                <div class="article-info__item">
                                    <i class="svg-icon">
                                        <svg class="svg-icon_eye">
                                            <use xlink:href="img/svg/sprite.svg#icon-eye"></use>
                                        </svg>
                                    </i>
                                    <span>390</span>
                                </div>
                                <div class="article-info__item">
                                    <i class="svg-icon">
                                        <svg class="svg-icon_pen">
                                            <use xlink:href="img/svg/sprite.svg#icon-pen"></use>
                                        </svg>
                                    </i>
{{--                                    <span>{{ $user->from_date }}</span>--}}
                                </div>
                            </div>
                        </div>
                        <div class="pages__item pages__item_mob response">
                            <h3 class="title-style-four reviews-title">Отклики на вакансию</h3>
                            <div class="response__text"><p>Размещение контактных данных строго запрещено.</p>
                                <p>Анкеты с размещенными контактными данными будут удалены.</p></div>
                            <div class="response__form">
                                <form action="#">
                                    <div class="el-form-row">
                                        <span class="text-form-item">Ваш отклик:</span>
                                        <textarea class="type-textarea"></textarea>
                                    </div>
                                    <div class="form-row-btn form-row-btn_end">
                                        <button class="site-btn bnt-type-one f_15">Отправить отклик</button>
                                    </div>
                                </form>
                            </div>
                            <div class="el-hr el-other"></div>
                            <div class="hail">
                                <div class="hail__item item-hail">
                                    <div class="item-hail__img">
                                        <img src="img/hail-1.png" alt="img">
                                    </div>
                                    <div class="item-hail__content">
                                        <div class="item-hail__top">
                                            <div class="item-hail__name">Анжела, 52</div>
                                            <div class="item-hail__date">15.09.2020</div>
                                        </div>
                                        <div class="item-hail__text">
                                            <p>С удовольствием поработаю у вас. Умею готовить, так же могу составить компанию</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="hail__item item-hail">
                                    <div class="item-hail__img">
                                        <img src="img/hail-2.png" alt="img">
                                    </div>
                                    <div class="item-hail__content">
                                        <div class="item-hail__top">
                                            <div class="item-hail__name">Екатерина, 34</div>
                                            <div class="item-hail__date">14.09.2020</div>
                                        </div>
                                        <div class="item-hail__text">
                                            <p>Как я рада вновь поработать с одногодками, есть большой опыт в итальянской семье, рекомендации могу предоставить в личной переписке.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-con__aside">
                    @include('questionnaires.aside')
                    <div class="aside-baner">
                        <h3 class="aside-baner__title">Увеличьте ваши
                            шансы найти
                            работу!</h3>
                        <div class="aside-baner__text aside-baner__text_mar">
                            <p>Выведите вашу анкету в ТОП. Ваши контактные данные будут открыты для всех
                                потенциальных работодателей</p>
                        </div>
                        <div class="aside-baner__btn">
                            <a href="{{ route('tariffs') }}" class="site-btn btn-site-two">Посмотреть тарифы</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>




@endsection
