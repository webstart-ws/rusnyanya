@if ($paginator->hasPages())
    <div class="pagination">

        {{-- Previous Page Link --}}
        <a href="{{ $paginator->previousPageUrl() }}" class="pagination__btn pagination__btn_prev @if($paginator->onFirstPage()) no-pointer @endif">
            <i class="svg-icon">
                <svg>
                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bnt-prev"></use>
                </svg>
            </i>
            @if (!$paginator->onFirstPage())
                <span>@lang('pagination.previous')</span>
            @endif
        </a>

        <ul class="pagination__items">
        {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="pagination__item disabled">
                        <a href="#" class="no-pointer ">{{ $element }}</a>
                    </li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        <li class="pagination__item @if ($page == $paginator->currentPage()) pagination__item_active @endif">
                            <a href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endforeach
                @endif
            @endforeach
        </ul>

        {{-- Next Page Link --}}
        <a href="{{ $paginator->nextPageUrl() }}" class="pagination__btn pagination__btn_next @if(!$paginator->hasMorePages()) no-pointer @endif">
            @if ($paginator->hasMorePages())
                <span>@lang('pagination.next')</span>
            @endif
            <i class="svg-icon">
                <svg>
                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bnt-next"></use>
                </svg>
            </i>
        </a>
    </div>
@endif
