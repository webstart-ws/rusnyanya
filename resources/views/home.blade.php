@extends('layouts.app')

@section('title') главная @endsection

@section('main_content')
    <div class="main-baner">
        <div class="container">
            <div class="main-baner__content">
                <h1 class="main-title main-baner__title">Русскоязычный домашний персонал в Европе</h1>
                <p class="type-text-1 main-baner__text">Предоставляем персональный и самостоятельный
                    подбор кандидатов по базе данных RUSnyanya</p>
                <div class="wr-two-btn main-baner__wr-btn">
                    <a href="{{ route('questionnaires.list') }}" class="site-btn bnt-type-one bnt-type-one3"><span>Найти персонал</span></a>
                    <a href="{{ route('vacancies.list') }}" class="site-btn bnt-type-one bnt-type-one3"><span>Найти работу</span></a>
                </div>
            </div>
        </div>
        <div class="main-baner__img" style="background-image: url(img/maing-bg.png);"></div>
    </div>
    <div class="section-con section-con_main">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="el-filetr">
                        <div class="el-filetr__item">
                            <!-- el-filetr__link_active -->
                            <a href="#" class="el-filetr__link el-filetr__link_active">Новые вакансии</a>
                            <a style="display: none" href="#" class="el-filetr__link el-filetr__link_mob">Новые анкеты</a>
                        </div>

                        <div class="el-filetr__item el-filetr__item_mob">

                            <div style="display: none" class="el-select el-select_city js-select">
                                <div class="el-select__main js-select-main">
                                    <i class="svg-icon">
                                        <svg class="svg-icon_label">
                                            <use xlink:href="img/svg/sprite.svg#label"></use>
                                        </svg>
                                    </i>
                                    <span class="el-select__text js-select-text">Все страны</span>
                                </div>
                                <div class="el-select__content js-el-select-content">
                                    <ul class="el-select__list">
                                        <li class="el-select__item js-select-item" data-value="Все страны">Все
                                            страны</li>
                                        <li class="el-select__item el-select__item_active js-select-item"
                                            data-value="Украина">Украина</li>
                                        <li class="el-select__item js-select-item" data-value="Россия">Россия
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div style="display: none" class="el-select el-select_servise js-select">
                                <div class="el-select__main js-select-main">
                                    <i class="svg-icon">
                                        <svg class="svg-icon_arrows">
                                            <use xlink:href="img/svg/sprite.svg#arrowTopBot"></use>
                                        </svg>
                                    </i>
                                    <span class="el-select__text js-select-text">Все специальности</span>
                                </div>
                                <div class="el-select__content js-el-select-content">
                                    <ul class="el-select__list">
                                        <li class="el-select__item js-select-item" data-value="Няня">Няня</li>
                                        <li class="el-select__item el-select__item_active js-select-item"
                                            data-value="Все специальности">Все специальности</li>
                                        <li class="el-select__item js-select-item" data-value="Сиделка">
                                            <a href="#">Сиделка</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Репетитор">
                                            <a href="#">Репетитор</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Домработница">
                                            <a href="#"> Домработница</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Повар">
                                            <a href="#">Повар</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Семейная пара">
                                            <a href="#">Семейная пара</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Онлайн услуги">
                                            <a href="#">Онлайн услуги</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Онлайн услуги">
                                            <a href="#">Водитель</a>
                                        </li>
                                        <li class="el-select__item js-select-item" data-value="Онлайн услуги">
                                            <a href="#">Батлер</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>

                        <div class="el-filetr__item el-filetr__item_fil">
                            <!-- js-bnt-filter -->
                            <a href="#" class="btn-filter ">
                                <i class="svg-icon">
                                    <svg>
                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-filter')}}"></use>
                                    </svg>
                                </i>
                            </a>
                        </div>
                    </div>
                    <div class="ankets">

                        @foreach($vacancies as $key => $vacancy)

                        <div class="anket-item">
                            <a href="{{ route('vacancy.show', $vacancy->id) }}" class="anket-item__title">{{ $vacancy->title }}</a>
                            <div class="anket-item__row">
                                <div class="el-info">
                                    <div class="el-info__item">
                                        <i class="svg-icon">
                                            <svg>
                                                <use xlink:href="{{asset('img/svg/sprite.svg#label-two')}}"></use>
                                            </svg>
                                        </i>
{{--                                        <span class="el-info__text"> {{$vacancy->country ? $vacancy->country->name : ''}}, {{$vacancy->city ? $vacancy->city->name : ''}}</span>--}}
                                        <span class="el-info__text"> {{ $vacancy->country_name }}, {{ $vacancy->city_name }}</span>

                                    </div>
                                    <div class="el-info__item">
                                        <i class="svg-icon">
                                            <svg>
                                                <use xlink:href="{{asset('img/svg/sprite.svg#icon-money')}}"></use>
                                            </svg>
                                        </i>
                                        <span class="el-info__text">{{$vacancy['salary']}} {{$vacancy->price_period_id == "0" ? 'в час' : ' в месяц'}}</span>
                                    </div>
                                    <div class="el-info__item">
                                        <i class="svg-icon">
                                            <svg>
                                                <use xlink:href="{{asset('img/svg/sprite.svg#icon-home')}}"></use>
                                            </svg>
                                        </i>
                                        <span class="el-info__text">{{ $vacancy->accommodation_names }}</span>
                                    </div>
                                </div>
                            </div>
                            <p class="anket-item__text">{{ $vacancy->comments }}</p>
                        </div>
                        @endforeach
                    </div>
                    <div class="wr-all">
                        <a href="{{asset('vacancies')}}" class="all-btn-text">
                            <i class="svg-icon">
                                <svg class="svg-icon__burger">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#icon-burger')}}"></use>
                                </svg>
                            </i>
                            <span class="search-all">Посмотреть все вакансии</span>
                        </a>
                    </div>
                </div>
                @guest
                    <div class="section-con__aside">
                        <div class="aside-baner">
                            <div class="aside-baner__img">
                                <img src="{{asset('img/img-baner.png')}}" alt="img">
                            </div>
                            <div class="aside-baner__text aside-baner__text_mar">
                                <p>Бесплатно регистрируйтесь и разместите вашу анкету или вакансию. Получайте прямые
                                    предложения по всей Европе</p>
                            </div>
                            <div class="aside-baner__btn">
                                <a href="{{ route('register', ['type' => 'vacancies', 'auth_required' => true]) }}" class="site-btn btn-site-two">Добавить вакансию</a>
                                <a href="{{ route('register', ['type' => 'questionnaires']) }}" class="site-btn btn-site-two">Добавить анкету</a>
                            </div>
                        </div>
                    </div>
                @endauth
            </div>
        </div>
    </div>

    <div class="se-reviews  se-reviews_bg">
        <div class="container">
            <div class="se-reviews__row js-row-js">
                <h2 class="section-title">О нас говорят</h2>
                <div class="reviews js-mob-slick reviews_margin">
                    <div class="reviews__item">
                        <div class="reviews__text">
                            <img src="{{ asset('img/reviews-img.png') }}" class="reviews__img" alt="img">
                            Через этот сайт я <br> нашла  в 2017 работу в Лондоне в отличной семье,
                            очень привязалась к ним, может быть поэтому сейчас трудно найти других, но я надеюсь
                        </div>
                        <div class="reviews__autor">
                            <span>Valentina Violetta Pascal Burlaca</span>
                        </div>
                    </div>
                    <div class="reviews__item">
                        <div class="reviews__text">
                            <img src="{{ asset('img/reviews-img-2.png') }}" class="reviews__img" alt="img">
                            Я считаю, что такая помощь, и в таком крайне важном вопросе не просто нужна а очень! Спасибо.
                        </div>
                        <div class="reviews__autor">
                            <span>Сидоренко Ирина</span>
                        </div>
                    </div>
                    <div class="reviews__item">
                        <div class="reviews__text">
                            <img src="{{ asset('img/reviews-img-3.png') }}" class="reviews__img" alt="img">
                            Прекрасная страница как для тех, кто ищет няню и других помощников в доме, так и для тех,
                            кто ищет такую работу. Спасибо за инфо.
                        </div>
                        <div class="reviews__autor">
                            <span>Инна Динская</span>
                        </div>
                    </div>
                </div>
                <div class="slider-dots js-slider-dots"></div>
                <div class="wr-all reviews-all">
                    <a href="https://www.facebook.com/rusnyanya/reviews/?ref=page_internal" target="_blank" class="all-btn-text">
                        <i class="svg-icon">
                            <svg class="svg-icon__sms">
                                <use xlink:href="{{asset('img/svg/sprite.svg#icon-sms')}}"></use>
                            </svg>
                        </i>
                        <span>Все отзывы о нас</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="se-worker">
        <div class="container js-row-js">

            <div class="worker js-slider-worker">
                @foreach($questionnaires as $questionnaire)
                    <div class="worker__item worker-item">
                    <div class="worker-item__img">
                        <a href="{{ route('questionnaires.show', $questionnaire->id ) }}" target="_blank">
                            <img src="{{ asset($questionnaire->avatar) }}" alt="">
                        </a>
                    </div>
                    <div class="worker-item__content">
                        <a href="{{ route('questionnaires.show', $questionnaire->id ) }}" target="_blank" class="worker-item__title">{{ $questionnaire->author->name }}, {{$questionnaire->age}}</a>
                        <div class="worker-item__info">{{ $questionnaire->speciality }}</div>
                        <div class="worker-item__el">
                            <i class="svg-icon">
                                <svg class="svg-icon_label">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#label-two')}}"></use>
                                </svg>
                            </i>
                            <span>{{ $questionnaire->country_name }}</span>
                        </div>
                        <div class="worker-item__el">
                            <i class="svg-icon">
                                <svg class="svg-icon_label">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#icon-money')}}"></use>
                                </svg>
                            </i>
                            <span>{{ $questionnaire->salary }} {{ $questionnaire->price_period_id == "0" ? 'в час' : ' в месяц' }}</span>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="slider-dots slider-dots_mod js-slider-dots"></div>
            <div class="wr-all anket-all">
                <!-- одно из двух -->
                <!-- <a href="#" class="all-btn-text">
                    <i class="svg-icon">
                        <svg class="svg-icon__burger">
                            <use xlink:href="img/svg/sprite.svg#icon-burger"></use>
                        </svg>
                    </i>
                    <span class="search-all">Посмотреть все вакансии</span>
                </a> -->

                <a href="{{asset('questionnaires')}}" class="all-btn-text">
                    <i class="svg-icon">
                        <svg class="svg-icon__sms">
                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-sms')}}"></use>
                        </svg>
                    </i>
                    <span>Все анкеты работников</span>
                </a>
            </div>
        </div>
    </div>

    <div class="body__opas js-boyd--opas"></div>

@endsection
