
<!-- попап -->
<div class="popup popup_login js-popup" data-id-popup="popup-0">
    <div class="popup__cont">
        <div class="popup__wrapp popup__wrapp_registr  js-popup__wrapp">

            <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="{{asset('../img/svg/sprite.svg#icon-close')}}"></use>
                    </svg>
                </span>

                <div class="autorization">
                    <div class="title-style-four popup__title">Авторизуйтесь</div>
                    <div class="popup__text">
                        <p>Если уже зарегистрированы</p>
                    </div>
                    <div class="popup__form">
                        <form action="{{ route('login') }}" method="post" class="js-form-validation">
                            @csrf
                            <div class="el-form-row">
                                <div class="el-form js-el-form">
                                    <span class="text-form-item">Логин или email:</span>
                                    <input type="text" name="email" data-text-valid="This is a valid field" id="email" class="type-input">
                                </div>
                            </div>
                            <div class="el-form-row">
                                <div class="el-form js-el-form">
                                    <span class="text-form-item">Пароль:</span>
                                    <input type="password" name="password" id="password" class="type-input">
                                </div>
                            </div>
                            <div class="form-row-btn form-row-btn_f">
                                <label class="el-check">
                                    <input type="checkbox" class="form-checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} />
                                    <i class="shecked-icon"></i>
                                    <span class="check-text">Запомнить меня</span>
                                </label>
                                <button type="submit" class="site-btn bnt-type-one bnt-type-one2 f_15 js-bnt-valid login">Войти</button>
                            </div>
                            <div class="form-row-btn form-row-btn_end">
                                <a href="popup-3" class="type-link-dot js-btn-popup">Забыли пароль?</a>
                            </div>

                            <p style="text-align: center; margin-top: 10px;">или войти через</p>
                            <div class="d-flex justify-center items-center">
                                <a href="{{ route('social.auth', 'facebook') }}" class="auth-driver" title="Facebook">
                                    <img src="{{ asset('img/svg/facebook-brands.svg') }}">
                                </a>
                                <a href="{{ route('social.auth', 'google') }}" class="auth-driver" title="Google">
                                    <img src="{{ asset('img/svg/google-brands.svg') }}">
                                </a>
                                <a href="{{ route('social.auth', 'vkontakte') }}" class="auth-driver" title="VKontakte">
                                    <img src="{{ asset('img/svg/vk-brands.svg') }}">
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="applicants">
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Нет аккаунта?</div>

                        <div class="popup__btn-row">
                            <a href="popup-0-sois" class="btn-site js-btn-popup btn-site-six mind-auto">Зарегистрироваться</a>
                        </div>
                    </div>

                    <div class="applicants__item">
                        <div class="title-style-four popup__title">Соискателям</div>
                        <div class="popup__text">
                            <p>Если вы хотите найти работу
                                и создать анкету на нашем сайте</p>
                        </div>
                        <div class="popup__btn-row">
                            <a href="{{ route('register', ['type' => \App\Models\User::ROLE_APPLICANT_VIEW]) }}"
                               class="btn-site btn-site-six">Создать анкету</a>
                        </div>
                    </div>
                    <div class="applicants__item">
                        <div class="title-style-four popup__title">Работодателям</div>
                        <div class="popup__text">
                            <p>Вы можете совершенно бесплатно
                                добавить вакансию о поиске работника.</p>
                        </div>
                        <div class="popup__btn-row">
                            <a href="{{ route('register', ['type' => \App\Models\User::ROLE_VACANCY_VIEW]) }}"
                               class="btn-site btn-site-six">Добавить вакансию</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<!-- не знаю пр какой кнопке будет вызываться http://joxi.ru/brR1oq4COMN7LA -->
<div class="popup js-popup" data-id-popup="popup-0-sois">
    <div class="popup__cont">
        <div class="popup__wrapp popup__wrapp_registr  js-popup__wrapp">

            <div class="popup__row">
                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="{{asset('../img/svg/sprite.svg#icon-close')}}"></use>
                    </svg>
                </span>

                <div class="autorization">
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Соискателям</div>
                        <div class="popup__text">
                            <p>Если вы хотите найти работу и создать анкету на нашем сайте</p>
                        </div>
                        <div class="popup__btn-row">
                            <a href="{{ route('register', ['type' => \App\Models\User::ROLE_APPLICANT_VIEW]) }}"
                               class="btn-site btn-site-six">Создать анкету</a>
                        </div>
                    </div>
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Работодателям</div>
                        <div class="popup__text">
                            <p>Вы можете совершенно бесплатно добавить вакансию о поиске работника.</p>
                        </div>
                        <div class="popup__btn-row">
                            <a href="{{ route('register', ['type' => \App\Models\User::ROLE_VACANCY_VIEW]) }}"
                               class="btn-site btn-site-six">Добавить вакансию</a>
                        </div>
                    </div>

                </div>
                <div class="applicants">
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Уже зарегистрированы?</div>
                        <div class="popup__btn-row">
                            <a href="popup-0" class="btn-site js-btn-popup bnt-type-one">Войти в кабинет</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


{{-- show tariffs popup --}}
<div class="popup js-popup" data-id-popup="popup-1">
    <div class="popup__cont">
        <div class="popup__wrapp popup__wrapp_stand  js-popup__wrapp">
            <div class="popup__row">
                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="{{asset('../img/svg/sprite.svg#icon-close')}}"></use>
                    </svg>
                </span>

                <div class="popup__content">
                    <div class="title-style-four popup__title text-center">Показ контактных данных</div>
                    <div class="popup__text text-center">
                        <p>У вас есть возможность посмотреть еще 6 контактных данных.
                            Вы хотите воспользоваться этим?</p>
                    </div>
                    <div class="popup__btn_two">
                        <a href="#" class="site-btn bnt-type-one f_15 btn-site-7">Отмена</a>
                        <a href="{{ route('tariffs') }}" class="site-btn bnt-type-one f_15">Показать контакты</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- tariffs popup --}}
<div class="popup popup-payment js-popup" data-id-popup="popup-2">
    <div class="popup__cont no-center">
        <div class="popup__wrapp popup__wrapp_payment  js-popup__wrapp">

            <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="{{asset('../img/svg/sprite.svg#icon-close')}}"></use>
                    </svg>
                </span>

                <div class="popup-payment__left">
                    <div class="payment__prise">
                        <div class="payment__prise-text">К оплате</div>
                        <div class="payment__prise-price"><span>EUR</span> 25.00</div>
                    </div>
                    <div class="payment__left-bot">
                        <a href="#" class="btn-type-9">
                            <svg>
                                <use xlink:href="{{asset('../img/svg/sprite.svg#icon-ar-left')}}"></use>
                            </svg>
                            <span>Отменить оплату</span>
                        </a>
                    </div>
                </div>

                <div class="popup-payment__right">
                    <div class="title-style-four popup__title">Оплата тарифа </div>
                    <div class="popup-payment__row">
                        <div class="popup__text">
                            <p>Оплатите с помощью карты</p>
                        </div>
                        <div class="popup-payment__cart">
                            <img src="{{asset('img/visa.png')}}" alt="img">
                            <img src="{{asset('img/mastercart.png')}}" alt="img">
                        </div>
                    </div>
                    <div class="popup__form">
                        <form action="#">
                            <div class="el-form-row">
                                <div class="el-form">
                                    <span class="text-form-item">Номер кредитной карты:</span>
                                    <input type="text" name="card" data-text-valid="This is a valid field"
                                           class="type-input" placeholder="4584 - ">
                                </div>
                            </div>
                            <span class="text-form-item">Срок действия:</span>
                            <div class="el-form-row ">
                                <div class="cols-row">
                                    <div class="el-form">
                                        <div class="el-select el-select_filter mtpc0 js-select">

                                            <div class="el-select__main js-select-main">
                                                <span class="el-select__text js-select-text">Все страны</span>
                                                <i class="svg-icon">
                                                    <svg class="svg-icon_ar-bot">
                                                        <use xlink:href="{{asset('img/svg/sprite.svg#icon-arrow-bot')}}"></use>
                                                    </svg>
                                                </i>
                                            </div>

                                            <div class="el-select__content wrap-select-tarifs js-el-select-content">
                                                <ul class="el-select__list el-select__tarifs">
                                                    <li class="el-select__item js-select-item el-select__item_active"
                                                        data-value="Все страны">Все страны</li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="el-form">
                                        <div class="el-select el-select_filter mtpc0 js-select">

                                            <div class="el-select__main js-select-main">
                                                <span class="el-select__text js-select-text">Все страны</span>
                                                <i class="svg-icon">
                                                    <svg class="svg-icon_ar-bot">
                                                        <use xlink:href="{{asset("img/svg/sprite.svg#icon-arrow-bot")}}"></use>
                                                    </svg>
                                                </i>
                                            </div>

                                            <div class="el-select__content wrap-select-tarifs js-el-select-content">
                                                <ul class="el-select__list el-select__tarifs">
                                                    <li class="el-select__item js-select-item el-select__item_active"
                                                        data-value="Все страны">Все страны</li>

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="el-form-row">
                                <div class="cols-row">
                                    <div class="el-form">
                                        <span class="text-form-item">CVC/CW
                                        </span>
                                        <input type="text" name="card" data-text-valid="This is a valid field"
                                               class="type-input" placeholder="341">
                                    </div>
                                    <div class="el-form">
                                        <span class="info-text-dop">Код из 3 или 4 цифр</span>
                                    </div>
                                </div>
                            </div>

                            <div class="el-form-row">
                                <label class="el-check el-check-top">
                                    <input type="checkbox" class="form-checkbox" checked>
                                    <i class="shecked-icon"></i>
                                    <span class="check-text">Соглашаюсь с условиями оказания услуг</span>
                                </label>
                            </div>

                            <div class="form-row-btn form-row-btn_mar">
                                <button type="submit" class="site-btn bnt-type-one bnt-type-one2 f_15 ">Перейти к оплате <span>EUR
                                        25.00</span></button>
                            </div>

                            <div class="popup__dop-b">
                                <div class="popup__text text-center">
                                    <p>Или выберите другой способ оплаты</p>
                                </div>
                                <div class="el-form-row">
                                    <a href="{{ route('make.payment') }}" class="btn-pay">
                                        <span>Оплатить через</span>
                                        <img src="{{asset('img/img-pay2.png')}}" alt="img">
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

{{-- forgot password popup --}}
<div class="popup js-popup" data-id-popup="popup-3">
    <div class="popup__cont">
        <div class="popup__wrapp popup__wrapp_stand  js-popup__wrapp">

            <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="{{asset('../img/svg/sprite.svg#icon-close')}}"></use>
                    </svg>
                </span>

                <div class="autorization popup__forget">
                    <div class="title-style-four popup__title">Восстановление пароля</div>
                    <div class="popup__form">
                        <form action="{{ route('password.email') }}" method="POST" class="js-form-validation">
                            @csrf
                            <div class="el-form-row">
                                <div class="el-form js-el-form">
                                    <span class="text-form-item">Email:</span>
                                    <input type="text" name="email" data-text-valid="This is a valid field" class="type-input">
                                </div>
                            </div>
                            <div class="form-row-btn form-row-btn__center">
                                <button type="submit" class="site-btn bnt-type-one f_15 js-bnt-valid">Восстановить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div><!-- кнопка прокрутки вверх -->

<div class="scroll-up js-scroll-up">
    <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
        <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
    </svg>
</div>

<div class="body__opas js-boyd--opas"></div>




