<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="UTF-8">
    <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-translate-customization" content="9f841e7780177523-3214ceb76f765f38-gc38c6fe6f9d06436-c">

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset("img/icons/apple-icon-57x57.png")}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/icons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/icons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/icons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/icons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/icons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/icons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('/libs/manifest.json')}}">

    <link rel="shortcut icon" href="{{ asset('img/icons/favicon.ico') }}">

    <title>@yield('title')</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('/css/public.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset("libs/fancybox/jquery.fancybox.min.css") }}" rel="stylesheet">

    <link href="{{asset('css/intlTelInput.css')}}" rel="stylesheet">
    <link href="{{asset('css/demo.css')}}" rel="stylesheet">


    {{--image cropp links--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" crossorigin="anonymous" />--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha256-WqU1JavFxSAMcLP2WIOI+GB2zWmShMI82mTpLDcqFUg=" crossorigin="anonymous"></script>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" integrity="sha256-jKV9n9bkk/CTP8zbtEtnKaKf+ehRovOYeKoyfthwbC8=" crossorigin="anonymous" />--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js" integrity="sha256-CgvH7sz3tHhkiVKh05kSUgG97YtzYNnWt6OXcmYzqHY=" crossorigin="anonymous"></script>--}}
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css"/>--}}

{{--    <link rel="stylesheet" href="{{ asset("css/fontawesome.css") }}">--}}
    @yield('css')
</head>

{{--image cropp style--}}
{{--<style type="text/css">--}}
{{--    img {--}}
{{--        display: block;--}}
{{--        max-width: 100%;--}}
{{--    }--}}
{{--    .preview {--}}
{{--        overflow: hidden;--}}
{{--        width: 160px;--}}
{{--        height: 160px;--}}
{{--        margin: 10px;--}}
{{--        border: 1px solid red;--}}
{{--    }--}}
{{--    .modal-lg{--}}
{{--        max-width: 1000px !important;--}}
{{--    }--}}
{{--</style>--}}
<body>

<!-- ОБОЛОЧКА -->
<div class="wrapper">

    <div class="wrapper__content">
        <!-- header.kit -->
        @include('inc.header')
    </div>

    <div class="container">
        {{-- show flash messages --}}
        @include('inc.flash-messages')
    </div>

    @yield('main_content')

    <!-- подвал сайта footer.kit-->
    @include('inc.footer')

</div>

<!-- не знаю пр какой кнопке будет вызываться http://joxi.ru/brR1oq4COMN7LA -->
<div class="popup js-popup" data-id-popup="popup-0-sois">
    <div class="popup__cont">
        <div class="popup__wrapp popup__wrapp_registr  js-popup__wrapp">

            <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                <div class="autorization">
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Соискателям</div>
                        <div class="popup__text">
                            <p>Если вы хотите найти работу
                                и создать анкету на нашем сайте</p>
                        </div>
                        <div class="popup__btn-row">
                            <a href="#" class="btn-site btn-site-six">Создать анкету</a>
                        </div>
                    </div>
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Работодателям</div>
                        <div class="popup__text">
                            <p>Вы можете совершенно бесплатно
                                добавить вакансию о поиске работника.</p>
                        </div>
                        <div class="popup__btn-row">
                            <a href="#" class="btn-site btn-site-six">Добавить вакансию</a>
                        </div>
                    </div>

                </div>
                <div class="applicants">
                    <div class="applicants__item applicants__item_mobile">
                        <div class="title-style-four popup__title">Уже зарегистрированы?</div>
                        <div class="popup__btn-row">
                            <a href="popup-0" class="btn-site js-btn-popup bnt-type-one">Войти в кабинет</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<input id="profile-images-count" type="hidden" value="{{ auth()->user() && auth()->user()->images ? count(auth()->user()->images) : 0 }}" />

@include('layouts.popups')

<!-- Подключеине скриптов -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery.mask-1.14.10.js') }}"></script>
<script src="{{ asset('js/jquery-ui-1.21.1.js') }}"></script>
<script src="{{ asset('libs/slick/slick.min.js') }}"></script>
<script src="{{ asset('libs/fancybox/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('libs/masket/jquery.maskedinput.min.js') }}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script src="{{ asset('js/main.js') }}"></script>
<script src="{{asset('/js/popup.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script src="{{ asset('js/ajax.js') }}"></script>
<script src="{{ asset('js/profile.js') }}"></script>


<script src="{{asset('js/google-translate.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="//translate.google.com/translate_a/element.js?cb=TranslateInit"></script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
@yield('js')

</body>
</html>
