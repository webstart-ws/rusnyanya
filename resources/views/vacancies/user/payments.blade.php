@extends('layouts.app')

@section('title') Мои платежи @endsection

@section('main_content')
    <div class="section-con con-vacancies">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="el-filetr">
                        <div class="el-filetr__item">
                            <h2 class="el-filetr__link el-filetr__link_active">Мои платежи</h2>
                        </div>

                    </div>



                    {{-- include pagination if needed --}}
                </div>

                @include('vacancies.aside')
            </div>
        </div>
    </div>

@endsection
