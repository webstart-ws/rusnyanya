@extends('layouts.app')

@section('title') Регистрация работодателя @endsection

{{--@section('css')--}}
    <link rel="stylesheet" href="{{ asset('css/register.css') }}">
{{--    <script src="https://www.google.com/recaptcha/api.js" async defer></script>--}}
{{--@endsection--}}

@section('main_content')
    <div class="section-con section-con__top-pad section-con_mob-pad section-con_no-cont section-reg">
        <div class="container" id="vacancies-register-page">
            <div class="section-con__row" style="display: flex">
                <div class="section-con__main">
                    <div class="pages pages__cont">
                        <div class="pages__item pages-anket">
                            <div class="vacancy">
                                <h1 class="title-style-tree">Регистрация работодателя</h1>
                                @if($infoMessage)
                                    <div class="info-message">{{ $infoMessage }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="pages__item response">
                            <div class="page-forms">
                                <form action="{{ route('vacancy.register') }}" method="post" class="js-form-validation"  enctype="multipart/form-data">
                                    @csrf

                                    <div class="page-forms__cont page-forms__cont_active js-page-forms__one page-forms__one mt-0" data-pag-form="0">
                                        <div class="page-forms__max-w">
                                            <div class="title-form-h mt-0">Личные данные</div>

                                            <div class="form-row-wr">
                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Ваше имя:</span>
                                                        <input type="text" data-text-valid="Это поле обязательно"
                                                               value="{{ old('name') }}" name="name" class="type-input ">
                                                        @if($errors->has('name'))
                                                            <div class="validation-error-text">{{ $errors->first('name') }}</div>
                                                        @endif
                                                    </div>
                                                    <div class="el-form ">
                                                        <span class="text-form-item ">Фамилия:</span>
                                                        <input type="text" data-text-valid="Это поле обязательно"
                                                               value="{{ old('fam') }}" name="fam" class="type-input ">
                                                        @if($errors->has('fam'))
                                                            <div class="validation-error-text">{{ $errors->first('fam') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form ">
                                                        <span class="text-form-item">E-mail:</span>
                                                        <input type="text" data-text-valid="Это поле обязательно" id="email" value="{{ old('email') }}" name="email" class="type-input ">
                                                        @if($errors->has('email'))
                                                            <div class="validation-error-text">{{ $errors->first('email') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form ">
                                                        <span class="text-form-item inactiv_focuse_title">Пароль:</span>
                                                        <input type="password" data-text-valid="Минимум 6 символов" id="pwd" name="password" class="type-input ">
                                                        <span class="show_pass">
                                                            <svg class="svg_one_pass" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0z"/>
                                                                <path fill="rgba(111,127,137,0.3)" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.822 9.822 0 0 1-17.64 0A9.77 9.77 0 0 1 12 6m0-2a11.827 11.827 0 0 0-11 7.5 11.817 11.817 0 0 0 22 0A11.827 11.827 0 0 0 12 4zm0 5a2.5 2.5 0 1 1-2.5 2.5A2.5 2.5 0 0 1 12 9m0-2a4.5 4.5 0 1 0 4.5 4.5A4.507 4.507 0 0 0 12 7z"/>
                                                            </svg>
                                                            <svg class="svg_one_pass_act" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z"/>
                                                                <path fill="#2d3133" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.647 9.647 0 0 1-2.41 3.12l1.41 1.41A11.8 11.8 0 0 0 23 11.5 11.834 11.834 0 0 0 8.36 4.57l1.65 1.65A10.108 10.108 0 0 1 12 6zm-1.07 1.14L13 9.21a2.5 2.5 0 0 1 1.28 1.28l2.07 2.07a4.679 4.679 0 0 0 .14-1.07A4.483 4.483 0 0 0 12 7a4.244 4.244 0 0 0-1.07.14zM2.01 3.87l2.68 2.68A11.738 11.738 0 0 0 1 11.5 11.827 11.827 0 0 0 12 19a11.73 11.73 0 0 0 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45zm7.5 7.5l2.61 2.61A.5.5 0 0 1 12 14a2.5 2.5 0 0 1-2.5-2.5c0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75a4.6 4.6 0 0 0-.36 1.78 4.505 4.505 0 0 0 6.27 4.14l.98.98A10.432 10.432 0 0 1 12 17a9.77 9.77 0 0 1-8.82-5.5 9.9 9.9 0 0 1 2.93-3.53z"/>
                                                            </svg>
                                                        </span>

                                                        @if($errors->has('password'))
                                                            <div class="validation-error-text">{{ $errors->first('password') }}</div>
                                                        @endif
                                                    </div>

                                                    <div class="el-form ">
                                                        <span class="text-form-item inactiv_focuse_title">Повторите пароль:</span>
                                                        <input type="password" data-text-valid="Не совпадает" name="password_repeat" id="pwd_two" class="type-input">
                                                        <span class="show_pass_two">
                                                            <svg class="svg_one_pass1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0z"/>
                                                                <path fill="rgba(111,127,137,0.3)" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.822 9.822 0 0 1-17.64 0A9.77 9.77 0 0 1 12 6m0-2a11.827 11.827 0 0 0-11 7.5 11.817 11.817 0 0 0 22 0A11.827 11.827 0 0 0 12 4zm0 5a2.5 2.5 0 1 1-2.5 2.5A2.5 2.5 0 0 1 12 9m0-2a4.5 4.5 0 1 0 4.5 4.5A4.507 4.507 0 0 0 12 7z"/>
                                                            </svg>
                                                            <svg class="svg_one_pass_act1" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                                                <path fill="none" d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z"/>
                                                                <path fill="#2d3133" d="M12 6a9.77 9.77 0 0 1 8.82 5.5 9.647 9.647 0 0 1-2.41 3.12l1.41 1.41A11.8 11.8 0 0 0 23 11.5 11.834 11.834 0 0 0 8.36 4.57l1.65 1.65A10.108 10.108 0 0 1 12 6zm-1.07 1.14L13 9.21a2.5 2.5 0 0 1 1.28 1.28l2.07 2.07a4.679 4.679 0 0 0 .14-1.07A4.483 4.483 0 0 0 12 7a4.244 4.244 0 0 0-1.07.14zM2.01 3.87l2.68 2.68A11.738 11.738 0 0 0 1 11.5 11.827 11.827 0 0 0 12 19a11.73 11.73 0 0 0 4.32-.82l3.42 3.42 1.41-1.41L3.42 2.45zm7.5 7.5l2.61 2.61A.5.5 0 0 1 12 14a2.5 2.5 0 0 1-2.5-2.5c0-.05.01-.08.01-.13zm-3.4-3.4l1.75 1.75a4.6 4.6 0 0 0-.36 1.78 4.505 4.505 0 0 0 6.27 4.14l.98.98A10.432 10.432 0 0 1 12 17a9.77 9.77 0 0 1-8.82-5.5 9.9 9.9 0 0 1 2.93-3.53z"/>
                                                            </svg>
                                                        </span>
                                                        @if($errors->has('password_repeat'))
                                                            <div class="validation-error-text">{{ $errors->first('password_repeat') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__two page-forms__two" data-pag-form="1">
                                        <div class="title-form-h">Ваши фотографии</div>

                                        <div class="el-form-row form-margin">
                                            <label class="img-form">
                                                <input type="file" name="images[]" id="register-images-input" accept="image/*" multiple="multiple" />
                                                <span class="img-form__text">Перетащите файлы сюда или нажмите кнопку ниже,
                                                    чтобы выбрать их на компьютере. Разрешается добавлять до 5 фотографий</span>
                                                <span class="img-form__btn">
                                                    <button class="site-btn bnt-type-one bnt-type-one3 f_15" id="file-choose-btn">Выберите файл</button>
                                                </span>
                                            </label>

                                            @error('images.*')
                                            <div class="validation-error-text">{{ $message }}</div>
                                            @enderror

                                            <div id="register-images"></div>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="0">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="2">Далее</a>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="0">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="2">Далее</a>
                                        </div>

                                    </div>

                                    <div class="page-forms__cont js-page-forms__three page-forms__three" data-pag-form="2">
                                        <div class="title-form-h">Контактные данные</div>
                                        <div class="el-form-row">
                                            <p class="page-forms__text page-bottom">Укажите ваши контактные данные, по которым вы хотели бы получать отклики от заинтересованных соискателей.</p>
                                        </div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form">
                                                <span class="text-form-item">E-mail:</span>
                                                <input type="text" data-text-valid="Это поле обязательно" id="mail" value="{{ old('email1') }}" name="email1" class="type-input ">
                                                @if($errors->has('email1'))
                                                    <div class="validation-error-text">{{ $errors->first('email1') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form" id="tele">
                                                <span class="text-form-item">Телефон:</span>
                                                <input type="tel" id="phone" data-text-valid="This is a valid field" value="{{ old('phone') }}"
                                                       name="phone" class="type-input " />
                                                @if($errors->has('phone'))
                                                    <div class="validation-error-text">{{ $errors->first('phone') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form" id="tele">
                                                <span class="text-form-item">Whatsapp:</span>
                                                <input type="tel" id="phone1" data-text-valid="This is a valid field" value="{{ old('whatsapp') }}"
                                                       name="whatsapp" class="type-input" />
                                                @if($errors->has('whatsapp'))
                                                    <div class="validation-error-text">{{ $errors->first('whatsapp') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form ">
                                                <span class="text-form-item">Skype:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('skype') }}" name="skype" class="type-input">
                                                @if($errors->has('skype'))
                                                    <div class="validation-error-text">{{ $errors->first('skype') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form ">
                                                <span class="text-form-item">Viber:</span>
                                                <input type="tel" id="phone2" data-text-valid="Это поле обязательно" value="{{ old('viber') }}"
                                                       name="viber" class="type-input js-in-phone " />
                                                @if($errors->has('viber'))
                                                    <div class="validation-error-text">{{ $errors->first('viber') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Ссылка на социальную сеть:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('linkSocial') }}" name="soc_link" class="type-input">
                                                @if($errors->has('linkSocial'))
                                                    <div class="validation-error-text">{{ $errors->first('linkSocial') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="el-form-row">
                                        <div class="page-forms-bot">
                                            <div class="page-forms-bot__left">
                                                <label class="el-check ">
                                                    <input type="checkbox" class="form-checkbox " name="hide_contacts" value="1"
                                                        {{ old('hide_contacts') ? 'checked' : '' }} />
                                                    <i class="shecked-icon"></i>
                                                    <span class="check-text">Скрыть мои контакты для соискателей</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="el-form-row">
                                        <div class="page-forms-bot">
                                            <div class="page-forms-bot__left">
                                                <label class="el-check">
                                                    <input type="checkbox" class="form-checkbox" name="subscriptions_enabled" value="1"
                                                        {{ old('subscriptions_enabled') ? 'checked' : '' }} />
                                                    <i class="shecked-icon"></i>
                                                    <span class="check-text">Рассылка новостей</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="el-form-row">
                                        <div class="page-forms-bot mb-1">
                                            <div class="page-forms-bot__left">
                                                @if($errors->has('agree_agreement'))
                                                    <div class="validation-error-text">{{ $errors->first('agree_agreement') }}</div>
                                                @endif
                                                <label class="el-check ">
                                                    <input type="checkbox" class="form-checkbox " name="agree_agreement" value="1"
                                                        {{ old('agree_agreement') ? 'checked' : '' }} />
                                                    <i class="shecked-icon"></i>
                                                    <span class="check-text">Я согласен с условиями оказания
                                                        услуг и с политикой конфиденциальности в отношении обработки персональных данных </span>
                                                </label>
                                            </div>
                                        </div>
{{--                                        <div class="g-recaptcha mb-2" data-callback="recaptchaCallback" data-sitekey="{{ config('app.captcha.site_key') }}"></div>--}}

{{--                                        <div class="page-forms-bot mb-2">--}}
{{--                                            <div class="form-group">--}}
{{--                                                {!! NoCaptcha::renderJs() !!}--}}
{{--                                                {!! NoCaptcha::display() !!}--}}
{{--                                                @error('g-recaptcha-response')--}}
{{--                                                    <div class="alert alert-danger mt-1 mb-1"> {{ $message }} </div>--}}
{{--                                                @enderror--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="page-forms-bot__right">
                                            <button type="submit" class="site-btn bnt-type-one bnt-type-one1 f_15 js-bnt-valid">Зарегистрироваться</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section-con__aside">
                    <div class="aside-content">
                        <div class="title-style-six">Для новичков</div>
                        <p>Вакансия заполняется по желанию. Размещается бесплатно до 10 дней. Заявки от пользователей вы будете получать на почту. </p>
                        <p>Вы можете на время скрыть вашу вакансию, указав Мой статус 'Скрыть из поиска'. </p>
                        <p>После оплаты тарифа вам будут доступны контатные данные соискателей.  </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit"></script>
    <script src="{{asset('js/intlTelInput.js')}}"></script>
    <script>
        $('.in_search_label').on('click', function (){
            $('input[name=in_search]').trigger('click');
        });

        function recaptchaCallback() {
            $('button[type=submit]').removeAttr('disabled');
        }

        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            utilsScript: "js/utils.js",
        });


        var input1 = document.querySelector("#phone1");
        window.intlTelInput(input1, {
            utilsScript: "js/utils.js",
        });

        var input2 = document.querySelector("#phone2");
        window.intlTelInput(input2, {
            utilsScript: "js/utils.js",
        });

    </script>

@endsection
