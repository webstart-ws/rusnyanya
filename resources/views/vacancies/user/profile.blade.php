@extends('layouts.app')

@section('title') Профиль @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/register.css') }}">
@endsection

@section('main_content')
    <div class="section-con section-con__top-pad section-con_mob-pad section-con_no-cont section-reg">
        <div class="container" id="profile-page">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages pages__cont">
                        <div class="pages__item pages-anket">
                            <div class="vacancy">
                                <h1 class="title-style-tree">Редактирование профиля</h1>
                            </div>
                        </div>
                        <div class="pages__item response">
                            <div class="page-forms">
                                <form action="{{ route('vacancy.profile.update') }}" method="post" class="js-form-validation" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')

                                    <div class="page-forms__cont page-forms__cont_active js-page-forms__one page-forms__one" data-pag-form="0">
                                        <div class="page-forms__max-w">
                                            <div class="title-form-h">Личные данные</div>

                                            <div class="form-row-wr">
                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Ваше имя:</span>
                                                        <input type="text" data-text-valid="This is a valid field"
                                                               value="{{ old('name') ?? $user->name }}" name="name" class="type-input js-valid-in">
                                                        @if($errors->has('name'))
                                                            <div class="validation-error-text">{{ $errors->first('name') }}</div>
                                                        @endif
                                                    </div>
                                                    <div class="el-form">
                                                        <span class="text-form-item ">Фамилия:</span>
                                                        <input type="text" data-text-valid="This is a valid field"
                                                               value="{{ old('fam')  ?? $user->fam }}" name="fam" class="type-input js-valid-in">
                                                        @if($errors->has('fam'))
                                                            <div class="validation-error-text">{{ $errors->first('fam') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row">
                                                    <div class="el-form js-el-form">
                                                        <span class="text-form-item">E-mail:</span>
                                                        <input type="text" data-text-valid="This is a valid field" name="email"
                                                               value="{{ old('email') ?? $user->email }}" class="type-input">
                                                        @if($errors->has('email'))
                                                            <div class="validation-error-text">{{ $errors->first('email') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_col">
                                                    <div class="el-form js-el-form">
                                                        <span class="text-form-item">Пароль:</span>
                                                        <input type="password" data-text-valid="min 6 characters" name="password" value="{{ old('password') }}" class="type-input">
                                                        @if($errors->has('password'))
                                                            <div class="validation-error-text">{{ $errors->first('password') }}</div>
                                                        @endif
                                                    </div>
                                                    <div class="el-form js-el-form">
                                                        <span class="text-form-item">Повторите пароль:</span>
                                                        <input type="password" data-text-valid="Не совпадает" name="password_repeat" value="{{ old('password_repeat') }}" class="type-input">
                                                        @if($errors->has('password_repeat'))
                                                            <div class="validation-error-text">{{ $errors->first('password_repeat') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form no-btn  btn-site-7 f_15" data-btn-form="end">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="1">Далее</a>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__two page-forms__two" data-pag-form="1">
                                        <div class="title-form-h">Ваши фотографии</div>

                                        <div class="el-form-row form-margin">
                                            <label class="img-form">
                                                <input type="file" name="images[]" id="register-images-input" accept="image/*" multiple="multiple" />
                                                <span class="img-form__text">Перетащите файлы сюда или нажмите кнопку ниже,
                                                    чтобы выбрать их на компьютере. Разрешается добавлять до 5 фотографий.
                                                    Разрешены только изображения jpeg, png, jpg, webp и jfif.
                                                </span>
                                                <span class="img-form__btn">
                                                    <button class="site-btn bnt-type-one bnt-type-one3 f_15" id="file-choose-btn">Выберите файл</button>
                                                </span>
                                            </label>

                                            @error('images.*')
                                                <div class="validation-error-text">{{ $message }}</div>
                                            @enderror

                                            <div id="register-images">
                                                @foreach($user->images as $image)
                                                    <div>
                                                        <img class="thumbnail" src="{{ asset($image->path) }}" title="" alt="" />
                                                        <span class="close delete-btn" data-id="{{ $image->id }}">&times;</span>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="0">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="2">Далее</a>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__three page-forms__three" data-pag-form="2">
                                        <div class="title-form-h">Контактные данные</div>
                                        <div class="el-form-row">
                                            <p class="page-forms__text page-bottom">Укажите ваши контактные данные, по которым вы хотели бы получать отклики от заинтересованных соискателей.</p>
                                        </div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form js-el-form">
                                                <span class="text-form-item">E-mail:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('email1') ?? $user->email1 }}" name="email1" class="type-input">
                                                @if($errors->has('email1'))
                                                    <div class="validation-error-text">{{ $errors->first('email1') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Телефон:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('phone') ?? $user->phone }}"
                                                       name="phone" class="type-input js-in-phone js-valid-in" placeholder="+7(495) 000 00 00">
                                                @if($errors->has('phone'))
                                                    <div class="validation-error-text">{{ $errors->first('phone') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Whatsapp:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('whatsapp') ?? $user->whatsapp }}" name="whatsapp" class="type-input">
                                                @if($errors->has('whatsapp'))
                                                    <div class="validation-error-text">{{ $errors->first('whatsapp') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form">
                                                <span class="text-form-item">Skype:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('skype') ?? $user->skype }}" name="skype" class="type-input">
                                                @if($errors->has('skype'))
                                                    <div class="validation-error-text">{{ $errors->first('skype') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Viber:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('viber') ?? $user->viber }}" name="viber" class="type-input js-in-phone" placeholder="+7(495) 000 00 00">
                                                @if($errors->has('viber'))
                                                    <div class="validation-error-text">{{ $errors->first('viber') }}</div>
                                                @endif
                                            </div>
                                            <div class="el-form">
                                                <span class="text-form-item">Ссылка на социальную сеть:</span>
                                                <input type="text" data-text-valid="This is a valid field" value="{{ old('linkSocial') ?? $user->linkSocial }}" name="soc_link" class="type-input">
                                                @if($errors->has('linkSocial'))
                                                    <div class="validation-error-text">{{ $errors->first('linkSocial') }}</div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="1">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="3">Далее</a>
                                        </div>
                                    </div>

                                    <div class="el-form-row">
                                        <div class="page-forms-bot">
                                            <div class="page-forms-bot__left">
                                                <label class="el-check js-el-form">
                                                    <input type="checkbox" class="form-checkbox" name="hide_contacts" value="1"
                                                        {{ old('hide_contacts') || $user->hide_contacts ? 'checked' : '' }} />
                                                    <i class="shecked-icon"></i>
                                                    <span class="check-text">Скрыть мои контакты для соискателей</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="el-form-row">
                                        <div class="page-forms-bot">
                                            <div class="page-forms-bot__left">
                                                <label class="el-check js-el-form">
                                                    <input type="checkbox" class="form-checkbox" name="subscriptions_enabled" value="1"
                                                        {{ old('subscriptions_enabled') || $user->subscriptions_enabled ? 'checked' : '' }} />
                                                    <i class="shecked-icon"></i>
                                                    <span class="check-text">Рассылка новостей</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="el-form-row">
                                        <div class="page-forms-bot">
                                            <div class="page-forms-bot__right">
                                                <button type="submit" class="site-btn bnt-type-one bnt-type-one1 f_15 js-bnt-valid">Сохранить</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                @include('vacancies.aside')
            </div>
        </div>
    </div>
@endsection
