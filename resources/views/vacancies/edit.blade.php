@extends('layouts.app')

@section('title')  Редактирование вакансии @endsection

@section('main_content')
    <div class="section-con section-con__top-pad section-con_mob-pad section-con_no-cont section-reg">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages pages__cont">
                        <div class="pages__item pages-anket">
                            <div class="vacancy">
                                <h1 class="title-style-tree">Редактирование вакансии</h1>
                            </div>
                        </div>
                        <div class="pages__item response">
                            <div class="page-forms">
                                <form action="{{ route('vacancy.update', $vacancy->id) }}" method="post" class="js-form-validation">
                                    @csrf
                                    @method('PUT')

                                    <div class="page-forms__cont js-page-forms__four page-forms__four" data-pag-form="3">
                                        <div class="wr-tops">
                                            <div class="title-form-h">Информация вакансии</div>
                                            <div class="el-form-row">
                                                <div class="el-form js-el-form">
                                                    <span class="text-form-item">Заголовок *</span>
                                                    <input type="text" name="title" class="type-input" value="{{ old('title') ?? $vacancy->title }}">
                                                    @if($errors->has('title'))
                                                        <div class="validation-error-text">{{ $errors->first('title') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="el-form-row">
                                                <div class="el-form js-el-form">
                                                    <span class="text-form-item">Текст *</span>
                                                    <textarea name="comments" class="type-textarea js-textarea">{{ old('comments') ?? $vacancy->comments }}</textarea>
                                                    @if($errors->has('comments'))
                                                        <div class="validation-error-text">{{ $errors->first('comments') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__four page-forms__four" data-pag-form="3">
                                        <div class="wr-tops">
                                            <div class="title-form-h">Ищу кандидата в</div>
                                            <div class="el-form-row el-form-row_col">
                                                <div class="el-form">
                                                    <span class="text-form-item">Страна:</span>
                                                    @if($errors->has('country_id'))
                                                        <div class="validation-error-text">{{ $errors->first('country_id') }}</div>
                                                    @endif
                                                    <div class="el-select el-select_filter   js-select">
                                                        <div class="el-select__main js-select-main">
                                                            <span class="el-select__text js-select-text">Все страны</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                </svg>
                                                            </i>
                                                        </div>

                                                        <div class="el-select__content js-el-select-content">
                                                            <x-countries-select></x-countries-select>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="country_id" value="{{ old('country_id') ?? $vacancy->country_id }}">
                                                    @if($errors->has('country'))
                                                        <div class="validation-error-text">{{ $errors->first('country') }}</div>
                                                    @endif
                                                </div>

                                                <div class="el-form">
                                                    <span class="text-form-item">Регион:</span>
                                                    @if($errors->has('region_id'))
                                                        <div class="validation-error-text">{{ $errors->first('region_id') }}</div>
                                                    @endif
                                                    <div class="el-select el-select_filter js-select">
                                                        <div class="el-select__main js-select-main">
                                                            <span class="el-select__text js-select-text">Любой регион</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                </svg>
                                                            </i>
                                                        </div>

                                                        <div class="el-select__content js-el-select-content" id="regions-select-area"></div>
                                                    </div>

                                                    <input type="hidden" name="region_id" value="{{ old('region_id') ?? $vacancy->region_id }}">
                                                </div>

                                                <div class="el-form">
                                                    <span class="text-form-item">Город:</span>
                                                    @if($errors->has('city_id'))
                                                        <div class="validation-error-text">{{ $errors->first('city_id') }}</div>
                                                    @endif
                                                    <div class="el-select el-select_filter js-select">
                                                        <div class="el-select__main js-select-main">
                                                            <span class="el-select__text js-select-text">Любой город</span>
                                                            <i class="svg-icon">
                                                                <svg class="svg-icon_ar-bot">
                                                                    <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                </svg>
                                                            </i>
                                                        </div>

                                                        <div class="el-select__content js-el-select-content" id="cities-select-area"></div>
                                                    </div>

                                                    <input type="hidden" name="city_id" value="{{ old('city_id') ?? $vacancy->city_id }}">
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__five page-forms__five" data-pag-form="4">
                                        <div class="el-form-row"></div>
                                        <div class="el-form-row el-form-row_three">
                                            <div class="el-form">
                                                <div class="el-form-row">
                                                    <div class="el-form">
                                                        <span class="title-form-h">На должность</span>
                                                        <div class="filter__row-item">
                                                            <ul class="filter__list filter-list js-filter-list">
                                                                @foreach($positions as $position)
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="radio" class="form-checkbox" name="position_id" value="{{ $position->id }}"
                                                                                {{ old('position_id') == $position->id || $vacancy->position_id == $position->id ? 'checked' : '' }}>
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">{{ $position->name }}</span>
                                                                        </label>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                        @if($errors->has('position'))
                                                            <div class="validation-error-text">{{ $errors->first('position') }}</div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="el-form el-form_w_flex">
                                                <div class="el-form-row el-form-row_two">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Предлагаемая зарплата:</span>
                                                        <div class="salary">
                                                            <div class="salary__price">
                                                                <input type="number" class="salary-in" placeholder="Сумма"
                                                                       name="salary" value="{{ old('salary') ?? $vacancy->salary }}">
                                                                <span class="salary__currency">€</span>
                                                                @if($errors->has('salary'))
                                                                    <div class="validation-error-text">{{ $errors->first('salary') }}</div>
                                                                @endif
                                                            </div>

                                                            <div class="salary__select">
                                                                <div class="el-select el-select_filter js-select">
                                                                    <div class="el-select__main js-select-main">
                                                                        <span class="el-select__text js-select-text">В час</span>
                                                                        <i class="svg-icon">
                                                                            <svg class="svg-icon_ar-bot">
                                                                                <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                            </svg>
                                                                        </i>
                                                                    </div>

                                                                    <div class="el-select__content js-el-select-content">
                                                                        <ul class="el-select__list">
                                                                            @foreach($prices as $price_period)
                                                                                <li class="el-select__item js-select-item " data-value="{{ $price_period->name }}" >
                                                                                    <input type="radio" style="display: none" name="price_period" value="{{ $price_period->id }}}">
                                                                                    {{ $price_period->name }}
                                                                                </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                    @if($errors->has('price_period'))
                                                                        <div class="validation-error-text">{{ $errors->first('price_period') }}</div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="el-form-row el-form-row_two">
                                                    <div class="el-form">
                                                        <span class="text-form-item">Рекомендации:</span>
                                                        <div class="el-select el-select_filter   js-select">
                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text">Нет</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>

                                                            <div
                                                                class="el-select__content js-el-select-content">
                                                                <ul class="el-select__list">
                                                                    <li class="el-select__item js-select-item el-select__item_active" data-value="Нет">
                                                                        <input type="text" style="display: none" name="recommend" value="0">
                                                                        Нет
                                                                    </li>
                                                                    <li class="el-select__item js-select-item" data-value="Да">
                                                                        <input type="text" style="display: none" name="recommend" value="1">
                                                                        Да
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        @if($errors->has('recommendations'))
                                                            <div class="validation-error-text">{{ $errors->first('recommendations') }}</div>
                                                        @endif
                                                    </div>

                                                    <div class="el-form">
                                                        <span class="text-form-item">Водительские права:</span>
                                                        <div class="el-select el-select_filter   js-select">
                                                            <div class="el-select__main js-select-main">
                                                                <span class="el-select__text js-select-text">Нет</span>
                                                                <i class="svg-icon">
                                                                    <svg class="svg-icon_ar-bot">
                                                                        <use xlink:href="img/svg/sprite.svg#icon-arrow-bot"></use>
                                                                    </svg>
                                                                </i>
                                                            </div>

                                                            <div class="el-select__content js-el-select-content">
                                                                <ul class="el-select__list">
                                                                    <li class="el-select__item js-select-item" data-value="Есть права">
                                                                        <input type="text" style="display: none" name="driver_license" value="0">
                                                                        Есть права
                                                                    </li>
                                                                    <li class="el-select__item js-select-item el-select__item_active" data-value="Нет">
                                                                        <input type="text" style="display: none" name="driver_license" value="1">
                                                                        Нет
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        @if($errors->has('driver_license'))
                                                            <div class="validation-error-text">{{ $errors->first('driver_license') }}</div>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="el-form el-form-row_two">
                                                    <div class="el-form">
                                                        <div class="el-form">
                                                            <span class="text-form-item">Родной язык:</span>
                                                            <div class="filter__row-item">
                                                                <ul class="filter__list filter-list js-filter-list">
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox"
                                                                                   class="form-checkbox">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">Русский</span>
                                                                        </label>
                                                                    </li>
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox"
                                                                                   class="form-checkbox">
                                                                            <i class="shecked-icon"></i>
                                                                            <span
                                                                                class="check-text">Украинский</span>
                                                                        </label>
                                                                    </li>
                                                                    <li
                                                                        class="filter-list__item js-fil-list filter-list__item_none">
                                                                        <ul>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            2</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            3</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            4</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            5</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            6</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            7</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            8</span>
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="filter-list__item">
                                                                        <a href=""
                                                                           class="filter-list__all js-fil-list-all"
                                                                           data-text="Свернуть">
                                                                            <span>Открыть все</span>
                                                                            <i class="svg-icon">
                                                                                <svg class="svg-icon_ar-bot">
                                                                                    <use
                                                                                        xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                                    </use>
                                                                                </svg>
                                                                            </i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            @if($errors->has('native_language'))
                                                                <div class="validation-error-text">{{ $errors->first('native_language') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="el-form">
                                                        <div class="el-form">
                                                            <span class="text-form-item">Какие языки вы еще знаете?</span>
                                                            <div class="filter__row-item">
                                                                <ul class="filter__list filter-list js-filter-list">
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox"
                                                                                   class="form-checkbox">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">English</span>
                                                                        </label>
                                                                    </li>
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox"
                                                                                   class="form-checkbox">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">German</span>
                                                                        </label>
                                                                    </li>
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox"
                                                                                   class="form-checkbox">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">French</span>
                                                                        </label>
                                                                    </li>
                                                                    <li class="filter-list__item">
                                                                        <label class="el-check">
                                                                            <input type="checkbox"
                                                                                   class="form-checkbox">
                                                                            <i class="shecked-icon"></i>
                                                                            <span class="check-text">Spanish</span>
                                                                        </label>
                                                                    </li>
                                                                    <li
                                                                        class="filter-list__item js-fil-list filter-list__item_none">
                                                                        <ul>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            2</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            3</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            4</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            5</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            6</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            7</span>
                                                                                </label>
                                                                            </li>
                                                                            <li class="filter-list__item">
                                                                                <label class="el-check">
                                                                                    <input type="checkbox"
                                                                                           class="form-checkbox">
                                                                                    <i class="shecked-icon"></i>
                                                                                    <span class="check-text">Русский
                                                                                            8</span>
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </li>
                                                                    <li class="filter-list__item">
                                                                        <a href=""
                                                                           class="filter-list__all js-fil-list-all"
                                                                           data-text="Свернуть">
                                                                            <span>Открыть все</span>
                                                                            <i class="svg-icon">
                                                                                <svg class="svg-icon_ar-bot">
                                                                                    <use
                                                                                        xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                                                    </use>
                                                                                </svg>
                                                                            </i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            @if($errors->has('other_language'))
                                                                <div class="validation-error-text">{{ $errors->first('other_language') }}</div>
                                                            @endif
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="page-forms__btn">
                                            <a href="#" class="site-btn bnt-type-one js-btn-form btn-site-7 f_15" data-btn-form="3">Назад</a>
                                            <a href="#" class="site-btn bnt-type-one js-btn-form f_15" data-btn-form="5">Далее</a>
                                        </div>
                                    </div>

                                    <div class="page-forms__cont js-page-forms__four page-forms__four" data-pag-form="3">
                                        <div class="wr-tops">
                                            <div class="title-form-h">Документы кандидата</div>

                                            @if($errors->has('document_id'))
                                                <div class="validation-error-text">{{ $errors->first('document_id') }}</div>
                                            @endif

                                            @if(count($documents) > 0)
                                                <div class="el-form-row el-form-row_three">
                                                    @foreach($documents as $i => $document)
                                                        <div class="el-form el-form_w_flex">
                                                            <div class="el-form-row">
                                                                <div class="el-form">
                                                                    <div class="filter__row-item">
                                                                        <ul class="filter__list filter-list js-filter-list">
                                                                            @if(isset($document[$i]))
                                                                                @foreach($documents[$i] as $doc)
                                                                                    <li class="filter-list__item">
                                                                                        <label class="el-check">
                                                                                            <input type="radio" class="form-checkbox" name="document_id" value="{{ $doc['id'] }}"
                                                                                                {{ old('document_id') == $doc['id'] || $vacancy->document_id == $doc['id'] ? 'checked' : '' }}>
                                                                                            <i class="shecked-icon"></i>
                                                                                            <span class="check-text">{{ $doc['name'] }}</span>
                                                                                        </label>
                                                                                    </li>
                                                                                @endforeach
                                                                            @endif
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="page-forms__cont js-page-forms__four page-forms__four" data-pag-form="3">
                                        <div class="wr-tops">
                                            <div class="el-form-row">
                                                <div class="ancet-search">
                                                    <span class="text-form-item">Участие в поиске.</span>
                                                    <p class="ancet-search__text">Вы можете скрыть вашу вакансию из поиска, например, если нашли работника.</p>
                                                    <div class="slider-row-btn">
                                                        <span class="slider-row-btn__left">Опубликовать</span>
                                                        <label class="slider-btn">
                                                            <input type="checkbox" class="slider-btn__check" name="published"
                                                                   {{ old('published') == '0' || $vacancy->published == 0 ? '' : 'checked' }} value="1">
                                                            <i class="slider-btn__icon"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="el-form-row">
                                                <div class="page-forms-bot__right">
                                                    <button type="submit" class="site-btn bnt-type-one bnt-type-one1 f_15 js-bnt-valid">Обновить</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @include('vacancies.aside')
            </div>
        </div>
    </div>
@endsection
