 @extends('layouts.app')

@section('title') Вакансия - {{ $vacancy->title }} @endsection

@section('main_content')
<div class="section-con section-con__top-pad section-con_mob-pad  section-con_no-cont">
    <div class="container">
        <div class="section-con__row">
            <div class="section-con__main">
                <div class="pages vacancy-mob-pad">
                    <div class="pages__item pages__item_mob bgwhite">
                        <div class="vacancy">
                            <h1 class="title-style-five">{{ $vacancy->title }}</h1>
                            <div class="vacancy__info">{{ $vacancy->position_name }}</div>
                            <div class="el-info vacancy_el_info">
                                <h2 class="title-style-five questionnaire__title">{{ $vacancy->author->name }}</h2>
                                <div class="el-info__item">
                                    <i class="svg-icon">
                                        <svg>
                                            <use xlink:href="{{asset('img/svg/sprite.svg#icon-money')}}"></use>
                                        </svg>
                                    </i>
                                    <!-- t_strong t_red-->
                                    <span class="el-info__text t_strong t_red">{{ $vacancy->salary }} {{$vacancy->price_period_id == "0" ? 'в час' : ' в месяц'}}</span>
                                </div>
                                <div class="el-info__item">
                                    <i class="svg-icon">
                                        <svg>
                                            <use xlink:href="{{asset("img/svg/sprite.svg#label-two")}}"></use>
                                        </svg>
                                    </i>
                                    <span class="el-info__text">{{ $vacancy->country_name }} {{  $vacancy->city_name }}</span>
                                </div>
                                <div class="el-info__item">
                                    <i class="svg-icon">
                                        <svg>
                                            <use xlink:href="{{asset("img/svg/sprite.svg#icon-home")}}"></use>
                                        </svg>
                                    </i>
                                    <span class="el-info__text no-life">{{ $vacancy->document_name }}</span>
                                </div>
                            </div>
                            <div class="el-hr el-mob"></div>
                            <div class="vacancy__item">
                                <ul class="vacancy__list-item vacancy-list">
                                    <li>
                                        <span class="vacancy-list__left">Документы:</span>
                                        <span class="vacancy-list__right">{{ $vacancy->accommodation_names }}</span>
                                    </li>
                                    <li>
                                        <span class="vacancy-list__left">Район города: </span>
                                        <span class="vacancy-list__right">{{ $vacancy->region_name}}</span>
                                    </li>
                                    <li>
                                        <span class="vacancy-list__left">Родной язык: </span>
                                        <span class="vacancy-list__right">{{ $vacancy->language_name}}</span>
                                    </li>
                                    <li>
                                        <span class="vacancy-list__left">Рекомендации:</span>
                                        <span class="vacancy-list__right">{{$vacancy->recommendations == '1' ? "Обязательны рекомендации" : "Не обязательно"}}
                                            </span>
                                    </li>
                                    <li>
                                        <span class="vacancy-list__left">Водительские права:</span>
                                        <span class="vacancy-list__right">{{$vacancy->driver_license == '1' ? "Есть права" : "Нет"}}
                                            </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="el-hr"></div>
                        <div class="section-text section-text_mar">
                            <h3>Подробнее о вакансии</h3>
                            <p>{{$vacancy->comments}}</p>
                            @auth
                                <h3>Контакты работодателя</h3>
                                <p>Контакты работодателя Ксения скрыты, для просмотра контактов <a href="{{ route('tariffs') }}">оплатите тариф</a> чтобы связаться
                                    с работодателем на прямую или бесплатно оставьте свой отклик в форме ниже.</p>
                            @endauth
                        </div>
                        <div class="article-info article-info_mar">
                            <div class="article-info__item">
                                <i class="svg-icon">
                                    <svg class="svg-icon_id">
                                        <use xlink:href="{{asset("img/svg/sprite.svg#icon-id")}}"></use>
                                    </svg>
                                </i>
                                <span>{{$vacancy['id']}}</span>
                            </div>
                            <div class="article-info__item">
                                <i class="svg-icon">
                                    <svg class="svg-icon_eye">
                                        <use xlink:href="{{asset("img/svg/sprite.svg#icon-eye")}}"></use>
                                    </svg>
                                </i>
                                <span>{{$vacancy['number_views']}}</span>
                            </div>
                            <div class="article-info__item">
                                <i class="svg-icon">
                                    <svg class="svg-icon_pen">
                                        <use xlink:href="{{asset("img/svg/sprite.svg#icon-pen")}}"></use>
                                    </svg>
                                </i>
                                <span>{{$vacancy->from_date}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="pages__item pages__item_mob response">
                       <h3 class="title-style-four reviews-title">Отклики на вакансию</h3>
                       <div class="response__text"><p>Размещение контактных данных строго запрещено.</p>
                        <p>Анкеты с размещенными контактными данными будут удалены.</p></div>
                        <div class="response__form">
                            <form action="#">
                                <div class="el-form-row">
                                    <span class="text-form-item">Ваш отклик:</span>
                                    <textarea class="type-textarea"></textarea>
                                </div>
                                <div class="form-row-btn form-row-btn_end">
                                    <button class="site-btn bnt-type-one f_15">Отправить отклик</button>
                                </div>
                            </form>
                        </div>
                        <div class="el-hr el-other"></div>
                        <div class="hail">
{{--                            @foreach ($comments as $comment)--}}
{{--                                 <div class="hail__item item-hail">--}}
{{--                                    <div class="item-hail__img">--}}
{{--                                        <img src="img/hail-1.png" alt="img">--}}
{{--                                    </div>--}}
{{--                                <div class="item-hail__content">--}}
{{--                                    <div class="item-hail__top">--}}
{{--                                        <div class="item-hail__name">Анжела, 52</div>--}}
{{--                                        <div class="item-hail__date">15.09.2020</div>--}}
{{--                                    </div>--}}
{{--                                    <div class="item-hail__text">--}}
{{--                                        <p>{{$c->komm}}</p>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            @endforeach--}}
                            @foreach($comments as $comment)
                             <div class="hail__item item-hail">
                                <div class="item-hail__img">
                                    <img src="img/hail-2.png" alt="img">
                                </div>
                                <div class="item-hail__content">
                                    <div class="item-hail__top">
                                        <div class="item-hail__name">{{ $comment->author }}, 34</div>
                                        <div class="item-hail__country">Country</div>
                                        <div class="item-hail__date">{{$comment->date_comment}}</div>
                                    </div>
                                    <div class="item-hail__text">
                                        <p>{{$comment->komm}}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            @include('vacancies.aside')
        </div>
    </div>
</div>

<!-- кнопка прокрутки вверх -->
<div class="scroll-up js-scroll-up">
    <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
        <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
    </svg>
</div>

<div class="body__opas js-boyd--opas"></div>
@endsection
