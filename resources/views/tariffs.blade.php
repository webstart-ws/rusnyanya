@extends('layouts.app')

@section('title') Тарифы @endsection

@section('main_content')
    <div class="section-con section-con__top-pad section-con_no-cont section-con_mob-pad">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages">
                        <div class="pages__item">
                            <div class="vacancy vacancy_tar">
                                <h1 class="title-style-five">Тарифы</h1>
                                <div class="section-text section-text_p">
                                    <p>Услуги RUSnyanya бесплатные для соискателей. <br>
                                        Есть платные услуги, с ними вы можете ознакомится ниже: </p>
                                </div>
                                <div class="tariffs">
                                    <div class="tariff-item">
                                        <div class="tariff-item__main">
                                            <h3 class="title-style-six">Хороший старт</h3>
                                            <ul class="tariff-item__list">
                                                <li class="tariff-item__item">Вывод вашей анкеты в ТОП </li>
                                                <li class="tariff-item__item">Открытие ваших контактов для работодателей</li>
                                                <li class="tariff-item__item"> Открытие контактных данных работодателей  с опубликованной
                                                    вакансией  (только если разрешено работодателем) </li>
                                                <li class="tariff-item__item">Срок исполнения 15 дней</li>
                                            </ul>
                                        </div>
                                        <div class="tariff-item__right">
                                            <div class="tariff-item__price"><span>EUR</span> 25.00</div>
                                            <div class="tariff-item__btn">
                                                <a href="popup-2" class="site-btn js-btn-popup bnt-type-one bnt-type-one2 f_15">Оплатить</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tariff-item">
                                        <div class="tariff-item__main">
                                            <h3 class="title-style-six">Персональная помощь</h3>
                                            <ul class="tariff-item__list">
                                                <li class="tariff-item__item">Поиск работы под Вас! RUSnyanya является вашим
                                                    представителем на поиск работы и испытательный месяц</li>
                                                <li class="tariff-item__item">Стоимость: 50% от первой заработной платы.  Оплата по
                                                    договору после испытательного срока</li>
                                                <li class="tariff-item__item"> Срок исполенения 1 месяц</li>
                                            </ul>
                                        </div>
                                        <div class="tariff-item__right">
                                            <div class="tariff-item__price"><span>EUR</span> 0.00</div>
                                            <div class="tariff-item__btn ">
                                                <a href="popup-2" class="site-btn bnt-type-one bnt-type-one2 js-btn-popup f_15">Оплатить</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="popup js-popup" data-id-popup="popup-1">
        <div class="popup__cont">
            <div class="popup__wrapp popup__wrapp_stand  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="popup__content">
                        <div class="title-style-four popup__title text-center">Показ контактных данных</div>
                        <div class="popup__text text-center">
                            <p>У вас есть возможность посмотреть еще 6 контактных данных.
                                Вы хотите воспользоваться этим?</p>
                        </div>
                        <div class="popup__btn_two">
                            <a href="#" class="site-btn bnt-type-one f_15 btn-site-7">Отмена</a>
                            <a href="#" class="site-btn bnt-type-one f_15">Показать контакты</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- попап -->
    <div class="popup popup-payment js-popup" data-id-popup="popup-2">
        <div class="popup__cont no-center">
            <div class="popup__wrapp popup__wrapp_payment  js-popup__wrapp">

                <div class="popup__row">

                <span class="popup__close js-popup__close">
                    <svg>
                        <use xlink:href="../img/svg/sprite.svg#icon-close"></use>
                    </svg>
                </span>

                    <div class="popup-payment__left">
                        <div class="payment__prise">
                            <div class="payment__prise-text">К оплате</div>
                            <div class="payment__prise-price"><span>EUR</span> 25.00</div>
                        </div>
                        <div class="payment__left-bot">
                            <a href="#" class="btn-type-9">
                                <svg>
                                    <use xlink:href="../img/svg/sprite.svg#icon-ar-left"></use>
                                </svg>
                                <span>Отменить оплату</span>
                            </a>
                        </div>
                    </div>

                    <div class="popup-payment__right">
                        <div class="title-style-four popup__title">Оплата тарифа </div>
                        <div class="popup-payment__row">
                            <div class="popup__text">
                                <p>Оплатите с помощью карты</p>
                            </div>
                            <div class="popup-payment__cart">
                                <img src="img/visa.png" alt="img">
                                <img src="img/mastercart.png" alt="img">
                            </div>
                        </div>
                        <div class="popup__form">
                            <form action="#">
                                <div class="el-form-row">
                                    <div class="el-form">
                                        <span class="text-form-item">Номер кредитной карты:</span>
                                        <input type="text" name="card" data-text-valid="This is a valid field"
                                               class="type-input" placeholder="4584 - ">
                                    </div>
                                </div>
                                <span class="text-form-item">Срок действия:</span>
                                <div class="el-form-row ">
                                    <div class="cols-row">
                                        <div class="el-form">
                                            <div class="el-select el-select_filter mtpc0 js-select">

                                                <div class="el-select__main js-select-main">
                                                    <span class="el-select__text js-select-text">Все страны</span>
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon_ar-bot">
                                                            <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                </div>

                                                <div class="el-select__content wrap-select-tarifs js-el-select-content">
                                                    <ul class="el-select__list el-select__tarifs">
                                                        <li class="el-select__item js-select-item el-select__item_active"
                                                            data-value="Все страны">Все страны</li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="el-form">
                                            <div class="el-select el-select_filter mtpc0 js-select">

                                                <div class="el-select__main js-select-main">
                                                    <span class="el-select__text js-select-text">Все страны</span>
                                                    <i class="svg-icon">
                                                        <svg class="svg-icon_ar-bot">
                                                            <use xlink:href="img/svg/sprite.svg#icon-arrow-bot">
                                                            </use>
                                                        </svg>
                                                    </i>
                                                </div>

                                                <div class="el-select__content wrap-select-tarifs js-el-select-content">
                                                    <ul class="el-select__list el-select__tarifs">
                                                        <li class="el-select__item js-select-item el-select__item_active"
                                                            data-value="Все страны">Все страны</li>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-form-row">
                                    <div class="cols-row">
                                        <div class="el-form">
                                        <span class="text-form-item">CVC/CW
                                        </span>
                                            <input type="text" name="card" data-text-valid="This is a valid field"
                                                   class="type-input" placeholder="341">
                                        </div>
                                        <div class="el-form">
                                            <span class="info-text-dop">Код из 3 или 4 цифр</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="el-form-row">
                                    <label class="el-check el-check-top">
                                        <input type="checkbox" class="form-checkbox" checked>
                                        <i class="shecked-icon"></i>
                                        <span class="check-text">Соглашаюсь с условиями оказания услуг</span>
                                    </label>
                                </div>

                                <div class="form-row-btn form-row-btn_mar">
                                    <button type="submit" class="site-btn bnt-type-one bnt-type-one2 f_15 ">Перейти к оплате <span>EUR
                                        25.00</span></button>
                                </div>

                                <div class="popup__dop-b">
                                    <div class="popup__text text-center">
                                        <p>Или выберите другой способ оплаты</p>
                                    </div>
                                    <div class="el-form-row">
                                        <a href="{{ route('make.payment') }}" class="btn-pay">
                                            <span>Оплатить через</span>
                                            <img src="img/img-pay2.png" alt="img">
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="scroll-up js-scroll-up">
        <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
            <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
        </svg>
    </div>

    <div class="body__opas js-boyd--opas"></div>

@endsection

