@foreach(['success', 'danger', 'info', 'warning'] as $message)
    @if(session()->has($message))
        <div class="alert alert-{{$message}}">
            {{ session()->get($message) }}
        </div>
    @endif
@endforeach
