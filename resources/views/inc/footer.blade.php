<div class="footer">
    <div class="container">
        <div class="footer-top-mob js-footer-top-mob"></div>
        <div class="footer__row row_flex footer__row_top job_link">






        </div>
        <div class="footer__row row_flex footer-bottom js-footer-bottom">
            <div class="footer-bottom__item footer-bottom__item_mob row_flex js-wr-logo">
                <div class="footer__logo js-footer-logo">
                    <a href="/">
                        <img src="{{asset('img/svg/logo-site.svg')}}" alt="logo-footer">
                    </a>
                </div>
                <div class="footer__year">
                    <span>© 2012 — 2020</span>
                </div>
            </div>
            <div class="footer-bottom__item js-wr-social footer-bottom__item_mob">
                <div class="social js-footer-social">
                    <ul class="social__list">
                        <li class="social__item">
                            <a href="https://www.facebook.com/rusnyanya" class="social__link social__link_fb">
                                <svg class="svg-icon">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#fb')}}"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="https://web.telegram.org" class="social__link social__link_tl">
                                <svg class="svg-icon">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#tl')}}"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="https://vk.com/club44511022" class="social__link social__link_vk">
                                <svg class="svg-icon">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#vk')}}"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="https://www.youtube.com/channel/UCw5HUCcvNKr_XVt8XkNTHSw" class="social__link social__link_yo">
                                <svg class="svg-icon">
                                    <use xlink:href="{{asset('img/svg/sprite.svg#yo')}}"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer-bottom__item">
                <div class="payment">
                    <a href="{{asset("paywithpaypal")}}">
                        <div class="payment__item">
                                <img src="{{asset('img/img-pay.png')}}" alt="img">
                        </div>
                    </a>
                    <div class="payment__item">
                        <img src="{{asset('img/visa.png')}}" alt="img">
                    </div>
                    <div class="payment__item pay-card">
                        <img src="{{asset('img/mastercart.png')}}" alt="img">
                    </div>
                    <div class="payment__item pay-card">
                        <img src="{{asset('img/multipro.png')}}" alt="img">
                    </div>
                </div>
            </div>
            <div class="footer-bottom__item footer-bottom__item_developer">
                <div class="developer">
                    Разработка —  <a href="https://webstart.am">Webstart</a><br>
                    Дизайн —<a href="https://alexey-popov.com/"> Алексей Попов</a>

                </div>
            </div>
        </div>
    </div>

</div>
