<header class="header">
    <div class="container">
        <div class="header__row js-header-row row_flex">
            <div class="header__logo">
                <!-- на внутренних страницах убираем класс no-pointer-->
                <a href="/" class="">
                    <img src="{{asset('img/svg/logo-site.svg')}}" alt="logo-site">
                </a>
            </div>
            <div class="header__item item-header-menu js-menu">
                <nav class="header__menu menu-header">
                    <ul class="menu-header__list">
                        <li class="menu-header__item">
                            <!-- для активного пункта добавить класс header__link_active -->
                            <a href="{{ route('questionnaires.list') }}" class="header__link {{ url()->current() == route('questionnaires.list') ? 'header__link_active' : '' }}"
                            >Анкеты</a>
                        </li>
                        <li class="menu-header__item">
                            <a href="{{ route('vacancies.list') }}" class="header__link {{ url()->current() == route('vacancies.list') ? 'header__link_active' : '' }}"
                            >Вакансии</a>
                        </li>
                        <li class="menu-header__item">
                            <a href="{{ route('english') }}" class="header__link {{ url()->current() == route('english') ? 'header__link_active' : '' }}"
                            >English</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="header__item item-header-rating js-mob-rating">
                <div class="rating">
                    <ul class="rating__list">
                        <li class="rating__item rating__item_active">
                            <i class="svg-icon">
                                <svg>
                                    <use xlink:href="img/svg/sprite.svg#star"></use>
                                </svg>
                            </i>
                        </li>
                        <li class="rating__item rating__item_active">
                            <i class="svg-icon">
                                <svg>
                                    <use xlink:href="img/svg/sprite.svg#star"></use>
                                </svg>
                            </i>
                        </li>
                        <li class="rating__item rating__item_active">
                            <i class="svg-icon">
                                <svg>
                                    <use xlink:href="img/svg/sprite.svg#star"></use>
                                </svg>
                            </i>
                        </li>
                        <li class="rating__item rating__item_active">
                            <i class="svg-icon">
                                <svg>
                                    <use xlink:href="img/svg/sprite.svg#star"></use>
                                </svg>
                            </i>
                        </li>
                        <li class="rating__item">
                            <i class="svg-icon">
                                <svg>
                                    <use xlink:href="img/svg/sprite.svg#star"></use>
                                </svg>
                            </i>
                        </li>
                    </ul>
                    <a href="#" class="rating__text">
                        <span>8.5</span> facebook rating
                    </a>
                </div>
            </div>


            @guest
                <div class="header__item item-header-login-registration js-mob-login-registration">
                    <a  class="header__login-registration js-btn-popup header__link">
                        <i class="svg-icon">
                            <svg>
                                <use xlink:href="img/svg/sprite.svg#key"></use>
                            </svg>
                        </i>
                        <span id="login">Вход и регистрация </span>
                    </a>
                </div>
            @else
                <div class="header__item header__item-user header-user js-header-user">
                    <div class="header-user__wr">
                        <div class="header-user__row">
                            <div class="header-user__img">
                                <a href="#">
                                    <img src="{{ asset(auth()->user()->avatar) }}" alt="img">
                                </a>
                            </div>
                            <div class="header-user__name">
                                <a href="#">
                                    <span>{{ auth()->user()->name }}</span>
                                    <i class="svg-icon">
                                        <svg>
                                            <use xlink:href="img/"></use>
                                        </svg>
                                    </i>
                                </a>
                            </div>
                        </div>
                        <div class="header-user__menu-wr">
                            <div class="header-user__menu">
                                <ul>
                                    <li><a href="{{ route('profile') }}" id="profile-btn">Профиль</a></li>
                                    <form action="{{ route('logout') }}" method="POST" id="logoutForm">
                                        @csrf
                                        <li><a href="javascript:void(0);" class="aside-menu__link" id="logout-btn">Выход</a></li>
{{--                                        <li><a href="javascript:void(0);" id="logout-btn">Выход</a></li>--}}
                                    </form>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
        @endauth


            <!-- burger-menu_active -->
            <div class="burger-menu js-burger-menu">
                <span></span>
            </div>
        </div>
    </div>

    <!-- mob-menu_open -->
    <div class="mob-menu js-mob-menu"></div>
</header>
