<div class="footer__item mob_none ">
    <ul class="footer__list footer__list_just">
    <li>
        <ul>

            @foreach($job_type as $id => $jobs)
                <li><a href="{{ route('vacancies.list', ['position' => [$jobs->id_positions]]) }}">{{ $jobs->name }}</a></li>
            @endforeach

            <li><a href="{{ route('vacancies.list', ['accommodation' => [43075]]) }}">Работа с проживанием</a></li>
        </ul>
    </li>
</ul>
</div>
@foreach(array_chunk($applicant_place,8) as $links)
    <div class="footer__item mob_none">
        <ul class="footer__list">
            @foreach($links as $id => $link)
                <li><a href="{{ route('questionnaires.list', ['city' => [$link['city_id']]]) }}">{{ $link['name'] }}</a></li>
            @endforeach
        </ul>
    </div>
@endforeach

<div class="footer__item footer__item_mob">
    <div class="footer__info">
        <a href="#" class="link-icon">
                        <span class="link-icon__icon">
                            <i class="svg-icon svg-icon_info svg-icon_col">
                                <svg>
                                    <use xlink:href="{{asset('img/svg/sprite.svg#info')}}"></use>
                                </svg>
                            </i>
                            <span>?</span>
                        </span>
            <span class="link-icon__text">Задать вопрос</span>
        </a>
    </div>
    <ul class="footer__list">
        <li><a href="{{ route('about.list') }}">About | О проекте</a></li>
        <li><a href="{{route('news.list')}}">News | Новости</a></li>
        <li><a href="{{ route('contacts.list') }}">Contacts | Контакты</a></li>
        <li><a href="{{ route('howItWork.list') }}">How it works</a></li>
    </ul>
    <div class="language">
        <img src="{{asset('img/lang__ru.png')}}" alt="ru" data-google-lang="ru" class="language__img">
        <img src="{{asset('img/lang__en.png')}}" alt="en" data-google-lang="en" class="language__img">
        <img src="{{asset('img/lang__de.png')}}" alt="de" data-google-lang="de" class="language__img">
        <img src="{{asset('img/lang__fr.png')}}" alt="fr" data-google-lang="fr" class="language__img">
        <img src="{{asset('img/lang__pt.png')}}" alt="pt" data-google-lang="pt" class="language__img">
    </div>

</div>
