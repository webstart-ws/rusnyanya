
<!DOCTYPE html>
<!-- Оболочка документа -->
<html lang="ru">

<!-- Заголовок страницы, контейнер для других важных данных (не отображается) -->

<head>
    <title>Email</title>
    <!-- Подключение CSS -->
    <link rel="stylesheet" href="{{asset('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('libs/fancybox/jquery.fancybox.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/icons/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/icons/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/icons/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/icons/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/icons/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/icons/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/icons/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/icons/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/icons/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('img/icons/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/icons/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/icons/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/icons/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('js/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('img/icons/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
<!-- ОБОЛОЧКА -->
<div class="wrapper">

    <div class="wrapper__content">


        <div class="wr-body">
            <div class="container">
                <div class="el-email">
                    <div class="el-email__title">Вы успешно зарегистрировались</div>
                    <br>
                    <p>{{$user['name'] . ' ' . $user['fam'] }}, ваша анкета зарегистрирована на <a href="http://rusnyanya.org/">rusnyanya.com</a></p>
                    <br>
                    <p>
                        После прохождения модерации вы сможете посмотреть свою анкету по ссылке: <a href="http://rusnyanya.org/profile">https://rusnyanya.com/profiles/{{$user['id']}}/</a>
                    </p>
                    <br>
                    <p><b>Ваш логин:</b> <span>{{$user['email']}}</span></p>
                    <p><b>Пароль:</b> <span>{{$password}}</span></p>
                </div>
                <div class="el-social" style="display: none">
                    <div class="el-social__text">Следите за новостями RUSnyanya.com в соцсетях:</div>
                    <div class="social">
                        <ul class="social__list">
                            <li class="social__item">
                                <a href="https://www.facebook.com/rusnyanya" class="social__link social__link_fb">
                                    <svg class="svg-icon">
                                        <use xlink:href="{{asset('img/svg/sprite.svg#fb')}}"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="#" class="social__link social__link_tl">
                                    <svg class="svg-icon">
                                        <use xlink:href="{{asset('img/svg/sprite.svg#tl')}}"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="https://vk.com/club44511022" class="social__link social__link_vk">
                                    <svg class="svg-icon">
                                        <use xlink:href="{{asset('img/svg/sprite.svg#vk')}}"></use>
                                    </svg>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="https://www.youtube.com/channel/UCw5HUCcvNKr_XVt8XkNTHSw" class="social__link social__link_yo">
{{--                                    <svg class="svg-icon">--}}
{{--                                        <use xlink:href="{{asset('img/svg/sprite.svg#yo')}}"></use>--}}
{{--                                    </svg>--}}

                                    <img src="{{asset('img/svg/sprite.svg#yo')}}" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Подключеине скриптов -->
<script src="{{asset('libs/jquery/jquery.min.js')}}"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('libs/slick/slick.min.js')}}"></script>
<script src="{{asset('libs/fancybox/jquery.fancybox.min.js')}}"></script>
<script src="{{asset('libs/masket/jquery.maskedinput.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>

</body>

</html>
