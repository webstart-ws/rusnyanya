<ul class="el-select__list">
    <li class="el-select__item js-select-item el-select__item_active" data-value="Все страны">Выберите город</li>
    @foreach($cities as $city)
        <li class="el-select__item js-select-item cities {{ old('city_of_work_id') == $city->id ? 'el-select__item_active' : '' }}"
            data-value="{{ $city->name }}" data-id="{{ $city->id }}"
        >{{ $city->name }}</li>
    @endforeach
</ul>
