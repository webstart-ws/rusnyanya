<ul class="el-select__list" id="country-select" data-value="country">
    <li class="el-select__item js-select-item el" data-value="Все страны">Все страны</li>
    @foreach ($countries as $country)
        <li class="el-select__item js-select-item countries {{ old('country_of_work_id') == $country->id ? 'el-select__item_active' : '' }}"
            data-value="{{ $country->name }}" data-id="{{ $country->id }}"
        >{{ $country->name }}</li>
    @endforeach
</ul>
