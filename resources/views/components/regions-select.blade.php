<ul class="el-select__list" id="region-select" data-value="region">
    <li class="el-select__item js-select-item" data-value="Все страны">Любой регион</li>
    @foreach ($regions as $region)
        <li class="el-select__item js-select-item regions" data-value="{{ $region->name }}" data-id="{{ $region->id }}">{{ $region->name }}</li>
    @endforeach
</ul>
