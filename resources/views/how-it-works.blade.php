@extends('layouts.app')
@section('title') How it works @endsection


@section('main_content')



    <div class="section-con section-con__top-pad section-con_no-cont section-con_mob-pad section-english">
        <div class="container">
            <div class="section-con__row">
                <div class="section-con__main">
                    <div class="pages">
                        <div class="pages__item pages__item_mob english__item">
                            <div class="vacancy">
                                <h1 class="title-style-tree">How it works | Как это работает</h1>
                            </div>
                            <!-- section-text_p -->
                            <div class="section-text section-text_p">
                                <h4>Если вы желаете найти работу в качестве няни/сиделки/домработницы? Вам необходимо:</h4><br>
                               <p>1. <a href="{{  route('register', ['type' => \App\Models\User::ROLE_APPLICANT_VIEW])  }}">Зарегистрироваться</a> и разместить свою анкету бесплатно. Все анкеты проходят модерацию в течении суток. Если ваша анкета грамотно и полно составлена, то она появится на сайте. </p>
                               <p>2. Вы можете воспользоваться платными услугами. Напрммер, вы можете открыть свои контакные данные для всех зарегистрированных пользователей RUSnyanya (стоимость - 25 EUR) или получить помощь в поиске работы (стоимость 50% от заработной платы). Подробнее о платных услугах в личном кабинете. </p>

                            </div>
                            <div class="section-text section-text_p">
                                <h4>Если вы работодатель и хотите найти кандидата в качестве няни/сиделки/домработницы?</h4>
                                <h5>Вам то вам необходимо:</h5><br>
                                <p>1. Зарегистрироваться и, по желанию, разместить бесплатную вакансию на 10 дней (можете не размещать) </p>
                                <p>2. Если вы желаете связаться с кандидатми лично (получить контактные данные кандидата), то оплатите подходящий тариф. Стоимость тарифа зависит от страны поиска кандидата. Также вы можете ознакомится с тарифами в договоре оказания услуг  </p>

                            </div>


                        </div>

                    </div>
                </div>
                @guest
                <div class="section-con__aside section-con__aside-zero">
                    <div class="aside-baner">
                        <div class="aside-baner__img">
                            <img src="img/img-baner.png" alt="img">
                        </div>
                        <div class="aside-baner__text aside-baner__text_mar">
                            <p>Бесплатно регистрируйтесь и разместите вашу анкету или вакансию. Получайте прямые
                                предложения по всей Европе</p>
                        </div>
                        <div class="aside-baner__btn">
                            <a href="{{ route('register', ['type' => 'vacancies', 'auth_required' => true]) }}" class="site-btn btn-site-two">Добавить вакансию</a>
                            <a href="{{ route('register', ['type' => 'questionnaires']) }}" class="site-btn btn-site-two">Добавить анкету</a>
                        </div>
                    </div>
                </div>
                @endguest
            </div>
        </div>
    </div>
    <div class="scroll-up js-scroll-up">
        <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
            <path class="scroll-up__path js-scroll-up__path" d="M24,0 a24,24 0 0,1 0,48 a24,24 0 0,1 0,-48" />
        </svg>
    </div>

    <div class="body__opas js-boyd--opas"></div>
@endsection
