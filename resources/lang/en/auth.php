<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'auth_required' => 'To add a vacancy, register.',

    'registered' => [
        'success' => 'You have successfully registered',
        'error'   => 'Something went wrong, contact the administration',
    ],

    'subscribe' => [
        'success' => 'You have successfully subscribed.',
        'error'   => 'Something went wrong, contact the administration'
    ],

    'unsubscribe' => [
        'success' => 'You have successfully unsubscribed.',
        'error'   => 'Something went wrong, contact the administration'
    ],

    'delete' => [
        'success' => 'Your profile has been successfully deleted.',
        'error'   => 'Something went wrong, contact the administration'
    ],

    'update' => [
        'success' => 'Your profile has been successfully updated.',
        'error'   => 'Something went wrong, contact the administration'
    ],

    'image' => [
        'delete' => [
            'success' => 'Photo successfully deleted',
            'error'   => 'Something went wrong, try it again later or contact the administration'
        ]
    ]

];
