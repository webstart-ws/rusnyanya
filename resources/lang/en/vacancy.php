<?php

return [
    'save' => [
        'success'    => 'The vacancy has been successfully created.',
        'error'     => 'The vacancy has not been created, contact the administration.',
        'validate'  => 'Please fill the form correctly.',
    ],

    'update' => [
        'success'   => 'The vacancy has been successfully updated.',
        'error'     => 'The vacancy has not been updated, contact the administration.',
        'validate'  => 'Please fill the form correctly.',
    ],

    'delete' => [
        'success'   => 'The vacancy was successfully deleted.',
        'error'     => 'The vacancy has not been deleted, contact the administration.',
    ],

    'not_found' => 'There is no vacancy with the requested ID.',

];
