<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\FooterLinksController;
use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\QuestionnairesController;


Route::group(['prefix' => 'admin'], function() {
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('admin.login.page');
    Route::post('login', [LoginController::class, 'login'])->name('admin.login');
    Route::post('logout', [LoginController::class, 'logout'])->name('admin.logout');

    Route::group(['middleware' => 'auth:admin'], function() {
        Route::get('', [DashboardController::class, 'index'])->name('dashboard');
        Route::get('footer-links', [FooterLinksController::class, 'index'])->name('footer-links');

        // Users
        Route::group(['prefix' => 'users'], function() {
            Route::group(['prefix' => 'applicants'], function() {
                Route::get('/', [UserController::class, 'get_applicants'])->name('admin.applicants');
                Route::get('/edit/{id}', [UserController::class, 'edit'])->name('applicant.edit');
                Route::post('/update/{id}', [UserController::class, 'update'])->name('applicant.update');
                Route::get('/delete/{id}', [UserController::class, 'delete'])->name('applicant.delete');
            });

            Route::group(['prefix' => 'vacancy'], function() {
                Route::get('/', [UserController::class, 'get_vacancy'])->name('admin.vacancy');
                Route::get('/edit/{id}', [UserController::class, 'edit_vacancy'])->name('vacancy.edit');
                Route::post('/update/{id}', [UserController::class, 'update_vacancy'])->name('vacancy.update');
                Route::post('/delete/{id}', [UserController::class, 'delete_vacancy'])->name('vacancy.delete');
            });
            // Route::get('/', [UserController::class, 'getAllUsers'])->name('admin.users');
        });

        // News
        Route::group(['prefix' => 'news'], function() {
            Route::get('/', [NewsController::class, 'all'])->name('admin.news.all');
            Route::get('/new', [NewsController::class, 'create'])->name('admin.news.add');
            Route::get('/edit/{id}', [NewsController::class, 'edit'])->name('admin.news.edit');
            Route::post('/edit/{id}', [NewsController::class, 'update'])->name('admin.news.update');
            Route::get('/delete/{id}', [NewsController::class, 'delete'])->name('admin.news.delete');
            Route::post('/image', [NewsController::class, 'uploadImage'])->name('news.content.image.post');
        });

        // Reviews
        Route::group(['prefix' => 'reviews'], function() {
            Route::get('/', [ReviewController::class, 'all'])->name('admin.review.all');
            Route::get('/new', [ReviewController::class, 'add'])->name('admin.review.add');
            Route::post('/create', [ReviewController::class, 'create'])->name('admin.review.create');
            Route::get('/edit/{id}', [ReviewController::class, 'edit'])->name('admin.review.edit');
            Route::post('/edit/{id}', [ReviewController::class, 'update'])->name('admin.review.update');
            Route::get('/delete/{id}', [ReviewController::class, 'delete'])->name('admin.review.delete');
            Route::post('/image', [ReviewController::class, 'uploadImage'])->name('review.content.image.post');
        });
    });
});
