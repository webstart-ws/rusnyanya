<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MainController;
use App\Http\Controllers\Auth\Social\LoginController as SocialLogin;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\VacancyController;
use App\Http\Controllers\QuestionnairesController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\WorldController;
use App\Http\Controllers\PayPalPaymentController;
use App\Http\Controllers\TariffController;

Route::get('/migrate', function () {
    Artisan::call('migrate');
    return 'DONE!';
});

Route::get('/seed', function () {
    Artisan::call('world:init');
    Artisan::call('db:seed RemoveUselessCountries');
    Artisan::call('db:seed TariffsSeeder');
    return 'DONE!';
});



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::post("/getJobType", [HomeController::class, "getJobType"]);

/** image routes */
Route::get('crop-image-upload', [ImageController::class, 'index']);
Route::post('crop-image-upload ', [ImageController::class, 'uploadCropImage']);

/** Stripe  routes **/
Route::get('stripe', 'StripePaymentController@stripe');
Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');


/** Paypal routes */
Route::get('paywithpaypal', array('as' => 'paywithpaypal','uses' => 'PayPalController@payWithPaypal',));
Route::post('paypal', array('as' => 'paypal','uses' => 'PayPalController@postPaymentWithpaypal',));
Route::get('paypal', array('as' => 'status','uses' => 'PayPalController@getPaymentStatus',));


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index'])->name('home');


/** Vacancies routes **/
Route::group(['prefix' => 'vacancies'], function () {

    Route::get('', [VacancyController::class, 'index'])->name('vacancies.list');
    Route::group(['middleware' => ['auth']], function () {
        Route::get('new', [VacancyController::class, 'create'])->name('vacancy.create');
        Route::post('store', [VacancyController::class, 'store'])->name('vacancy.store');
        Route::get('my', [VacancyController::class, 'myVacancies'])->name('vacancy.my');
        Route::get('{vacancy}/edit', [VacancyController::class, 'edit'])->name('vacancy.edit');
        Route::put('{vacancy}', [VacancyController::class, 'update'])->name('vacancy.update');
        Route::delete('{vacancy}', [VacancyController::class, 'destroy'])->name('vacancy.delete');
        Route::put('profile-update', [VacancyController::class, 'updateProfile'])->name('vacancy.profile.update');
    });

    Route::group(['middleware' => ['guest']], function () {
        Route::post('register', [VacancyController::class, 'register'])->name('vacancy.register');
    });

    Route::get('{vacancy}', [VacancyController::class, 'show'])->name('vacancy.show');
});


/** News routes **/
Route::group(['prefix' => 'news'], function () {
    Route::get('', [NewsController::class, 'index'])->name('news.list');
    Route::get('{id}', [NewsController::class, 'show'])->name('news.single');
});


/** Questionnaires routes **/
Route::group(['prefix' => 'questionnaires'], function () {
    Route::get('', [QuestionnairesController::class, 'index'])->name('questionnaires.list');

    Route::group(['middleware' => ['auth']], function () {
        Route::get('new', [QuestionnairesController::class, 'create'])->name('questionnaires.create');
        Route::post('store', [QuestionnairesController::class, 'store'])->name('questionnaires.store');
        Route::get('edit/{id}', [QuestionnairesController::class, 'myQuestionnaires'])->name('questionnaires.edit');

        Route::put('profile', [QuestionnairesController::class, 'updateProfile'])->name('questionnaire.profile.update');
        Route::put('profile/edit', [QuestionnairesController::class, 'editProfile'])->name('questionnaire.profile.edit');
    });

    Route::group(['middleware' => ['guest']], function () {
        Route::post('register', [QuestionnairesController::class, 'register'])->name('applicant.register');
    });

    Route::get('{id}', [QuestionnairesController::class, 'show'])->name('questionnaires.show');
});


/** Favorites routes **/
Route::group(['prefix' => 'favorites', 'middleware' => ['auth']], function () {

    Route::post('', [QuestionnairesController::class, 'addFavorite'])->name('favorite.add');
});


Route::group(['middleware' => ['auth']], function () {
    Route::group(['prefix' => 'profile'], function () {
        Route::get('', [UserController::class, 'profilePage'])->name('profile');
        Route::delete('', [UserController::class, 'delete'])->name('profile.delete');

        Route::delete('image/{id}', [UserController::class, 'deleteImage'])->name('profile.delete.image');

        Route::get('payments', [UserController::class, 'payments'])->name('profile.payments');
        Route::post('subscribe', [UserController::class, 'subscribe'])->name('profile.subscribe');
    });
});

Route::get('world-countries', [WorldController::class, 'getCountriesSelect']);
Route::get('world-regions', [WorldController::class, 'getRegionsSelect']);
Route::get('world-cities', [WorldController::class, 'getCitiesSelect']);

Route::get('english', [MainController::class, 'english'])->name('english');
Route::get('about', [MainController::class, 'about'])->name('about.list');
Route::get('contacts', [MainController::class, 'contacts'])->name('contacts.list');
Route::get('how-it-work', [MainController::class, 'howItWork'])->name('howItWork.list');



Route::get('tariffs', [TariffController::class, 'tariffs'])
//    ->middleware('auth')
    ->name('tariffs');

Route::get('check_in_', [MainController::class, 'check_in_']);

Route::group(['middleware' => ['guest']], function (){
    Route::group(['prefix' => 'auth'], function () {
        Route::get('{driver}', [SocialLogin::class, 'redirectToProvider'])->name('social.auth');
        Route::get('{driver}/callback', [SocialLogin::class, 'handleProviderCallback'])->name('social.callback');
    });
});

Route::get('forget-password', [ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');
Route::post('forget-password', [ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
Route::get('reset-password/{token}', [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
Route::post('reset-password', [ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');



/** Paypal routes by Ara Mnatsyan **/
Route::get('handle-payment', [PayPalPaymentController::class, 'handlePayment'])->name('make.payment');
Route::get('cancel-payment', [PayPalPaymentController::class, 'paymentCancel'])->name('cancel.payment');
Route::get('payment-success', [PayPalPaymentController::class, 'paymentSuccess'])->name('success.payment');
