<?php

return [
    'client_id' => env('PAYPAL_SANDBOX_API_USERNAME'),
    'secret' => env('PAYPAL_SANDBOX_API_SECRET'),
    'settings' => array(
        'mode' => 'sandbox',
        'http.ConnectionTimeOut' => 1000,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'FINE'
    ),

];
