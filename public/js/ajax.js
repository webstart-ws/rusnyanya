$(document).ready(function() {
    $.ajax({
        url: "/getJobType",
        type: "post",
        success: function (job) {

            if(job.success)
            {
                $(".job_link").append(job.html)
            }
        }
    })

    $(".login").click(function(){
        let email = $('#email').val()
        let password = $('#password').val()
        $.ajax({
            url: "/login",
            type:'post',
            data:{email:email , password:password},
            success: function(src){
                console.log("ok")
            }
        })
    });

    $('#logout-btn').click(function (){
        $('#logoutForm').submit();
    });
})
