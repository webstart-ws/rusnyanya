$().ready( () => {

   $('#logout-btn').on('click', function () {
       /* change confirm with something beautiful alert if needed */
       if (confirm('Are you sure you want to log out?')) {
           $('#logout-form').submit();
       }
   });

});
