'use strict'

/* preview uploaded files after input change */
function handleFileSelect(event) {
    /* Check File API support */
    if (window.File && window.FileList && window.FileReader) {
        let files = event.target.files,
            output = document.getElementById("register-images");

        if (files.length <= 5) {
            output.innerHTML = '';
            for (let i = 0; i < files.length; i++) {
                let file = files[i];

                if (!file.type.startsWith('image/')) {
                    continue;
                }

                let picReader = new FileReader();
                picReader.addEventListener("load", function (event) {
                    let picFile = event.target,
                        div = document.createElement("div");

                    div.innerHTML = `<img class="thumbnail" src="${picFile.result}" title="${file.name}" />`;
                    output.insertBefore(div, null);
                });

                picReader.readAsDataURL(file);
            }
        } else {
            alert("Разрешается добавлять до 5 фотографий");
        }
    } else {
        console.log("Your browser does not support File API");
    }
}

$(document).ready(function () {
    const fileSelect = document.getElementById("file-choose-btn"),
        fileElem = document.getElementById("register-images-input");

    fileSelect.addEventListener("click", function (e) {
        e.preventDefault();
        if (fileElem) {
            fileElem.click();
        }
    }, false);

    fileElem.addEventListener("change", handleFileSelect, false);

    /* Subscribe action */
    $('#subscribe-btn').on('click', function () {
        let subscribeValue = $(this).data('value');
        if (confirm('Вы уверены, что хотите отказаться от подписки?')) {
            $.ajax({
                url: '/profile/subscribe',
                method: 'POST',
                async: true,
                data: { subscribeValue: subscribeValue },
                success: response => {
                    if (response.success === true) {
                        alert(response.message);
                        location.reload();
                    }
                },
                error: error => {
                    alert(error.message);
                }
            });
        }
    });
    $('#pwd').bind('input', function(){
        if ($(this).val().length > 0) {
            $(".svg_one_pass").hide();
            $(".svg_one_pass_act").show();
        } else {
            $(".svg_one_pass").show();
            $(".svg_one_pass_act").hide();
        }
    });
    $(".show_pass").on('click' , function (){
        var p = document.getElementById("pwd");
        if (p.type === "password") {
            p.type = "text";
            $(".svg_one_pass").hide();
            $(".svg_one_pass_act").show();
        } else {
            p.type = "password";
            $(".svg_one_pass").show();
            $(".svg_one_pass_act").hide();
        }
    }) ;

    $('#pwd_two').bind('input', function(){
        if ($(this).val().length > 0) {
            $(".svg_one_pass1").hide();
            $(".svg_one_pass_act1").show();
        } else {
            $(".svg_one_pass1").show();
            $(".svg_one_pass_act1").hide();
        }
    });
    $(".show_pass_two").on('click' , function (){
        var p1 = document.getElementById("pwd_two");
        if (p1.type === "password") {
            p1.type = "text";
            $(".svg_one_pass1").hide();
            $(".svg_one_pass_act1").show();
        } else {
            p1.type = "password";
            $(".svg_one_pass1").show();
            $(".svg_one_pass_act1").hide();
        }
    }) ;

    let mail = document.getElementById("mail");

    $("#email").keyup(function() {
        mail.value = this.value;
    });



    /* Delete user profile */
    $('#profile-delete-btn').on('click', function (){
        if (confirm('Вы уверены, что хотите удалить свой профиль?')) {
            $.ajax({
                url: '/profile',
                method: 'DELETE',
                async: true,
                success: response => {
                    if (response.success === true) {
                        alert(response.message);
                        location.reload();
                    }
                },
                error: error => {
                    alert(error.message);
                }
            });
        }
    });


    /* Delete user specific image */
    $('#register-images .delete-btn').on('click', function () {
        let imageId = $(this).data('id'),
            parent = $(this).parent();

        if (confirm('Подтвердите для удаление этой фотографии?')) {
            $.ajax({
                url: `/profile/image/${imageId}`,
                method: 'DELETE',
                async: true,
                success: response => {
                    if (response.success === true) {
                        alert(response.message);
                        parent.remove();
                    }
                },
                error: error => {
                    alert(error.message);
                }
            });
        }
    });



});
